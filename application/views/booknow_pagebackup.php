<?php $this->load->view('slider_map'); ?>
<?php $this->load->view('slider_search_form'); ?>

<!-- label white html start -->
<div class="label-white white-lable-m">
  <div class="container searchtour">
    <div class="clearfix"></div>
  
  
  <div class="row">
      <div class="col-sm-8">
        <h1 class="h1">Book a <?php echo $book['journeyType'];?></h1>
      </div>
      </div>
  
  
    <div class="searchcat-tab booking-tabs">
     
      

          <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3><?php echo $posted_data['brandName']?> <?php echo $posted_data['modelName']?></h3>
                <p><?php echo $posted_data['vehicleSegments']?>, <?php echo $posted_data['seeting'];?> Seater</p>
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            <span class="price"><i class="fa fa-rupee"></i> <?php echo $book['totalCharge']?></span><br>
				(Plus service tax)<br>
All states service taxes, toll fees and parking will be extra on actual
            </div>
            
          </div>
          
            <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3><i class="fa fa-map-marker"></i> <?php echo $posted_data['bookedFrom']?></h3>
                
            </div>
            
               
             <?php if($posted_data['userJounarytype']=='local'){ ?>   
                
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt"><?php echo $book['hours'] ?> hrs - <?php echo $book['kms'] ?> kms</p>
             <?php }?> 
             <?php if($posted_data['userJounarytype']=='outstation'){ ?>   
                
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt"><?php echo $posted_data['totalkms'] ?>kms</p>
             <?php }?> 
            </div>
            
          </div>
          
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Depart</h3>
                <p>  <?php 
                    
                      $currentDateTime = $book['bookingPicupDate'];
$newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));
                    
                    echo $newDateTime?></p>
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
             <?php


//Our dates
$date1 = $book['bookingPicupDate'];
$date2 = $book['bookingDropDate'];

//Convert them to timestamps.
 $date1Timestamp = strtotime($date1);
 $date2Timestamp = strtotime($date2);
 $date3=date('Y-m-d',$date1Timestamp);

 $date4=date('Y-m-d',$date2Timestamp);

$datefirst=date_create($date3);
$datelast=date_create($date4);
$diff=date_diff($datefirst,$datelast);
$totaldays= $diff->format("%a");


 
//echo $days = floor($difference / (60*60*24) );
 
//echo $days;
     

?>
            <p class="detailtxt">Booking for <?php echo$totaldays; ?> days</p>

            
            </div>
            
          </div>
          
          
            <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Booking by</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt"><?php  echo $_SESSION['user_data']['userName']?></p>

            
            </div>
            
          </div>
          
            <div class="row rowheading">
          <div class="col-md-12">
          <h2 class="">Passenger Details</h2>
          </div>
          </div>
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>No. of Passengers </h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-6">
            
              <select class="form-control"  name="passenger" >
<?php
    for ($i=1; $i<=100; $i++)
    {
        ?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php
    }
?>
</select>

            
            </div>
            
          </div>
          
  <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Passenger Name</h3>
              
            </div>
            
           <div class="col-xxs-12 col-xs-6 col-md-6">
            
                <input type="text" name="passenger_name" class="form-control" value="<?php echo $posted_data['customerName'] ?>"/>

            
            </div>
            
          </div>
          
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Mobile</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-6">
            
                <input type="text" name="mobile" class="form-control"  value="<?php echo $posted_data['customerMobileNumber'] ?>"/>

            
            </div>
            
          </div>
          
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Reporting  Address</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-6">
            
                <input type="text" name="address" class="form-control"/>

            
            </div>
            
          </div>
      
      	   <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-6">
             	
                <textarea class="form-control">Special Instruction</textarea>
              
            </div>
            
          
            
          </div>
      
         <div class="row brdrrow">
            <div class="col-md-12">
             	
                By clicking confirm and pay, I agree with ihire <a href="#">terms of use</a>.
                
              
            </div>
            
          
            
          </div>
      
      <div class="clearfix"></div>
      <div><a href="#" class="btn btn-default">Confirm and Pay</a></div>
      
    </div>
  </div>
</div>

<!-- label white html exit --> 


<?php $this->load->view('google_api_details'); ?>
<?php $this->load->view('script'); ?>

