<!DOCTYPE html>
<html lang="en">
<head>
<title>ihire</title>
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-select.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-datepicker.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-timepicker.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/color.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/custom-responsive.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/slide-component.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/default.css')?>" rel="stylesheet">
<!-- font awesome this template -->
<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/fonts/css/font-awesome.min.css')?>" rel="stylesheet">
</head>
<body>
<div id="preloader">
  <div class="preloader-container"> <img src="images/preloader.gif" class="preload-gif" alt="preload-image"> </div>
</div>
<div class="map-wapper-opacity">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="language-opt tec-domain-cat8" id="translateElements"> <a href="#" class="applink" data-toggle="modal" data-target="#downloadpopup">Download ihire app</a> </div>
        <div class="call-us call-us2"> <span class="img-circle img-circle01"><i class="fa fa-phone"></i></span>
          <p>Call Us at xxxxxxxxx</p>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="logo-wraper">
          <div class="logo"> <a href="index.html"> <img src="images/logo.png" alt=""> </a> </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div id="languages" class="resister-social">
          <div class="login-register login"> <a href="#" data-toggle="modal" data-target="#loginpopup">Login</a> <a href="#" data-toggle="modal" data-target="#registerpopup">Register</a> </div>
          <div class="social-icon social-icon2"> <a href="#" class="img-circle"> <i class="fa fa-facebook"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-twitter"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-tumblr"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-instagram"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-youtube"></i> </a> </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="header-menu-wrap">
          <nav class="navbar navbar-default" role="navigation">
            <div class="main  collapse navbar-collapse">
              <div class="column">
                <div id="dl-menu" class="dl-menuwrapper"> <a href="#" class="dl-trigger" data-toggle="dropdown">My Account <i class="fa fa-angle-down"></i></a>
                  <ul class="dl-menu">
                    <li> <a href="#">Trips</a> </li>
                    <li> <a href="#">My Profile</a> </li>
                    <li> <a href="#">My Reviews</a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="google-image">
			<div id="directions-panel"></div>
			<div id="map-canvas"></div>
		</div>

<!-- Booking now form wrapper html start -->
<div class="booking-form-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <div class="row">
          <div class="form-wrap">
            <div class="form-headr"></div>
            <ul class="nav nav-tabs nav-justified">
              <li class="active"><a data-toggle="tab" href="#cab"><i class="fa fa-car"></i> Cab</a></li>
              <li><a data-toggle="tab" href="#coach"><i class="fa fa-bus"></i> Coach</a></li>
              <li><a data-toggle="tab" href="#self"><i class="fa fa-car"></i> Self Drive</a></li>
            </ul>
            <div class="tab-content">
              <div id="cab" class="tab-pane fade in active">
                <div class="form-select">
                  <div class="radiowrap">
                    <div class="col-sm-4">
                      <label>
                        <input type="radio" name="localos">
                        Local</label>
                    </div>
                    <div class="col-sm-6">
                      <label>
                        <input type="radio" name="localos">
                        OutStation</label>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="local" id="cablocal" style="display: none">
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    zxczxczx
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-8 custom-select-box tec-domain-cat5 day">
                          <div class="row" >
                            <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="From Date"  />
                          </div>
                        </div>
                        <div class="col-sm-4 custom-select-box tec-domain-cat6 time">
                          <div class="row">
                            <select class="selectpicker" data-live-search="false" >
                              <option class="time1"> 08:00</option>
                              <option class="time1"> 09:00</option>
                              <option class="time1"> 10:00</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="To Date"  />
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat2">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false"  >
                          <option>Select Package</option>
                          <option>Package 1</option>
                          <option>Package 2</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-button">
                      <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="local" id="caboutstation">
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>One Way</option>
                          <option>Round Trip</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-7 padding-left0">
                          <div class='input-group date inputdate'>
                            <input type='text' class="form-control custom-select-box" placeholder="From Date" />
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                        </div>
                        <div class="col-sm-5 custom-select-box tec-domain-cat6">
                          <div class="row">
                            <div class="input-group bootstrap-timepicker">
                              <input type="text" class="form-control inputtime input-small custom-select-box">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>To city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box">
                      <div class="row">
                        <div class='input-group date inputdate'>
                          <input type='text' class="form-control custom-select-box" placeholder="To Date" />
                          <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>Select Package</option>
                          <option>4 hrs. - 0 kms</option>
                          <option>8 hrs. - 80 kms</option>
                          <option>Full day</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-button">
                      <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="coach" class="tab-pane fade">
                <div class="form-select">
                  <div class="radiowrap">
                    <div class="col-sm-4">
                      <label>
                        <input type="radio" name="localos">
                        Local</label>
                    </div>
                    <div class="col-sm-6">
                      <label>
                        <input type="radio" name="localos">
                        OutStation</label>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="local" id="coachlocal" style="display: none">
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-8 custom-select-box tec-domain-cat5 day">
                          <div class="row" >
                            <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="From Date"  />
                          </div>
                        </div>
                        <div class="col-sm-4 custom-select-box tec-domain-cat6 time">
                          <div class="row">
                            <select class="selectpicker" data-live-search="false" >
                              <option class="time1"> 08:00</option>
                              <option class="time1"> 09:00</option>
                              <option class="time1"> 10:00</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="To Date"  />
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat2">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false"  >
                          <option>Select Package</option>
                          <option>Package 1</option>
                          <option>Package 2</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat2">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false"  >
                          <option>No. of Pessengers</option>
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>10</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-button">
                      <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="local" id="coachoutstation">
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>One Way</option>
                          <option>Round Trip</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-8 custom-select-box tec-domain-cat5 day">
                          <div class="row" >
                            <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="From Date"  />
                          </div>
                        </div>
                        <div class="col-sm-4 custom-select-box tec-domain-cat6 time">
                          <div class="row">
                            <select class="selectpicker" data-live-search="false" >
                              <option class="time1"> 08:00</option>
                              <option class="time1"> 09:00</option>
                              <option class="time1"> 10:00</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>To city</option>
                          <option>khulna</option>
                          <option>dhaka</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                      <div class="row">
                        <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="To Date"  />
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat2">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false"  >
                          <option>No. of Pessengers</option>
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>10</option>
                        </select>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-button">
                      <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="self" class="tab-pane fade">
                <div class="form-select">
                  <div class="selfdrive">
                    <div class="col-sm-12">
                      <label>I am in</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 custom-select-box tec-domain-cat1">
                      <div class="row">
                        <select class="selectpicker" data-live-search="false" >
                          <option>New Delhi</option>
                          <option>Noida</option>
                          <option>Jaipur</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-8 custom-select-box tec-domain-cat5 day">
                          <div class="row" >
                            <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="Pickup Date"  />
                          </div>
                        </div>
                        <div class="col-sm-4 custom-select-box tec-domain-cat6 time">
                          <div class="row">
                            <select class="selectpicker" data-live-search="false" >
                              <option class="time1"> 08:00</option>
                              <option class="time1"> 09:00</option>
                              <option class="time1"> 10:00</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-8 custom-select-box tec-domain-cat5 day">
                          <div class="row" >
                            <input class="form-control custom-select-box tec-domain-cat5" type="date" name="date" placeholder="Drop Date"  />
                          </div>
                        </div>
                        <div class="col-sm-4 custom-select-box tec-domain-cat6 time">
                          <div class="row">
                            <select class="selectpicker" data-live-search="false" >
                              <option class="time1"> 08:00</option>
                              <option class="time1"> 09:00</option>
                              <option class="time1"> 10:00</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-button">
                      <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Booking now form wrapper html Exit --> 

<!-- label white html start -->
<div class="label-white white-lable-m">
  <div class="container searchtour">
    <div class="clearfix"></div>
  
  
  <div class="row">
      <div class="col-sm-8">
        <h1 class="h1">Book a Cab</h1>
      </div>
      </div>
  
  
    <div class="searchcat-tab booking-tabs">
     
      

          <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Honda Jazz</h3>
                <p>HatchBlack, 4 Seater</p>
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            <span class="price"><i class="fa fa-rupee"></i> 2400</span><br>
				(Plus service tax)<br>
All states service taxes, toll fees and parking will be extra on actual
            </div>
            
          </div>
          
            <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3><i class="fa fa-map-marker"></i> Greater Noida</h3>
                
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt">8 hrs - 80 kms</p>
                        
            </div>
            
          </div>
          
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Depart</h3>
                <p>Sun, July 3, 11:30 am</p>
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt">Booking for 3 days</p>

            
            </div>
            
          </div>
          
          
            <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Booking by</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt">New Test</p>

            
            </div>
            
          </div>
          
            <div class="row rowheading">
          <div class="col-md-12">
          <h2 class="">Passenger Details</h2>
          </div>
          </div>
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>No. of Passengers </h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt">4</p>

            
            </div>
            
          </div>
          
  <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Passenger Name</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt">New Test</p>

            
            </div>
            
          </div>
          
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Mobile</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-3">
            
            <p class="detailtxt">9999999999</p>

            
            </div>
            
          </div>
          
           <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-3">
             	<h3>Reporting  Address</h3>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-9">
            
            <p class="detailtxt">Lorm Ipsum Lorm Ipsum </p>

            
            </div>
            
          </div>
      
      	   <div class="row brdrrow">
            <div class="col-xxs-12 col-xs-6 col-md-6">
             	
                <textarea class="form-control">Special Instruction</textarea>
              
            </div>
            
          
            
          </div>
      
         <div class="row brdrrow">
            <div class="col-md-12">
             	
                By clicking confirm and pay, I agree with ihire <a href="#">terms of use</a>.
                
              
            </div>
            
          
            
          </div>
      
      <div class="clearfix"></div>
      <div><a href="#" class="btn btn-default">Confirm and Pay</a></div>
      
    </div>
  </div>
</div>

<!-- label white html exit --> 

<!-- label yellow html start -->
<div class="yellow-label-wrapper2">
  <div class="label-yellow stellar" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="" >
    <div class="container">
      <div class="row">
        <div class="destination">
          <h2>Destinations You'd Love</h2>
          <h4>Look at the wonderful places</h4>
        </div>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <div class="slider-btn"> <a class="right-cursor1" href="#carousel-example-generic" data-slide="prev"></a> <a class="left-cursor1" href="#carousel-example-generic" data-slide="next"></a> </div>
          <div class="carousel-inner">
            <div class="item active">
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item ">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider1.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item ">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider2.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item homepage-sllider-m">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider3.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item ">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider4.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item ">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider5.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item homepage-sllider-m">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider6.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item ">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider7.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item ">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider8.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="row">
                  <div class="slider-item homepage-sllider-m">
                    <div class="slider-img"><img class="img-responsive" alt="First slide" src="images/slider/slider9.jpg"/></div>
                    <div class="slider-text-hover">
                      <div class="slider-hover-content"></div>
                      <div class="Orange">
                        <div class="slider-hover-content2">
                          <h4>Orange Skies</h4>
                          <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                      </div>
                    </div>
                    <div class="slider-text">
                      <div class="slider-text1">
                        <h4>Orange Skies</h4>
                        <p>Save upto 50%</p>
                      </div>
                      <div class="slider-text2"> <a href="#" class="btn slide-btn btn-lg">Avail Now</a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- label yellow html exit --> 

<!-- label white2 html start -->
<div class="label-white2">
  <div class="container">
    <div class="row">
      <div class="car-item-wrap">
        <div class="car-type">
          <div class="car-wrap"><img class="private-car" src="images/private-car.png" alt=""/></div>
          <h5>Private Car</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"><img class="morotbike-car" src="images/motorbike.png" alt=""/></div>
          <h5>Motorbike</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="minicar-car" src="images/minicar.png" alt=""/></div>
          <h5>Minicar</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="mini-track-car" src="images/mini-track.png" alt=""/></div>
          <h5>Mini Truck</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="boat-car" src="images/boat.png" alt=""/></div>
          <h5>Boat</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="snow-car" src="images/snow-bike.png" alt=""/></div>
          <h5>Snow Bike</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="tractor-car" src="images/tractor.png" alt=""/></div>
          <h5>Tractor</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="vihicel-car" src="images/vihicel.png" alt=""/></div>
          <h5>Large Vehicle</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="morotbike-car" src="images/motorbike.png" alt=""/></div>
          <h5>Motorbike</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
        <div class="car-type">
          <div class="car-wrap"> <img class="big-track-car" src="images/big-track.png" alt=""/></div>
          <h5>Big Truck</h5>
          <div class="car-type-btn"> <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- label white2 html Exit --> 

<!-- page21 html start -->
<div class="page19-Featued-Drivers">
  <div class="page19-wrap stellar"  data-stellar-background-ratio="0.5" data-stellar-vertical-offset="" >
    <div class="container">
      <div class="row">
        <div class="page19-span">
          <h2>Featued Drivers of the Week</h2>
        </div>
        <div class="page19-array2">
          <h4>These drivers are featured as best</h4>
        </div>
        <div id="carousel-example-generic5" class="carousel slide" data-ride="carousel">
          <div class="slider-btn"> <a class="right-cursor2" href="#carousel-example-generic5" data-slide="prev">
            <div class="featured-drivers"> <i class="fa fa-angle-left"></i></div>
            </a> <a class="left-cursor2" href="#carousel-example-generic5" data-slide="next">
            <div class="featured-drivers featured22"><i class="fa fa-angle-right"></i> </div>
            </a> </div>
          <div class="featured-drivers-wrapper">
            <div class="carousel-inner">
              <div class="item active">
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team1.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>John Doe</h4>
                    </a> <span>Romania</span> </div>
                </div>
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team2.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>Stephy Doe</h4>
                    </a> <span>Los Angeles</span> </div>
                </div>
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team3.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>Charlotte Doe</h4>
                    </a> <span>California</span> </div>
                </div>
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team4.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>Jonathan Doe</h4>
                    </a> <span>Spain</span> </div>
                </div>
                <div class="featured-drivers2 featured22">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team5.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>James Doe</h4>
                    </a> <span>Canada</span> </div>
                </div>
              </div>
              <div class="item">
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team1.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>John Doe</h4>
                    </a> <span>Romania</span> </div>
                </div>
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team2.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>Stephy Doe</h4>
                    </a> <span>Los Angeles</span> </div>
                </div>
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team3.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>Charlotte Doe</h4>
                    </a> <span>California</span> </div>
                </div>
                <div class="featured-drivers2">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team4.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>Jonathan Doe</h4>
                    </a> <span>Spain</span> </div>
                </div>
                <div class="featured-drivers2 featured22">
                  <div class="featured">
                    <div class="featured-images"><a href=""><img src="images/team5.jpg" alt=""/></a></div>
                    <div class="featured-hover"></div>
                  </div>
                  <div class="featured-text"> <a href="">
                    <h4>James Doe</h4>
                    </a> <span>Canada</span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- page21 html exit --> 
<!-- ================ footer html start ================ -->
<div class="footer custom-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="menu-wrap">
            <div class="menu-col">
              <div class="menu-item">
                <ul>
                  <li><a href="http://ihire.in" target="_blank">Corporate Website</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">Media</a></li>
                  <li><a href="#">Careers</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>
            </div>
            <div class="menu-col">
              <div class="menu-item">
                <ul>
                  <li><a href="#">Affiliate/ Alliances</a></li>
                  <li><a href="#">Cancellation policy</a></li>
                  <li><a href="#">Terms of use</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Disclaimer</a></li>
                </ul>
              </div>
            </div>
            <div class="menu-col">
              <div class="menu-item">
                <ul>
                  <li><a href="#">App Downloads</a></li>
                </ul>
              </div>
            </div>
            <div class="menu-col">
              <div class="menu-item">
                <ul>
                  <li><a href="">Transporter Zone</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="col-sm-5">
						<div class="row">
							<div class="footer-map-bg">
								
							</div>
						</div>
					</div> --> 
    </div>
    <div class="row">
      <div class="social-item"> <a href="#" class="img-circle"> <i class="fa fa-facebook"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-twitter"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-tumblr"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-instagram"></i> </a> <a href="#" class="img-circle"> <i class="fa fa-youtube"></i> </a> </div>
      <div class="copy-right">
        <p><span><img src="images/footer-logo.png"></span> &copy; Copyright 2016 | All Rights Rerved.</p>
      </div>
    </div>
  </div>
</div>

<!-- Download Modal -->
<div id="downloadpopup" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Download</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-mobile-phone"></i>
            <input type="text" class="form-control" placeholder="Mobile" />
          </div>
        </div>
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-user"></i>
            <input type="text" class="form-control" placeholder="Name" />
          </div>
        </div>
        <div class="form-group">
          <p>Choose your OS</p>
          <div class="row">
            <div class="col-md-3">
              <label>
                <input type="radio" name="os">
                <img src="images/andriod-logo.png"></label>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <label>
                <input type="radio" name="os">
                <img src="images/ios-logo.png"></label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <input type="submit" value="Download" class="form-btn">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Login Modal -->
<div id="loginpopup" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-user"></i>
            <input type="text" class="form-control" placeholder="Username / Phone no." />
          </div>
        </div>
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-lock"></i>
            <input type="password" class="form-control" placeholder="Password" />
          </div>
        </div>
        <div class="form-group">
          <input type="submit" value="Login" class="form-btn">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Register Modal -->
<div id="registerpopup" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Register</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-user"></i>
            <input type="text" class="form-control" placeholder="Name" />
          </div>
        </div>
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-map-marker"></i>
            <input type="text" class="form-control" placeholder="Location" />
          </div>
        </div>
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-mobile"></i>
            <input type="text" class="form-control" placeholder="Add 10 digit mobile number" />
          </div>
        </div>
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-envelope"></i>
            <input type="text" class="form-control" placeholder="Email" />
          </div>
        </div>
        <div class="form-group">
          <div class="right-inner-addon"> <i class="fa fa-lock"></i>
            <input type="password" class="form-control" placeholder="Password" />
          </div>
        </div>
        <div class="form-group">
          <input type="submit" value="Signup" class="form-btn">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ================ footer html Exit ================ --> 
  <script src="<?php echo base_url('assets/js/jquery.js')?>"></script> 
<script src="<?php echo base_url('assets/js/menu/jquery.min.js')?>"></script> 
<script src="<?php echo base_url('assets/js/menu/modernizr.custom.js')?>"></script> 
<script src="<?php echo base_url('assets/js/menu/jquery.dlmenu.js')?>"></script> 
<script src="<?php echo base_url('assets/js/bg-slider/jquery.imagesloaded.min.js')?>"></script> 
<script src="<?php echo base_url('assets/js/bg-slider/cbpBGSlideshow.min.js')?>"></script> 
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="<?php echo base_url('assets/js/g-map/map.js" type="text/javascript')?>"></script> 
<script src="<?php echo base_url('assets/js/uikit.js" type="text/javascript')?>"></script> 
<script src="<?php echo base_url('assets/js/jquery.stellar.js')?>"></script> 
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script> 
<script src="<?php echo base_url('assets/js/bootstrap-select.js')?>"></script> 
<script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/js/custom.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/js/custom2.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/js/menu/custom-menu.js')?>"></script> 
<script type="text/javascript">
         	function setModalMaxHeight(element) {
  this.$element     = $(element);  
  this.$content     = this.$element.find('.modal-content');
  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight     = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
      'overflow': 'hidden'
  });
  
  this.$element
    .find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
  });
}

$('.modal').on('show.bs.modal', function() {
  $(this).show();
  setModalMaxHeight(this);
});

$(window).resize(function() {
  if ($('.modal.in').length != 0) {
    setModalMaxHeight($('.modal.in'));
  }
});

         </script> 
<script src="<?php echo base_url('js/bootstrap-datepicker.min.js')?>"></script> 
<script src="<?php echo base_url('js/bootstrap-timepicker.min.js')?>"></script> 
<script type="text/javascript">
   		$(function(){
			
			 $('.inputdate').datepicker();
			 
			  $('.inputtime').timepicker();
			
         
			});
   </script>
</body>
</html>