
<?php if (!empty($list)) { ?>       
    <?php foreach ($list as $vehicle_list) { ?> 

        <div class="col-md-3 col-sm-6">
            <div class="grid-dv">
                <figure style="width:203px;height:150px;"><img src="<?php echo model_image . strtolower($vehicle_list['ModelImage']); ?>" alt="" style="width:203px;height:125px;"></figure>
                <h2><?php echo $vehicle_list['brandName'] ?> <?php echo $vehicle_list['modelName']; ?></h2>
                <h3><?php echo $vehicle_list['segementsName']; ?> |<?php echo $vehicle_list['numbersOfseat']; ?> Seater</h3>
                <hr>
                <div class="clearfix"></div>
                <div class="row bottom">
                    <div class="col-md-6 price">
                        Starting @ <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $vehicle_list['totalCharge']; ?></span>
                    </div>

                    <div class="col-md-6">
                        <button class="btn btn-default btn-block booknow" modelId="<?php echo $vehicle_list['modelId'] ?>">Book Now</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
} else {
    ?>

    <?php echo $data['message']; ?> 
<?php }
?>

<div class="clearfix"></div>
<ul class='page'>
    <?php
    if (!empty($number['totalpages'])) {
        $start = 0;
        $limit = 10;
        if (isset($_POST['pageNo'])) {

            $id = $_POST['pageNo'];
            $start = ($id - 1) * $limit;
        } 
        elseif (isset($_GET['pageNo'])) {

            $id = $_GET['pageNo'];
            $start = ($id - 1) * $limit;
        }else {
            $id = 1;
        }

        $total = $number['totalpages'];
        if ($id > 1) {

            echo "<button id=" . ($id - 1) . " class='paginationshow button' onclick='serach_data1(id,-100);'>PREVIOUS</button>";
        }
        ?>

        <?php
        for ($i = 1; $i <= $total; $i++) {
            if ($i == $id) {
                echo "<li class='current'>" . $i . "</li>";
            } else {
                echo "<li><button id=$i class='paginationshow'onclick='serach_data1(id,-100);'>" . $i . "</button></li>";
            }
        }
        ?>

        <?php
        if ($id != $total) {
            ////Go to previous page to show next 10 items.
            echo "<button id=" . ($id + 1) . " class='paginationshow button' onclick='serach_data1(id,-100);'>NEXT</button>";

            //echo "<a href=".base_url()."vehiclelist?page=".($id+1)."' class='paginationshow button'>NEXT</a>";
        }
    }
    ?>
</ul>


<script type="text/javascript">

    $(".booknow").click(function () {

        //alert('hello');
        modelId = $(this).attr('modelId');

        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();
        //alert(journeyType);
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var pick_drop_charges = $("#pick_drop_charges").val();
        //alert(pick_drop_charges);
        window.location = "<?php echo base_url(); ?>selfdrive_transporterlist?userId=" + userId + "&journeyType=" + journeyType + "&bookingPicupDate=" + bookingPicupDate + "&bookingDropDate=" + bookingDropDate + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo + "&modelId=" + modelId + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&pick_drop_charges=" + pick_drop_charges + "&ajax_transporter_request=" + 1;
    });


</script>