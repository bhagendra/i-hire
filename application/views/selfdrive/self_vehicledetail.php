<?php $this->load->view('selfdrive/slider_map'); ?>
<?php
$this->load->view('slider_search_form');
//prd($company_details);
?>
<!-- label white html start -->

<div class="container searchtour">
    <div class="clearfix"></div>
    <div class="row datetime">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4">
            <a href="#" class="pull-right btn btn-default">Modify Search</a>
        </div>
    </div>
</div>

<div class="label-white white-lable-m"id="midsection">
    <div class="container searchtour">

        <h3><?php echo $company_details['companyName'] ?>

<?php if ($rates['avgrate'] != '' && $rates['avgrate'] != '0') { ?>

                (


    <?php if ($company_details['avgrate'] > 0 && $company_details['avgrate'] <= 2) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 

                <?php } ?>
    <?php if ($company_details['avgrate'] >= 2 && $company_details['avgrate'] < 3) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                <?php } ?>

    <?php if ($company_details['avgrate'] >= 3 && $company_details['avgrate'] < 4) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 

                <?php } ?>
    <?php if ($company_details['avgrate'] >= 4 && $company_details['avgrate'] < 5) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 

                <?php } ?>
    <?php if ($company_details['avgrate'] >= 5) { ?>


                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                <?php } ?>)

            <?php } else {
                ?>

                (No Rating given yet)

            <?php }
            ?>
        </h3>


        <div class="clearfix"></div>

        <div class="searchcat-tab booking-tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#BookingInfo">Booking Info</a></li>
                <li><a data-toggle="tab" href="#FareDetails">Fare Details</a></li>
                <li><a data-toggle="tab" href="#VehicleDetails">Vehicle Details</a></li>
            </ul>
            <form method="get" action="<?php echo base_url() ?>selfdrive_booknow">
                <div class="tab-content">
                    <div id="BookingInfo" class="tab-pane fade in active">
                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Base Fare</h3>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <span class="price"><i class="fa fa-rupee"></i> <?php echo $bookinginfo['totalCharge'] ?></span><br>
                                (Plus service tax)<br>
                                All states service taxes, toll fees and parking will be extra on actual)
                            </div>

                        </div>

                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Depart</h3>

                            </div>
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <p class="detailtxt">


                                    <?php
                                    $currentDateTime = $bookinginfo['bookingPicupDate'];
                                    $newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));

                                    echo $newDateTime
                                    ?></p>

                            </div>
                        </div>
                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Drop</h3>

                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">

                                <p class="detailtxt"><?php
                                    $currentDateTime = $bookinginfo['bookingDropDate'];
                                    $newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));

                                    echo $newDateTime
                                    ?></p>


                            </div>

                        </div>

                    </div>

                    <div id="FareDetails" class="tab-pane fade">
                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Base Fare</h3>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <span class="price"><i class="fa fa-rupee"></i> <?php echo $terms['totalCharge'] ?></span><br>
                                (Plus service tax)<br>
                                All states service taxes, toll fees and parking will be extra on actual)
                            </div>

                        </div>

                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Package Fare</h3>
                                <p>(<?php echo $terms['durantion'] ?>)</p>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <span class="price"><i class="fa fa-rupee"></i> <?php echo $terms['totalCharge'] ?></span>
                            </div>

                        </div>

                        <?php if($terms['pick_drop_charges']!='0'){?>
                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Pick-up & Drop Charges</h3>

                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <span class="price"><i class="fa fa-rupee"></i> <?php echo $terms['pick_drop_charges'] ?></span>
                            </div>

                        </div>
<?php } ?>
                        <div class="row rowheading">
                            <div class="col-md-12">
                                <h2 class="">Inclutions &amp; Extras </h2>
                            </div>
                        </div>


                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Fule Included</h3>

                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <p class="detailtxt"><?php echo $terms['fuel_included']; ?>(km)</p>
                            </div>

                        </div>

                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Extra Km Rate</h3>

                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <p class="detailtxt"><i class="fa fa-rupee"></i> <?php echo $terms['extraKmsCharges']; ?></p>
                            </div>

                        </div>

                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Security Deposit</h3>

                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <p class="detailtxt"><i class="fa fa-rupee"></i> <?php echo $terms['securitydeposite']; ?></p>
                            </div>

                        </div>



                    </div>
                    <div id="VehicleDetails" class="tab-pane fade">

                        <div class="row">
                            <div class="col-md-7">  <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>Segment</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php echo $posted_data['segementsName'] ?></p>
                                    </div>

                                </div>

                                <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>Vehicle</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php echo $vehicledetails['brandName'] ?> <?php echo$vehicledetails['modelName'] ?></p>
                                    </div>

                                </div>

                                <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>Seating Capacity</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php echo $vehicledetails['numbersOfseat'] ?></p>
                                    </div>

                                </div>
                                <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>Transmission</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php
                                            if ($vehicledetails['automatic'] == '1') {
                                                echo "Automatic";
                                            } else {
                                                echo "Manual";
                                            }
                                            ?></p>

                                    </div>

                                </div>
                                <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>FuelType</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php echo$vehicledetails['fuelType'] ?></p>
                                    </div>

                                </div>

                            </div>

                            <div class="col-md-5">
                                <div class="imgsection">
                                    <img src="http://ihire.in/iHire_api/assets/upload/model/<?php echo strtolower($vehicledetails['ModelImage']); ?>" class="img-responsive">
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <div class="clearfix"></div>

                <input type="hidden" name="transporterId" value="<?php echo $posted_data['transporterId'] ?>"/>
                <input type="hidden" name="vehicleId" value="<?php echo $posted_data['vehicleId'] ?>"> 
                <input type="hidden" name="journeyType" value="<?php echo $posted_data['journeyType'] ?>"/>

                <input type="hidden" name="bookingPicupDate" value="<?php echo $posted_data['bookingPicupDate'] ?>"/>
                <input type="hidden" name="bookingDropDate" value="<?php echo $posted_data['bookingDropDate'] ?>"/>

                <input type="hidden" name="bookedFrom" value="<?php echo $posted_data['bookedFrom'] ?>"/>



                <input type="hidden" name="vehicleModel" value="<?php echo $vehicledetails['vehicleModel'] ?>"> 

                <input type="hidden" name="userJounarytype" value="<?php echo $posted_data['userJounarytype'] ?>"/>

                <input type="hidden" name="vehicleType" value="<?php echo $posted_data['vehicleType'] ?>"/>
                <input type="hidden" name="segments" value="<?php echo$vehicledetails['segments'] ?> "> 
                <input type="hidden" name="totalCharge" value="<?php echo $terms['totalCharge'] ?>">
                         <input type="hidden" name="segementsName" value="<?php echo $posted_data['segementsName'] ?>">
                         
                         <input type="hidden" name="modelName" value="<?php echo$vehicledetails['modelName'] ?>">
                
                         <input type="hidden" name="brandName" value="<?php echo $vehicledetails['brandName'] ?>">
                              <input type="hidden" name="numbersOfseat" value="<?php echo $vehicledetails['numbersOfseat'] ?>">
                              
                              <input type="hidden" name="fuelType" value="<?php echo$vehicledetails['fuelType'] ?>">
                               <input type="hidden" name="securitydeposite" value="<?php echo $terms['securitydeposite']; ?>">
                                <input type="hidden" name="extraKmsCharges" value="<?php echo $terms['extraKmsCharges']; ?>">
                                <input type="hidden" name="companyname" value=" <?php echo $company_details['companyName']?>">
                <input type="hidden" name="avgrate" value=" <?php echo $company_details['avgrate']?>">
                              
                

<?php if (!empty($_SESSION['user_data']['id'])) { ?>

                    <input type="hidden" name="customerMobileNumber" value="<?php echo $_SESSION['user_data']['mobileNumber'] ?>"> 

                    <input type="hidden" name="customerName" value="<?php echo $_SESSION['user_data']['userName'] ?>">
                    <input type="hidden" name="email" value="<?php echo $_SESSION['user_data']['emailid'] ?>">

<?php } ?>


                <div>

<?php if (!empty($_SESSION['user_data']['id'])) { ?>
                        <input type="submit" value="BOOK NOW" class="btn btn-default">

                        <?php
                    } else {
                        ?>
                        <a href="#" class="btn btn-default"data-toggle="modal" data-target="#loginpopup">Login</a>

                    <?php }
                    ?>

                </div>

            </form>

        </div>
    </div>
</div>

<!-- label white html exit --> 

<?php $this->load->view('google_api_details'); ?>
<?php $this->load->view('script'); ?>
