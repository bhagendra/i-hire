<?php $this->load->view('selfdrive/slider_map'); ?>
<?php
$this->load->view('slider_search_form');
//prd($company_details);
?>
<div class="container" >
			<div class="row">
                             <h3 style="text-align:center;">Book a <?php echo $posted_data['journeyType']?>(<?php echo $company_details['companyname']?>)</h3>
				<div class="page29-wrap" id="midsection">				
					<div class="col-sm-6">
						<div class="row booking">
							<div class="short-banner">
								<div class="short-banner-img"><img class="img-responsive" src="<?php echo base_url('assets/images/short-banner.jpg')?>" alt=""/></div> 
								<div class="short-banner-text"><a href=""><h4>I'm In <?php echo $posted_data['bookedFrom'] ?> <br/> For A Few Days & I Need A <?php echo $posted_data['brandName'] ?> <?php echo $posted_data['modelName'] ?></h4></a></div> 
							</div>
							<h3>Your Booking Details</h3>
							<h5>Give complete Booking details</h5>
							<form class="form-horizontal" method="get" action="<?php echo base_url(); ?>selfdrive_userbooking">
								<div class="form-group"> 
									<div class=" col-sm-12 third-form-wrap">
										<input type="text"value="<?php echo $posted_data['customerName'] ?>" class="form-control" id="inputname2" name="customer_name" placeholder="First Name*" required> 
									</div>
								</div>
								<div class="form-group"> 
									<div class=" col-sm-12 third-form-wrap"> 
										<input type="text" value="<?php echo $posted_data['customerMobileNumber'] ?>"class="form-control"  name="mobile_number" id="inputnumber2" placeholder="Mobile Number*" required>
									</div>
								</div>
								 <div class="form-group"> 
									<div class=" col-sm-12 third-form-wrap"> 
										<input type="text" name="lisence" class="form-control" id="inputcountry2" placeholder="Driving Lisence*" required>
									</div>
								</div>
								<div class="form-group"> 
									<div class=" col-sm-12 third-form-wrap"> 
									 <input type="text" name="address" class="form-control" id="inputcountry2" placeholder="Reporting Address*" required>
									</div>
								</div> 
								
  <!--hidden feilds-->
  <input type="hidden" name="transporterId" value="<?php echo $posted_data['transporterId']?>"/>
   <input type="hidden" name="vehicleId" value="<?php echo $posted_data['vehicleId']?>"/>
   <input type="hidden" name="journeyType" value="<?php echo $posted_data['journeyType']?>"/>
   
   <input type="hidden" name="bookingPicupDate" value="<?php echo $posted_data['bookingPicupDate']?>"/>
   
   <input type="hidden" name="bookingDropDate" value="<?php echo $posted_data['bookingDropDate']?>"/>
   
    <input type="hidden" name="bookedFrom" value="<?php echo $posted_data['bookedFrom']?>"/>
    
    <input type="hidden" name="customerMobileNumber" value="<?php echo $posted_data['customerMobileNumber']?>"/>
    
      <input type="hidden" name="userJounarytype" value="<?php echo $posted_data['userJounarytype']?>"/>
      
       <input type="hidden" name="vehicleModel" value="<?php echo $posted_data['vehicleModel']?>"/>
       
         <input type="hidden" name="vehicleType" value="<?php echo $posted_data['vehicleType']?>"/>

             <input type="hidden" name="totalCharge" value="<?php echo $posted_data['totalCharge']?>"/>
                 <input type="hidden" name="customerName" value="<?php echo $posted_data['customerName']?>"/>
                 
                   <input type="hidden" name="email" value="<?php echo $posted_data['email']?>"/>
                     <input type="hidden" name="segments" value="<?php echo $posted_data['segments']?>"/>
                       <input type="hidden" name="segementsName" value="<?php echo $posted_data['segementsName']?>"/>
                       
                       <input type="hidden" name="modelName" value="<?php echo $posted_data['modelName']?>"/>
                       <input type="hidden" name="brandName" value="<?php echo $posted_data['brandName']?>"/>
                       <input type="hidden" name="numbersOfseat" value="<?php echo $posted_data['numbersOfseat']?>"/>
                       <input type="hidden" name="fuelType" value="<?php echo $posted_data['fuelType']?>"/>
                       <input type="hidden" name="securitydeposite" value="<?php echo $posted_data['securitydeposite']?>"/>
                        <input type="hidden" name="extraKmsCharges" value=" <?php echo $posted_data['extraKmsCharges']; ?>">
                           <input type="hidden" name="companyname" value=" <?php echo $company_details['companyname']?>">
                <input type="hidden" name="avgrate" value=" <?php echo $company_details['avgrate']?>">
  <!--hidden feilds-->
                                                            
                                    <div class="form-group">
                            <p>By Clicking Conform & Pay,I Agree with IHIRE<a href="<?php echo base_url();?>pages/terms" target="_blank"> terms of use</a></p>
                        </div>                         
								
								<div class="form-group">
								  <div class="col-sm-offset-4 col-sm-8">
									<div class="completing-form-btnwrap completing-form-btnwrap2"><button type="submit" class="btn form-btn  btn-block">Conform & Pay</button></div>
								  </div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row">
							<div class="contact-details-wrap">
								<div class="contact-details-wrap-top">
									<div class="contact-details-car-wrap"><a href=""><img class="minicar4" src="<?php echo base_url('assets/images/minicar.png') ?>" alt=""/></a></div> 
									<h4><?php echo $posted_data['brandName'] ?> <?php echo $posted_data['modelName'] ?></h4>
								</div>
								
								
								<div class="contact-details-wrap-bottom Car-Type-wrap-margin"> 
									<div class="row">
										<div class="col-sm-12">
											<div class="Car-Type"><span>Car Type:</span></div>
											<div class="Private-Car"><span><?php echo $posted_data['brandName'] ?> <?php echo $posted_data['modelName'] ?>(<?php echo $posted_data['segementsName'] ?> <?php echo $posted_data['numbersOfseat'] ?> Seater)</span></div>
										</div>
									</div>
									
                                                                    <div class="row">
										<div class="col-sm-12">
											<div class="Car-Type"><span>Journey Type:</span></div>
                                                                                        <div class="Private-Car"><span><?php echo ucfirst($posted_data['userJounarytype']) ?></span></div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="Car-Type"><span>From:</span></div>
											<div class="Private-Car"><span><?php echo $posted_data['bookedFrom'] ?></span></div>
										</div>
									</div>
									
									
									
								 <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Pickup Date: </span></div>
                                    <div class="Private-Car"><span> <?php
                                            $currentDateTime = $book['bookingPicupDate'];
                                            $newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));

                                            echo $newDateTime
                                            ?></span></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Return Date:</span></div>
                                    <div class="Private-Car"><span><?php
                                            $currentDateTime = $book['bookingDropDate'];
                                            $newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));

                                            echo $newDateTime
                                            ?></span></div>
                                </div>
                            </div>
                                                                    
                                                                    <div class="row">
										<div class="col-sm-12">
											<div class="Car-Type"><span>Security:</span></div>
											<div class="Private-Car"><span><?php echo $posted_data['securitydeposite'] ?></span></div>
										</div>
									</div>
                                                                    
                                                                    <div class="row">
										<div class="col-sm-12">
											<div class="Car-Type"><span>FuelType:</span></div>
											<div class="Private-Car"><span><?php echo $posted_data['fuelType'] ?></span></div>
										</div>
									</div>

									
									
								</div>
								
								<div class="contact-details-wrap-footer">
									<span>Total Fare:</span>
									<h2><i class="fa fa-rupee"></i> <?php echo $book['totalCharge'] ?></h2>
									<p>(Inclusive of All Taxes)</p>
								</div>
							</div>
						</div>
					</div>				
				</div>  
			</div>
		</div>
<?php $this->load->view('google_api_details'); ?>
<?php $this->load->view('script'); ?>