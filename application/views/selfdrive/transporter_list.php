<?php $this->load->view('slider_common'); ?>
<?php $this->load->view('slider_search_form'); ?>
<div class="label-white white-lable-m"id="midsection">
  <div class="container tourrating">
    <div class="clearfix"></div>
      <div class="row datetime">
            <div class="col-sm-8">
                <h2><?php echo $posted_data['bookedFrom']; ?> 
                    <?php if (!empty($posted_data['bookedTo'])) {
                        ?>
                        to <?php echo $posted_data['bookedTo']; ?>
                    <?php } ?></h2>
                <ul class="list-inline">
                    <li class="date-dv"><?php echo $posted_data['bookingPicupDate']; ?></li>

                </ul>
            </div>
            <div class="col-sm-4">
                <a href="#" class="pull-right btn btn-default">Modify Search</a>
            </div>
        </div>
    <div class="clearfix"></div>
    
    <div class="rating-bar">
      <div class="row">
        <div class="col-sm-3">
            
            <p >Ratings  :
                <i class="fa fa-star starrate" id="1"aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="2" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="3" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="4" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="5" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> </p>
        </div>
           <div class="col-sm-3">
          <div class="custom-select-box tec-domain-cat1">
            <label>Rate :</label>
            <select class="selectpicker ratetypes" id="ratetype" onchange="transport_search(value, 'ratetype')" data-live-search="false" >
<!--              <option value="1">Low to High </option>-->
              <option value="2">High to low </option>
             
            </select>
          </div>
        </div>
           <div class="col-sm-3">
          <div class="custom-select-box tec-domain-cat1">
            <label>Fuel   :</label>
            <select class="selectpicker fueltypes" id="fueltype" onchange="transport_search(value, 'fueltype')"data-live-search="false" >
              
              <option value="Exclusives">Exclusive </option>
              <option value="Inclusive">Inclusive </option>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="custom-select-box tec-domain-cat1">
            <label>Price  :</label>
            <select class="selectpicker pricetypes" id="pricetype" onchange="transport_search(value, 'pricetype')"data-live-search="false" >
              <option value="1">Low to High </option>
              <option value="2">High to low </option>
             
            </select>
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearfix"></div>
    
    <div class="travelrating transporter" >
        <?php if (!empty($list)) { ?> 
         <?php foreach($list as $vehicle_list){  ?>     
 
    <div class="travelrating-block">
    <figure><img src="<?php echo transporter_image.$vehicle_list['logo'];?>" alt=""></figure>
    <div class="rating-detail">
    <div class="row">
    <div class="col-sm-8">
    <h2><?php echo$vehicle_list['companyName'] ?></h2>
  <p class="rating-p">Rating  :
        <?php if($vehicle_list['avgrate']!='0' && $vehicle_list['avgrate']!='') { ?>
         <?php if($vehicle_list['avgrate']>0 && $vehicle_list['avgrate']<=2 ) {?>
        <i class="fa fa-star" aria-hidden="true"></i> 
       
         <?php }?>
        <?php if($vehicle_list['avgrate']>=2 && $vehicle_list['avgrate']<3 ) {?>
         <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <?php }?>
        
         <?php if($vehicle_list['avgrate']>=3 && $vehicle_list['avgrate']<4 ) {?>
          <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
       
         <?php }?>
          <?php if($vehicle_list['avgrate']>=4 && $vehicle_list['avgrate']<5 ) {?>
           <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
      
        <?php }?>
          <?php if($vehicle_list['avgrate']>=5 ) {?>
        
        
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
      <?php }
      
      
          } else { 
          
          echo "(No rating has been given yet)";
          
       }
?>
    
    
    
    </p>

    </div>
    <div class="col-sm-4 price">
    Starting @<br><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $vehicle_list['totalCharge'] ?></span>
    </div>
    </div>
    <hr>
    <p><?php echo$vehicle_list['area'] ?> ,<?php echo$vehicle_list['city'] ?></p>
    <p><?php echo$vehicle_list['brandName'] ?> <?php echo$vehicle_list['modelName'] ?></p>
        <div class="clearfix"></div>
    <a  vehicleId="<?php echo $vehicle_list['vehicleId']; ?>" transporterId="<?php echo $vehicle_list['transporterId']; ?>" modelId="<?php echo $vehicle_list['modelId'];?>"totalCharge="<?php echo $vehicle_list['totalCharge'];?>" segments="<?php echo $vehicle_list['segments'];?>"vehicleType="<?php echo $vehicle_list['vehicleType'];?>"segementsName="<?php echo $vehicle_list['segementsName'];?>"avgrate="<?php echo $vehicle_list['avgrate'];?>"companyName="<?php echo $vehicle_list['companyName'];?>" class="btn btn-default booknowpage">Book Now</a>
    </div>
    </div>

             <?php
    }
} else {
    ?>

    <?php echo $data['message']; ?> 
<?php }
?>
  <ul class='page'>
    <?php
    if (!empty($number['totalpages'])) {
        $start = 0;
        $limit = 10;
        if (isset($_POST['pageNo'])) {

            $id = $_POST['pageNo'];
            $start = ($id - 1) * $limit;
        }
        
        elseif (isset($_GET['pageNo'])) {

            $id = $_GET['pageNo'];
            $start = ($id - 1) * $limit;
        }else {
            $id = 1;
        }

        $total = $number['totalpages'];
        if ($id > 1) {

            echo "<button id=" . ($id - 1) . " class='paginationshow button' onclick='transport_search(id,-100);'>PREVIOUS</button>";
        }
        ?>

        <?php
        for ($i = 1; $i <= $total; $i++) {
            if ($i == $id) {
                echo "<li class='current'>" . $i . "</li>";
            } else {
                echo "<li><button id=$i class='paginationshow'onclick='transport_search(id,-100);'>" . $i . "</button></li>";
            }
        }
        ?>

        <?php
        if ($id != $total) {
            ////Go to previous page to show next 10 items.
            echo "<button id=" . ($id + 1) . " class='paginationshow button' onclick='transport_search(id,-100);'>NEXT</button>";

            //echo "<a href=".base_url()."vehiclelist?page=".($id+1)."' class='paginationshow button'>NEXT</a>";
        }
    }
    ?>
</ul>
   
    </div>
    
    <input type="hidden" name="userId" id="userId" value="<?php echo $posted_data['userId'] ?>"/>
    
    <input type="hidden" name="journeyType" id="journeyType" value="<?php echo $posted_data['journeyType'] ?>"/>
    <input type="hidden" name="city_latitude" id="city_latitude" value="<?php echo $posted_data['city_latitude'] ?>"/>
    <input type="hidden" name="city_longitude" id="city_longitude" value="<?php echo $posted_data['city_longitude'] ?>"/>
    <input type="hidden" name="bookingPicupDate" id="bookingPicupDate" value="<?php echo $posted_data['bookingPicupDate'] ?>"/>
    <input type="hidden" name="bookingDropDate" id="bookingDropDate" value="<?php echo $posted_data['bookingDropDate'] ?>"/>
    <input type="hidden" name="bookedFrom" id="bookedFrom" value="<?php echo $posted_data['bookedFrom'] ?>"/>
    <input type="hidden" name="bookedTo" id="bookedTo" value="<?php echo $posted_data['bookedTo'] ?>"/>
    <input type="hidden" name="modelId" id="modelId" value="<?php echo $posted_data['modelId'] ?>"/>
    <input type="hidden" name="pick_drop_charges" id="pick_drop_charges" value="<?php echo $posted_data['pick_drop_charges'] ?>"/>
    
  </div>
</div>

<?php $this->load->view('google_api_common'); ?>
<?php $this->load->view('script'); ?>

<script type="text/javascript">
     localStorage.setItem('clickrating_value','');
 function transport_search(id, clickevent){
        
       // alert(clickevent);
       
        var modelId = $("#modelId").val();
        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();
        //alert(journeyType);
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var pick_drop_charges = $("#pick_drop_charges").val();

        var pageid = $(".paginationshow").attr("id");
        //Check the various Condition for Search
        
        if (clickevent == 'pricetype') {
            
            var pricefilter = id;
        } else {

            var pricefilter = $('#pricetype option:selected').val();
        }
        
          if (clickevent == 'fueltype') {
            
            var fuel_type = id;
        } else {

            var fuel_type = $('#fueltype option:selected').val();
        }


       if (clickevent == 'ratetype') {
            
            var rating = id;
        } else {

            var rating = $('#ratetype option:selected').val();
        }
        
         if (clickevent == 'starrate') {
            
            var ratingfilter = id;
            localStorage.setItem('clickrating_value',ratingfilter);
        } else {
                 localStorage.getItem('clickrating_value');
            var ratingfilter = localStorage.getItem('clickrating_value');//$('#ratetype option:selected').val();
        }
        
          if (pricefilter != "") {

            pageid = 1;

        }
         if (pricefilter != "" && clickevent=='-100' ) {

             var pageid = id;

        }
        
        if (pricefilter == "" && clickevent=='-100' ) {

             var pageid = id;

        }
        
//alert(ratingfilter);
        
      var dataString = 'modelId=' + modelId + '&journeyType=' + journeyType + '&city_latitude=' + city_latitude + '&city_longitude=' + city_longitude + '&bookingPicupDate=' + bookingPicupDate + '&bookingDropDate=' + bookingDropDate + '&bookedFrom=' + bookedFrom + '&bookedTo=' + bookedTo + '&pricefilter=' + pricefilter + '&fuel_type=' + fuel_type + '&rating=' + rating + '&ratingfilter=' + ratingfilter + '&pick_drop_charges=' + pick_drop_charges + "&pageNo=" + pageid + '&ajax_search_request=' + 1;

  //alert(dataString);
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>selfdrive_transporterlist",
            data: dataString,
            success: function (data) {

                $('.transporter').html(data);

            }
        });

    }
    
    
    
</script>
<!--book bow section-->
<script type="text/javascript">
  $(".booknowpage").click(function(){
    
        vehicleId=$(this).attr('vehicleId');
        transporterId=$(this).attr('transporterId');
        modelId=$(this).attr('modelId');
        totalCharge=$(this).attr('totalCharge');
        segments=$(this).attr('segments');
        vehicleType=$(this).attr('vehicleType');
        segementsName=$(this).attr('segementsName');
        companyName=$(this).attr('companyName');
        avgrate=$(this).attr('avgrate');
        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var pick_drop_charges = $("#pick_drop_charges").val();
        var userJounarytype = $("#journeyType").val();
       
        window.location="<?php echo base_url(); ?>selfdrivevehicle_detail?userId=" + userId + "&journeyType=" + journeyType + "&userJounarytype=" + userJounarytype + "&bookingPicupDate=" + bookingPicupDate + "&bookingDropDate=" + bookingDropDate + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo  + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&vehicleId=" + vehicleId  + "&transporterId=" + transporterId + "&modelId=" + modelId   + "&totalCharge=" + totalCharge  + "&segments=" + segments  + "&vehicleType=" + vehicleType+"&segementsName=" + segementsName  +"&companyName=" + companyName +"&avgrate=" + avgrate  +"&pick_drop_charges=" + pick_drop_charges + "&ajax_booknow_request=" + 1;
     
     
  }); 
   
    
 </script>   
 <!--book bow section-->