<?php $this->load->view('slider_common'); ?>
<?php $this->load->view('slider_search_form'); ?>
<div class="label-white white-lable-m" id="midsection">
    <div class="container searchtour">
        <div class="clearfix"></div>
        <div class="row datetime">
            <div class="col-sm-8">
                <h2><?php echo $posted_data['bookedFrom']; ?> 
                    <?php if (!empty($posted_data['bookedTo'])) {
                        ?>
                        to <?php echo $posted_data['bookedTo']; ?>
                    <?php } ?></h2>
                <ul class="list-inline">
                    <li class="date-dv"><?php echo $posted_data['bookingPicupDate']; ?></li>

                </ul>
            </div>
            <div class="col-sm-4">
                <a href="#" class="pull-right btn btn-default">Modify Search</a>
            </div>
        </div>
<div class="row">
    <div class="col-sm-4">
        
    </div>
    <div class="col-sm-4">
        
    </div>
         <div class="col-sm-4">
           <div class="custom-select-box tec-domain-cat1">
            <label>Car Type :</label>
    <select class="selectpicker cartypes" id="cartype" onchange="serach_data1(value, 'cartype')">
        <option  value="" >Select Car Type </option>
        <option  value="1">Automatic </option>
        <option  value="0" >Non-automatic </option>
    </select>
         </div>
              </div>
     </div>
        <div class="clearfix"></div>
        <div class="searchcat-tab"> 

            <ul class="row nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home" onClick="window.location.reload()">All</a></li>
                <?php foreach ($segment as $offer_list) { ?>
                    <li><a data-toggle="tab" class="segmentshow" segmentId="<?php echo $offer_list['id']; ?>" href="#<?php echo $offer_list['id']; ?>"><?php echo $offer_list['segments']; ?></a></li>
                <?php } ?>
            </ul>


            <div class="tab-content">
                <div id="home" class="tab-pane fade in active vehicle">
             <!--car type serach-->
           
                   
     
             
               
             <!--car type search ends-->
                    
                    
                    <div class="row">
                        <?php if (!empty($list)) { ?>       
                            <?php foreach ($list as $vehicle_list) { ?>   
                                <div class="col-md-3 col-sm-6">
                                    <div class="grid-dv">
                                        <figure style="width:203px;height:150px;"><img src="<?php echo model_image . strtolower($vehicle_list['ModelImage']); ?>" alt="" style="width:203px;height:125px;"></figure>
                                        <h2><?php echo $vehicle_list['brandName'] ?> <?php echo $vehicle_list['modelName']; ?></h2>
                                        <h3><?php echo $vehicle_list['segementsName']; ?> |<?php echo $vehicle_list['numbersOfseat']; ?> Seater</h3>
                                        <hr>
                                        <div class="clearfix"></div>
                                        <div class="row bottom">
                                            <div class="col-md-6 price">
                                                Starting @ <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $vehicle_list['totalCharge']; ?></span>
                                            </div>

                                            <div class="col-md-6">
                                                <button class="btn btn-default btn-block booknow" modelId="<?php echo $vehicle_list['modelId']?>">Book Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                        } else {
                            ?>

                            <?php echo $data['message']; ?> 
                        <?php }
                        ?>

                    </div>

                   <ul class='page'>
    <?php
    if (!empty($number['totalpages'])) {
        $start = 0;
        $limit = 10;
        if (isset($_POST['pageNo'])) {

            $id = $_POST['pageNo'];
            $start = ($id - 1) * $limit;
        }
        
        elseif (isset($_GET['pageNo'])) {

            $id = $_GET['pageNo'];
            $start = ($id - 1) * $limit;
        }else {
            $id = 1;
        }

        $total = $number['totalpages'];
        if ($id > 1) {

            echo "<button id=" . ($id - 1) . " class='paginationshow button' onclick='serach_data1(id,-100);'>PREVIOUS</button>";
        }
        ?>

        <?php
        for ($i = 1; $i <= $total; $i++) {
            if ($i == $id) {
                echo "<li class='current'>" . $i . "</li>";
            } else {
                echo "<li><button id=$i class='paginationshow'onclick='serach_data1(id,-100);'>" . $i . "</button></li>";
            }
        }
        ?>

        <?php
        if ($id != $total) {
            ////Go to previous page to show next 10 items.
            echo "<button id=" . ($id + 1) . " class='paginationshow button' onclick='serach_data1(id,-100);'>NEXT</button>";

            //echo "<a href=".base_url()."vehiclelist?page=".($id+1)."' class='paginationshow button'>NEXT</a>";
        }
    }
    ?>
</ul>
                </div>
                
            </div>
        </div>

    </div>           
    <!--input hidden feilds-->

    <input type="hidden" name="userId" id="userId" value="<?php echo $posted_data['userId'] ?>">

    <input type="hidden" name="journeyType" id="journeyType" value="<?php echo $posted_data['journeyType'] ?>">

    <input type="hidden" name="city_latitude" id="city_latitude" value="<?php echo $posted_data['city_latitude'] ?>">

    <input type="hidden" name="city_longitude" id="city_longitude" value="<?php echo $posted_data['city_longitude'] ?>">

    <input type="hidden" name="bookingPicupDate" id="bookingPicupDate"value="<?php echo $posted_data['bookingPicupDate'] ?>">

    <input type="hidden" name="bookingDropDate" id="bookingDropDate" value="<?php echo $posted_data['bookingDropDate'] ?>">

    <input type="hidden" name="bookedFrom" id="bookedFrom"value="<?php echo $posted_data['bookedFrom'] ?>">

    <input type="hidden" name="bookedTo" id="bookedTo" value="<?php echo $posted_data['bookedTo'] ?>">

    <input type="hidden" name="segments" id="segments"value="<?php echo $posted_data['segments'] ?>">

    <input type="hidden" name="automatic" id="automatic"value="<?php echo $posted_data['automatic'] ?>">

    <input type="hidden" name="pick_drop_charges" id="pick_drop_charges" value="<?php echo $posted_data['pick_drop_charges'] ?>">

    <input type="hidden" name="pageNo" id="pageNo" value="<?php echo $posted_data['pageNo'] ?>">


    <!--input hidden feilds-->


</div>   

<?php $this->load->view('google_api_common'); ?>
<?php $this->load->view('script'); ?>

<script type="text/javascript">
    $(".segmentshow").click(function () {

        segmentId = $(this).attr('segmentId');
        //alert(segmentId);
        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();

        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var automatic = $("#automatic").val();
        var pick_drop_charges = $("#pick_drop_charges").val();
        var pageNo = $("#pageNo").val();


        var dataString = 'segment=' + segmentId + '&journeyType=' + journeyType + '&city_latitude=' + city_latitude + '&city_longitude=' + city_longitude + '&bookingPicupDate=' + bookingPicupDate + '&bookingDropDate=' + bookingDropDate + '&bookedFrom=' + bookedFrom + '&bookedTo=' + bookedTo + '&automatic=' + automatic + '&pick_drop_charges=' + pick_drop_charges + '&ajax_segment_request=' + 1;

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>selfdrivelist",
            data: dataString,
            success: function (data) {

                $('.vehicle').html(data);

            }
        });
    });



</script>
<!--<script>
    $(".paginationshow2").click(function () {


        pageid = $(this).attr('pageid');
        //alert(pageid);
        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();

        //alert(journeyType);
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var automatic = $("#automatic").val();
        var pick_drop_charges = $("#pick_drop_charges").val();


        //alert(pageid);
        window.location = "<?php echo base_url(); ?>selfdrivelist?userId=" + userId + "&journeyType=" + journeyType + "&bookingPicupDate=" + bookingPicupDate + "&bookingDropDate=" + bookingDropDate + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo + "&pageNo=" + pageid + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&pick_drop_charges=" + pick_drop_charges + "&automatic=" + automatic + "&ajax_page_request=" + 1;

    });


</script>-->

<script type="text/javascript">
    
    $(".booknow").click(function () {
        
        //alert('hello');
        modelId=$(this).attr('modelId');
      
        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();
        //alert(journeyType);
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var pick_drop_charges = $("#pick_drop_charges").val();
         
        window.location ="<?php echo base_url(); ?>selfdrive_transporterlist?userId=" + userId +"&journeyType=" + journeyType + "&bookingPicupDate=" + bookingPicupDate + "&bookingDropDate=" + bookingDropDate + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo + "&modelId=" + modelId + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&pick_drop_charges=" + pick_drop_charges + "&ajax_transporter_request=" + 1;
    });
    
   
    </script>
    
    <script>
    function serach_data1(id, clickevent) {
       
       
       //alert(id);
           var sgid = $("li.active .segmentshow").attr("href");
           
           //alert(sgid);
           var sigmentid = '';
           var segments='';
           if(sgid !=undefined){
               
               sigmentid = sgid.split("#");
               segments=sigmentid[1];
           }
           else{
               
               segments='';
           }
           //alert(segments);
        //pager and car type section
        var pageid = $(".paginationshow").attr("id");
        
        if (clickevent == 'cartype') {
            
            var automatic1 = id;
        } else {

            var automatic1 = $('#cartype option:selected').val();
        }
        //page and car type section

        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var pick_drop_charges = $("#pick_drop_charges").val();

       // alert("automatic=" + automatic1);
       // alert("pageno=" + pageid);
        if (automatic1 != "") {

            pageid = 1;

        }
         if (automatic1 != "" && clickevent=='-100' ) {

             var pageid = id;

        }
        
        if (automatic1 == "" && clickevent=='-100' ) {

             var pageid = id;

        }
    
        var dataString = 'segment=' + segments + '&journeyType=' + journeyType + '&city_latitude=' + city_latitude + '&city_longitude=' + city_longitude + '&bookingPicupDate=' + bookingPicupDate + '&bookingDropDate=' + bookingDropDate + '&bookedFrom=' + bookedFrom + '&bookedTo=' + bookedTo + '&automatic=' + automatic1 + '&pick_drop_charges=' + pick_drop_charges + '&pageNo=' + pageid + '&ajax_segment_request=' + 1;

        //alert(dataString);
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>selfdrivelist",
            data: dataString,
            success: function (data) {

                $('.vehicle').html(data);

            }
        });

    }
</script>