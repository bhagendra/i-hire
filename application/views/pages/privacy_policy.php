<div class="label-white white-lable-m">
			<div class="container">
				
                <div class="clearfix profile-container fullwidth">
                <div class="row">
                <div class="col-md-12 col-xs-12"><h2 class="h2 btmmrgn">This Privacy Policy applies to ihire.in</h2>
                </div>
                </div>
                
                <div class="row">
                	<div class="col-md-12">
                    	
                            <p><b>ihire.in recognises the importance of maintaining your privacy. We value your privacy and appreciate your trust in us. This Policy describes how we treat user information we collect on https://www.ihire.in and other offline sources. This Privacy Policy applies to current and former visitors to our website and to our online customers. By visiting and/or using our website, you agree to this Privacy Policy.</b></p>
                            <p>ihire.in is a property of KGAV Solutions Private Limited, an Indian Company registered under the Companies Act, 1956 having its registered office at A-12/3, Naraina Industrial Area, Phase 1 New Delhi, Delhi - 110 028.</p>
                            <h4>Information we collect</h4>
                            
                            <p><b>Contact information.</b> We might collect your name, email, mobile number, phone number, street, city, state, pincode, country, credit card details, handset geo-location and ip address.</p>
                            <p><b>Payment and billing information.</b> We might collect your billing name, billing address and payment method when you make a booking. Credit card information will be obtained and processed by our online payment partner.</p>
                            
                            <p><b>Information you post.</b> We collect information you post in a public space on our website or on a third-party social media site belonging to travel/ transportation industry and ihire.in.</p>
                            <p><b>Demographic information.</b> We may collect demographic information about you, events you like, events you intend to participate in, tickets you buy, places you are visiting or intend to visit,  or any other information provided by you during the use of our website. We might collect this as a part of a survey also.</p>
                            
                            <p><b>Other information. </b>If you use our website, we may collect information about your IP address and the browser you're using. We might look at what site you came from, duration of time spent on our website, pages accessed or what site you visit when you leave us. We might also collect the type of mobile device you are using, or the version of the operating system your computer or device is running. </p>
                            <h4>We collect information in different ways.</h4>
                            
                            <p><b>We collect information directly from you.</b> We collect information directly from you when you register, make an inquiry or booking at ihire.in platform. We also collect information if you post a comment on our websites or ask us a question through phone or email.</p>
                            <p><b>We collect information from you passively.</b> We use tracking tools like Google Analytics, Google Webmaster, browser cookies and web beacons for collecting information about your usage of our website. </p>
                            
                            <p><b>We get information about you from third parties.</b> For example, if you use an integrated social media feature on our websites. The third-party social media site will give us certain information about you. This could include your name and email address.</p>
                            <h4>Use of your personal information</h4>
                            <p><b>We use information to contact you:</b> We might use the information you provide to contact you for confirmation of a purchase on our website or for other promotional purposes.</p>
                            <p><b>We use information to respond to your requests or questions.</b> We might use your information to confirm your registration, providing booking credentials and responding to your queries. </p>
                            
                            <p><b>We use information to improve our products and services.</b> We might use your information to customize your experience with us. This could include displaying content based upon your preferences.</p>
                            <p><b>We use information to look at site trends and customer interests.</b> We may use your information to make our website, products, and services better. We may combine information we get from you with information about you we get from third parties.</p>
                            <p><b>We use information for security purposes.</b> We may use information to protect our company, our customers, our partners, our websites and the State.</p>
                            
                            
                            <p><b>We use information for marketing purposes.</b> We might send you information about special promotions or offers. We might also tell you about new features, products and services. These might be our own offers, products and services, or third-party offering which we think you might find interesting. Or, for example, if you register, enquire or make booking on our platform we'll enrol you in our newsletter. </p>
                            
                            <p><b>We use information to send you transactional communications.</b> We might send you emails or SMS about your account, enquiries and purchases.</p>
                            <p>We use information as otherwise permitted by law.</p>
                            <h4>Sharing of information with third-parties</h4>
                            <p><b>We will share information with third parties who perform services on our behalf. </b>We share information with vendors who help us manage our online registration process or payment processors or transactional message processors. Some vendors may be located outside of India.</p>
                            
                            <p><b>We will share information with the event organizers.</b> We share your information with event organizers and other parties responsible for fulfilling the purchase obligation. The event organizers and other parties may use the information we give them as described in their privacy policies. </p>
                            <p><b>We will share information with our business partners.</b> This includes a third party service providers those who operate on our website, provide or sponsor promotional activities, Our partners use the information we give them as described in their privacy policies.</p>
                            <p><b>We may share information if we think we have to in order to comply with the law or to protect ourselves. We will share information to respond to a court order or subpoena.</b> We may also share it if a government agency or investigatory body requests. Or, we might also share information when we are investigating potential fraud. </p>
                            <p><b>We may share information with any successor to all or part of our business.</b> For example, if part of our business is sold we may give our customer list as part of that transaction.</p>
                            <p><b>We may share your information for reasons not described in this policy.</b> We will tell you before we do this.</p>
                            <h4>Email Opt-Out</h4>
                            <p><b>You can opt out of receiving our marketing emails.</b> To stop receiving our promotional emails, please email info@ihire.in. It may take about ten days to process your request. Even if you opt out of getting marketing messages, we will still be sending you transactional messages through email and SMS about your bookings.</p>
                            <h4>Third party sites </h4>
                            <p>If you click on one of the links to third party websites, you may be taken to websites we do not control. This policy does not apply to the privacy practices of those websites. Read the privacy policy of other websites carefully. We are not responsible for these third party sites.</p>
                            <h4>Grievance Officer</h4>
                            <p>n accordance with Information Technology Act 2000 and rules made there under, the name and contact details of the Grievance Officer are provided below:<br>
ihire hereby appoints ihire Support Manager as the grievance officer for the purposes of the rules drafted under the Information Technology Act, 2000, who may be contacted at support@ihire.in. You may address any grievances you may have in respect of this privacy policy or usage of your protected Information or other data to him.</p>
                            <h4>Updates to this policy</h4>
                            <p>rom time to time, we may update this Privacy Policy to reflect changes to our information practices. Any changes will be effective immediately upon the posting of the revised Privacy Policy. If we make any material changes, we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on the Services prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.</p>
                    </div>
                </div>
                
                
                
              		
                 </div>
			</div> 
		</div>

