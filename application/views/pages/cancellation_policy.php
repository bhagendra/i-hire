<div class="label-white white-lable-m">
			<div class="container">
				
                <div class="clearfix profile-container fullwidth">
                <div class="row">
                <div class="col-md-6 col-xs-6"><h1 class="h1 btmmrgn">Cancellation Policy</h1>
                </div>
                </div>
                
                
                
                 <div class="row">
                	<div class="col-md-12">
                    	
                        <ul class="commonlist">
                        <li>If the cancellation is done 24 hours before trip start time journey : Full refund of booking amount </li>
                        <li>If the cancellation is done between 24 and 12 hours before trip start time: 75 % refund of the booking amount</li>
                        <li>If the cancellation is less than 12 hours before trip start time : 50% refund of the booking amount </li>
                        <li>If the cancellation is after the trip start time: Zero refund of the booking amount</li>
                        </ul>
                        
                    </div>
                </div>
                
              		
                 </div>
			</div> 
		</div>