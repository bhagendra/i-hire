<div class="label-white white-lable-m">
			<div class="container">
				
                <div class="clearfix profile-container fullwidth">
                <div class="row">
                <div class="col-md-6 col-xs-6"><h1 class="h1 btmmrgn center">Website Disclaimer</h1>
                </div>
                </div>
                
                <div class="row">
                	<div class="col-md-12">
                    	<p>The information contained in this website is for general information purposes only. The information is provided by www.ihire.in, a property of KGAV Solutions Private Limited having its registered office at A - 12/3 Naraina Industrial Area, Phase – 1 110028. While we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
                        <p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website. </p>
                        
                        <p>Through this website you are able to link to other websites which are not under the control of KGAV Solutions Private Limited. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</p>
                         
                        <p>Every effort is made to keep the website up and running smoothly. However, KGAV Solutions Private Limited takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control. </p>
                        
                    </div>
                </div>
                
                 
                
              		
                 </div>
			</div> 
		</div>

