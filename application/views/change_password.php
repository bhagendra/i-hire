
<form method="post" action="<?php echo base_url(); ?>users_detail/change_password">
<!-- label white html start -->
		<div class="label-white white-lable-m">
			<div class="container">
				
                <div class="clearfix profile-container">
                <div class="row">
                <div class="col-md-6 col-xs-6"><h1 class="h1 btmmrgn">Change Password</h1></div>
                </div>
                      <?php if (isset($data)) { ?>
<CENTER><h3 style="color:green;"><?php echo $data['message'];?></h3></CENTER><br>
<?php } ?>
                
               <div class="form-group row noborder">
    <label class="col-sm-4 form-control-label">Old Password</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="oldpassword" id="oldpassword" placeholder="Enter old password">
    </div>
    <span id="old_help" class="has-error"></span>
  </div>
  
    <div class="form-group row noborder">
    <label class="col-sm-4 form-control-label">New Password</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="newpassword" id="newpassword" placeholder="Enter New Password">
    </div>
    <span id="new_help" class="has-error"></span>
  </div>
  
    <div class="form-group row noborder">
    <label class="col-sm-4 form-control-label">Confirm Password</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="conformpassword" id="conformpassword" placeholder="Confirm Password">
    </div>
    <span id="conform_help"class="has-error"></span>
     <span id="match_help"class="has-error"></span>
  </div>
  
    <div class="form-group row noborder">
   
   <div class="col-md-4">&nbsp;</div>
   <div class="col-md-8">
   	<input type="submit" name="submit"  id="submitpasswordcheck"class="btn btn-primary commonbtn">
   </div>
   
  </div>
                 </div>
			</div> 
		</div>
	</form>	<!-- label white html exit -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 
        <script>
    $("#submitpasswordcheck").click(function ()
    {
        $('.has-error').html('');
        var has_error = false;


        //var fromcity = $('.fromcity').val();
        var oldpassword = $('#oldpassword').val();
        var newpassword = $('#newpassword').val();
        var conformpassword = $('#conformpassword').val();
        
//        alert(from_date);
//        alert(to_date);
        if (oldpassword == '')
        {
            $('#old_help').html('Please Enter Old Passsword');
            has_error = true;
        }

       if(newpassword == '')
        {
            $('#new_help').html('Please Enter New Passsword');
            has_error = true;
        }
         if (conformpassword == '')
        {
            $('#conform_help').html('Please Enter Conform Passsword');
            has_error = true;
        }
        
        if(newpassword!=conformpassword){
            
             $('#match_help').html('New Password and Conform Password Must be same');
            has_error = true;
            
        }
//        if(to_date>=from_date ){
//             $('#to_date_help').html('End Date must be Grater than or Equal to Journey Date');
//            has_error = true;
//            
//        }
        if (has_error)
        {
 
            return false;
        }
 
    });

</script>