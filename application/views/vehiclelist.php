<?php $this->load->view('slider_common'); ?>
<?php $this->load->view('slider_search_form'); ?>
<!-- label white html start -->
<?php //prd($list); ?>
<div class="label-white white-lable-m"id="midsection">
    <div class="container searchtour">
        <div class="clearfix"></div>
        <div class="row datetime">
            <div class="col-sm-8">
                <h2><?php echo $posted_data['bookedFrom']; ?> 
                    <?php if (!empty($posted_data['bookedTo'])) {
                        ?>
                        to <?php echo $posted_data['bookedTo']; ?>
                    <?php } ?></h2>
                <ul class="list-inline">
                    <li class="date-dv"><?php echo $posted_data['bookingPicupDate']; ?></li>
                    <li class="time-dv"><?php echo $posted_data['sendTime']; ?></li>
                </ul>


            </div>
            <div class="col-sm-4">
                <a href="#" class="pull-right btn btn-default">Modify Search</a>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="searchcat-tab" > 

            <ul class="row nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">All</a></li>
                <?php foreach ($segment as $offer_list) { ?>
                    <li><a data-toggle="tab" class="segmentshow" segmentId="<?php echo $offer_list['id']; ?>" href="#<?php echo $offer_list['id']; ?>"><?php echo $offer_list['segments']; ?></a></li>
                <?php } ?>
            </ul>


            <div class="tab-content" >
                <div id="home" class="tab-pane fade in active">
                    <div class="row">
                        <?php if (!empty($list)) { ?>       
                            <?php foreach ($list as $vehicle_list) { ?>    

                                <div class="col-md-3 col-sm-6">
                                    <div class="grid-dv">
                                        <figure style="width:203px;height:150px;"><img src="<?php echo model_image . strtolower($vehicle_list['ModelImage']); ?>" alt="" style="width:203px;height:125px;"></figure>
                                        <h2>

                                            <?php
                                            if ($vehicle_list['vehicleType'] == 'cab'||$vehicle_list['vehicleType']=='CAB') {
                                                echo $vehicle_list['brandName'];
                                                echo'&nbsp;';
                                                echo $vehicle_list['modelName'];
                                            } elseif ($vehicle_list['vehicleType'] == 'coach') {
                                                echo $vehicle_list['segmentsName'];
                                            }
                                            ?></h2>
                                        <h3><?php echo $vehicle_list['segmentsName']; ?> |<?php echo $vehicle_list['numbersOfseat']; ?> Seater</h3>
                                        <hr>

                                        <div class="clearfix"></div>
                                        <div class="row bottom">
                                            <div class="col-md-6 price">
                                                Starting @     <?php if ($vehicle_list['afterdiscountPrice'] == '0' && $vehicle_list['discountprice'] == '0') { ?><span>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $vehicle_list['totalCharge']; ?>
                                                        <p></p>

                                                    </span>
                                                <?php } else {
                                                    ?>
                                                    <span>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo (round($vehicle_list['afterdiscountPrice'])); ?>
                                                        <p style="color:#000000;"><strike><?php echo $vehicle_list['totalCharge']; ?> </strike> <br>Save <?php echo
                                        (round($vehicle_list['discountprice']));
                                                    ?></p> 
                                                    </span> 

                                                <?php }
                                                ?>
                                            </div>

                                            <div class="col-md-6">

        <?php if ($vehicle_list['vehicleType'] == 'cab' || $vehicle_list['vehicleType'] == 'CAB') { ?>

                                                    <a modelId="<?php echo $vehicle_list['modelId']; ?>" class="btn btn-default btn-block transportsow" >Book Now</a>
                                                <?php } ?>
                                                <?php if ($vehicle_list['vehicleType'] == 'coach') { ?>
                                                    <a modelId="<?php echo $vehicle_list['segments']; ?>" class="btn btn-default btn-block transportsow" >Book Now</a>
        <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        } else {
                            ?>

                            <?php ; 
                            
                            if($data['code']='541'){
                               
                               $msg= $data['message']; 
                                echo $msg;
                                echo '<script language="javascript">';
echo 'alert("'.$msg.'")';
echo '</script>';
                            }
                            
                            ?> 
                        
                        
                        
<?php }
?>


                    </div>
                    <ul class='page'>
                        <?php
                        $start = 0;
                        $limit = 10;
                        if (!empty($number['totalpages'])) {
                            if (isset($_GET['pageNo'])) {

                                $id = $_GET['pageNo'];
                                $start = ($id - 1) * $limit;
                            } else {
                                $id = 1;
                            }

                            $total = $number['totalpages'];
                            if ($id > 1) {

                                echo "<button pageid=" . ($id - 1) . " class='paginationshow button'>PREVIOUS</button>";
                            }
                            ?>

                            <?php
                            for ($i = 1; $i <= $total; $i++) {
                                if ($i == $id) {
                                    echo "<li class='current'>" . $i . "</li>";
                                } else {
                                    echo "<li><button pageid=$i class='paginationshow'>" . $i . "</button></li>";
                                }
                            }
                            ?>
                            <?php
                            if ($id != $total) {
                                ////Go to previous page to show next 10 items.
                                echo "<button pageid=" . ($id + 1) . " class='paginationshow button'>NEXT</button>";

                                //echo "<a href=".base_url()."vehiclelist?page=".($id+1)."' class='paginationshow button'>NEXT</a>";
                            }
                        }
                        ?>
                    </ul>

                </div>

                <center>
                 <span style="display:none;" id="loadersId">
                    <img src="<?php echo base_url('assets/images/preloader.gif') ?>"/>
                </span>
                </center>
                    <?php foreach ($segment as $offer_list) { ?>
                    <div id="<?php echo $offer_list['id']; ?>" class="tab-pane fade vehicle">

    

                    </div>
<?php } ?>
            </div>
        </div>

    </div>           



</div>  


<!--html hiddeen feilds for search and For VEhicle list sSecond Page-->

<input type="hidden" id="userId"name="userId" value="<?php echo $posted_data['userId']; ?>"><br>
<input type="hidden" id="journeyType" name="journeyType" value="<?php echo $posted_data['journeyType']; ?>"><br>
<input type="hidden" id="userJounarytype"name="userJounarytype" value="<?php echo $posted_data['userJounarytype']; ?>"><br>
<input type="hidden" id="bookingPicupDate"name="bookingPicupDate" value="<?php echo $posted_data['bookingPicupDate']; ?>"><br>
<?php if ($posted_data['userJounarytype'] == 'local') { ?>
    <input type="hidden" id="hours" name="hours" value="<?php echo $posted_data['hours']; ?>"><br>
    <input type="hidden" id="kms"name="kms" value="<?php echo $posted_data['kms']; ?>"><br>
<?php } ?>
<input type="hidden" id="bookingDropDate"name="bookingDropDate" value="<?php echo $posted_data['bookingDropDate']; ?>"><br>
<input type="hidden" id="sendTime" name="sendTime" value="<?php echo $posted_data['sendTime']; ?>"><br>
<input type="hidden" id="bookedFrom" name="bookedFrom" value="<?php echo $posted_data['bookedFrom']; ?>"><br>
<input type="hidden" id="bookedTo"name="bookedTo" value="<?php echo $posted_data['bookedTo']; ?>"><br>
<input type="hidden" id="numbersOfseat" name="numbersOfseat" value="<?php echo $posted_data['numbersOfseat']; ?>"><br>
<input type="hidden" id="latitude"name="latitude" value="<?php echo $posted_data['latitude']; ?>"><br>
<input type="hidden" id="longitude"name="longitude" value="<?php echo $posted_data['longitude']; ?>"><br>
<input type="hidden" id="routeType"name="routeType" value="<?php echo $posted_data['routeType']; ?>"><br>
<input type="hidden" id="pageNo"name="pageNo" value="<?php echo $posted_data['pageNo']; ?>"><br>
<input type="hidden" id="totalkms"name="totalkms" value="<?php echo $posted_data['totalkms']; ?>"><br>
<input type="hidden" id="city_latitude"name="city_latitude" value="<?php echo $posted_data['city_latitude']; ?>"><br>
<input type="hidden" id="city_longitude"name="city_longitude" value="<?php echo $posted_data['city_longitude']; ?>"><br>






<!--html hidden Feilds for Search and For vehicle List Second Pages-->

<!-- label white html exit -->

<?php $this->load->view('google_api_common'); ?>
<?php $this->load->view('script'); ?>

<script type="text/javascript">
    segmentId = 0;

    $(".segmentshow").click(function () {
        $('#loadersId').show();
        segmentId = $(this).attr('segmentId');
        var userId = $("#userId").val();

        var journeyType = $("#journeyType").val();
        var userJounarytype = $("#userJounarytype").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var hours = $("#hours").val();
        var kms = $("#kms").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var sendTime = $("#sendTime").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var numbersOfseat = $("#numbersOfseat").val();
        var latitude = $("#latitude").val();
        var longitude = $("#longitude").val();
        var routeType = $("#routeType").val();
        var pageNo = $("#pageNo").val();
        var totalkms = $("#totalkms").val();
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();

        //alert(segmentId);

        var dataString = 'segment=' + segmentId + '&userId=' + userId + '&journeyType=' + journeyType + '&userJounarytype=' + userJounarytype + '&bookingPicupDate=' + bookingPicupDate + '&hours=' + hours + '&kms=' + kms + '&bookingDropDate=' + bookingDropDate + '&sendTime=' + sendTime + '&bookedFrom=' + bookedFrom + '&bookedTo=' + bookedTo + '&numbersOfseat=' + numbersOfseat + '&latitude=' + latitude + '&longitude=' + longitude + '&routeType=' + routeType + '&totalkms=' + totalkms + '&city_latitude=' + city_latitude + '&city_longitude=' + city_longitude + '&ajax_segment_request=' + 1;
        //alert(dataString);
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>vehiclelist",
            data: dataString,
            success: function (data) {
                $('#loadersId').hide();
                $('.vehicle').html(data);

            }
        });
    });

    modelId = 0;

    $(".transportsow").click(function () {
        //alert('hello');
        modelId = $(this).attr('modelId');
        var userId = $("#userId").val();

        var journeyType = $("#journeyType").val();
        var userJounarytype = $("#userJounarytype").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var hours = $("#hours").val();
        var kms = $("#kms").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var sendTime = $("#sendTime").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var numbersOfseat = $("#numbersOfseat").val();
        var latitude = $("#latitude").val();
        var longitude = $("#longitude").val();
        var routeType = $("#routeType").val();
        var pageNo = $("#pageNo").val();
        var totalkms = $("#totalkms").val();
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();

        //alert(totalkms);
        window.location = "<?php echo base_url(); ?>vehiclelist_second?modelId=" + modelId + "&userId=" + userId + "&journeyType=" + journeyType + "&userJounarytype=" + userJounarytype + "&bookingPicupDate=" + bookingPicupDate + "&hours=" + hours + "&kms=" + kms + "&bookingDropDate=" + bookingDropDate + "&sendTime=" + sendTime + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo + "&numbersOfseat=" + numbersOfseat + "&latitude=" + latitude + "&longitude=" + longitude + "&routeType=" + routeType + "&totalkms=" + totalkms + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&ajax_segment_request=" + 1;

    });
</script>
<script>
    $(".paginationshow").click(function () {
        // alert('helo');

        pageid = $(this).attr('pageid');
        var userId = $("#userId").val();
        var journeyType = $("#journeyType").val();
        var userJounarytype = $("#userJounarytype").val();
        var bookingPicupDate = $("#bookingPicupDate").val();
        var hours = $("#hours").val();
        var kms = $("#kms").val();
        var bookingDropDate = $("#bookingDropDate").val();
        var sendTime = $("#sendTime").val();
        var bookedFrom = $("#bookedFrom").val();
        var bookedTo = $("#bookedTo").val();
        var numbersOfseat = $("#numbersOfseat").val();
        var latitude = $("#latitude").val();
        var longitude = $("#longitude").val();
        var routeType = $("#routeType").val();
        var pageNo = $("#pageNo").val();
        var totalkms = $("#totalkms").val();
        var city_latitude = $("#city_latitude").val();
        var city_longitude = $("#city_longitude").val();

        //alert(pageid);
        window.location = "<?php echo base_url(); ?>vehiclelist/pagination_search?userId=" + userId + "&journeyType=" + journeyType + "&userJounarytype=" + userJounarytype + "&bookingPicupDate=" + bookingPicupDate + "&hours=" + hours + "&kms=" + kms + "&bookingDropDate=" + bookingDropDate + "&sendTime=" + sendTime + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo + "&numbersOfseat=" + numbersOfseat + "&latitude=" + latitude + "&longitude=" + longitude + "&routeType=" + routeType + "&pageNo=" + pageid + "&totalkms=" + totalkms + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&ajax_pagination_request=" + 1;

    });


</script>

<script>
// this function must be defined in the global scope
window.fadeIn = function(obj) {
    $(obj).fadeIn(1000);
}
</script>



