<?php $this->load->view('slider_common'); ?>
<?php $this->load->view('slider_search_form'); ?>
<?php //prd($list); ?>

<!-- label white html start -->
<div class="label-white white-lable-m" id="midsection">
  <div class="container tourrating">
    <div class="clearfix"></div>
         <div class="row datetime">
            <div class="col-sm-8">
            <h2><?php echo $posted_data['bookedFrom'];?> 
                <?php if(!empty($posted_data['bookedTo']))
                { ?>
                to <?php echo $posted_data['bookedTo'];?>
                <?php }?></h2>
            <ul class="list-inline">
            <li class="date-dv"><?php echo $posted_data['bookingPicupDate'];?></li>
            <li class="time-dv"><?php echo $posted_data['sendTime'];?></li>
            </ul>
            
            
            </div>
            <div class="col-sm-4">
            <a href="#" class="pull-right btn btn-default">Modify Search</a>
            </div>
            </div>
    <div class="clearfix"></div>
    <div class="rating-bar">
      <div class="row">
        <div class="col-sm-4">
            <p >Ratings  :
                <i class="fa fa-star starrate" id="1"aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="2" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="3" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="4" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> 
                <i class="fa fa-star starrate" id="5" aria-hidden="true" onclick="transport_search(id, 'starrate');"></i> </p>
        </div>
           <div class="col-sm-4">
           <div class="custom-select-box tec-domain-cat1">
            <label>Rate :</label>
            <select class="selectpicker ratetypes" id="ratetype" onchange="transport_search(value, 'ratetype')" data-live-search="false" >
<!--              <option value="1">Low to High </option>-->
              <option value="2">High to low </option>
             
            </select>
          </div>
        </div>
          
         <div class="custom-select-box tec-domain-cat1">
            <label>Price  :</label>
            <select class="selectpicker pricetypes" id="pricetype" onchange="transport_search(value, 'pricetype')"data-live-search="false" >
              <option value="1">Low to High </option>
              <option value="2">High to low </option>
             
            </select>
          </div>
      </div>
    </div>
    
    <div class="clearfix"></div>
    <?php if(!empty($list)) {?>
    <div class="travelrating transporter">
      <?php foreach($list as $vehicle_list){  ?>     
    <div class="travelrating-block">
    <figure><img src="<?php echo transporter_image.$vehicle_list['logo'];?>" alt=""><div class="certified">&nbsp;</div></figure>
    <div class="rating-detail">
    <div class="row">
    <div class="col-sm-8">
    <h2><?php echo$vehicle_list['companyName'] ?></h2>
      <p class="rating-p">Rating  :
        <?php if($vehicle_list['avgrate']!='0' && $vehicle_list['avgrate']!='') { ?>
         <?php if($vehicle_list['avgrate']>0 && $vehicle_list['avgrate']<=2 ) {?>
        <i class="fa fa-star" aria-hidden="true"></i> 
       
         <?php }?>
        <?php if($vehicle_list['avgrate']>=2 && $vehicle_list['avgrate']<3 ) {?>
         <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <?php }?>
        
         <?php if($vehicle_list['avgrate']>=3 && $vehicle_list['avgrate']<4 ) {?>
          <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
       
         <?php }?>
          <?php if($vehicle_list['avgrate']>=4 && $vehicle_list['avgrate']<5 ) {?>
           <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
      
        <?php }?>
          <?php if($vehicle_list['avgrate']>=5 ) {?>
        
        
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
        <i class="fa fa-star" aria-hidden="true"></i> 
      <?php }
      
      
          } else { 
          
          echo "(No rating has been given yet)";
          
       }
?>
    
    
    
    </p>

    
    
    
    
    </div>
   <div class="col-md-4 price">
                                                Starting @     <?php if ($vehicle_list['afterdiscountPrice'] == '0' && $vehicle_list['discountprice'] == '0') { ?><span>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $vehicle_list['totalCharge']; ?>
                                                        <p></p>
                                                        
                                                    </span>
                                                <?php } else {
                                                    ?>
                                                    <span>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo (round($vehicle_list['afterdiscountPrice'])) ; ?>
                                                        <p style="color:#000000;"><strike><?php echo $vehicle_list['totalCharge']; ?> </strike> <br>Save <?php echo 
                                                        (round($vehicle_list['discountprice'])); ?></p> 
                                                    </span> 

                                                <?php }
                                                ?>
                                            </div>
    </div>
    <hr>
    <p><?php echo$vehicle_list['area'] ?> ,<?php echo$vehicle_list['city'] ?></p>
    
    <?php if($posted_data['journeyType']=='cab') { ?>
    <p><?php echo$vehicle_list['brandName'] ?> <?php echo$vehicle_list['modelName'] ?></p>
    
    <?php } ?>
     <?php if($posted_data['journeyType']=='coach') { ?>
    <p><?php echo $vehicle_list['segmentsName'] ?> </p>
    
    <?php } ?>
    
    
        <div class="clearfix"></div>
    <a vehicleId="<?php echo $vehicle_list['vehicleId']?>" transporterId="<?php echo $vehicle_list['transporterId']?>" segmentsIds="<?php echo $vehicle_list['segments']?>"segmentsName="<?php echo $vehicle_list['segmentsName']?>" vehicleType="<?php echo $vehicle_list['vehicleType']?>" totalCharge="<?php echo $vehicle_list['totalCharge']?>" totalChargebydays="<?php echo $vehicle_list['totalChargebydays']?>"
 totalChargebykm="<?php echo $vehicle_list['totalChargebykm']?>" totalChargebykm="<?php echo $vehicle_list['totalChargebykm']?>" discountprice="<?php echo $vehicle_list['discountprice']?>" baseprice="<?php echo $vehicle_list['baseprice']?>" AddnightCharges="<?php echo $vehicle_list['AddnightCharges']?>" adddrivercharge="<?php echo $vehicle_list['adddrivercharge']?>"
      companyName="<?php echo $vehicle_list['companyName']?>" avgrate="<?php echo $vehicle_list['avgrate']?>" afterdiscountPrice="<?php echo $vehicle_list['afterdiscountPrice']?>"class="btn btn-default booknowpage">Book Now</a>
    </div>
    </div>
   <?php }} 

else{?>
    
   <?php echo @$data['message'];?> 
<?php }
?>
        <ul class='page paginationhide'>
        
    <?php 
    if(!empty($number['totalpages'])){
$start=0;
$limit=10;
if(isset($_GET['pageNo']))
{
    
    $id=$_GET['pageNo'];
    $start=($id-1)*$limit;
}
elseif(isset($_POST['pageNo']))
{
    
    $id=$_POST['pageNo'];
    $start=($id-1)*$limit;
}
else{
    $id=1;
}

$total=$number['totalpages'];
if($id>1)
{
    
    echo "<button id=".($id-1)." class='paginationshow button'onclick='transport_search(id,-100);'>PREVIOUS</button>";
}
?>
  
<?php

        for($i=1;$i<=$total;$i++)
        {
            if($i==$id && $i!==1) { echo "<li class='current'>".$i."</li>"; }
             
            else { echo "<li><button id=$i class='paginationshow'onclick='transport_search(id,-100);'>".$i."</button></li>"; }
        }
?>

      <?php 
if($id!=$total)
{
    ////Go to previous page to show next 10 items.
    echo "<button id=".($id+1)." class='paginationshow button'onclick='transport_search(id,-100);'>NEXT</button>";
    
     //echo "<a href=".base_url()."vehiclelist?page=".($id+1)."' class='paginationshow button'>NEXT</a>";
    }}
?>
</ul>
    </div>
    
  </div>
</div>

<!-- label white html exit --> 
<!--html hiddeen feilds for search and For VEhicle list sSecond Page-->

<input type="hidden" id="userId"name="userId" value="<?php echo $posted_data['userId']; ?>"><br>
<input type="hidden" id="journeyType" name="journeyType" value="<?php echo $posted_data['journeyType']; ?>"><br>
<input type="hidden" id="userJounarytype"name="userJounarytype" value="<?php echo $posted_data['userJounarytype']; ?>"><br>
<input type="hidden" id="bookingPicupDate"name="bookingPicupDate" value="<?php echo $posted_data['bookingPicupDate']; ?>"><br>
<?php if($posted_data['userJounarytype']=='local' ){ ?>
<input type="hidden" id="hours" name="hours" value="<?php echo $posted_data['hours']; ?>"><br>
<input type="hidden" id="kms"name="kms" value="<?php echo $posted_data['kms']; ?>"><br>
<?php } ?>
<input type="hidden" id="bookingDropDate"name="bookingDropDate" value="<?php echo $posted_data['bookingDropDate']; ?>"><br>
<input type="hidden" id="sendTime" name="sendTime" value="<?php echo $posted_data['sendTime'];?>"><br>
<input type="hidden" id="bookedFrom" name="bookedFrom" value="<?php echo $posted_data['bookedFrom']; ?>"><br>
<input type="hidden" id="bookedTo"name="bookedTo" value="<?php echo $posted_data['bookedTo']; ?>"><br>
<input type="hidden" id="numbersOfseat" name="numbersOfseat" value="<?php echo $posted_data['numbersOfseat']; ?>"><br>
<input type="hidden" id="latitude"name="latitude" value="<?php echo $posted_data['latitude']; ?>"><br>
<input type="hidden" id="longitude"name="longitude" value="<?php echo $posted_data['longitude']; ?>"><br>
<input type="hidden" id="routeType"name="routeType" value="<?php echo $posted_data['routeType']; ?>"><br>
<input type="hidden" id="pageNo"name="pageNo" value="<?php echo $posted_data['pageNo']; ?>"><br>
<input type="hidden" id="totalkms"name="totalkms" value="<?php echo $posted_data['totalkms']; ?>"><br>
<input type="hidden" id="city_latitude"name="city_latitude" value="<?php echo $posted_data['city_latitude']; ?>"><br>
<input type="hidden" id="city_longitude"name="city_longitude" value="<?php echo $posted_data['city_longitude']; ?>"><br>

<input type="hidden" id="modelId"name="modelId" value="<?php echo $list[0]['modelId']; ?>"><br>

<input type="hidden" id="segmentId"name="segmentId" value="<?php echo $list[0]['segments']; ?>"><br>


<?php $this->load->view('google_api_common'); ?>
<?php $this->load->view('script'); ?>

<script type="text/javascript">
     $(".booknowpage").click(function(){
     //alert('hello');
         vehicleId = $(this).attr('vehicleId');
         transporterId = $(this).attr('transporterId');
     
         segmentsIds = $(this).attr('segmentsIds');
         segmentsName = $(this).attr('segmentsName');
         vehicletype = $(this).attr('vehicletype');
         totalcharge = $(this).attr('totalcharge');
         totalchargebydays = $(this).attr('totalchargebydays');
         totalchargebykm = $(this).attr('totalchargebykm');
         discountprice = $(this).attr('discountprice');
         baseprice = $(this).attr('baseprice');
         addnightcharges = $(this).attr('addnightcharges');
         adddrivercharge = $(this).attr('adddrivercharge');
         
         companyName =$(this).attr('companyName');
         avgrate= $(this).attr('avgrate');
         afterdiscountPrice=$(this).attr('afterdiscountPrice');
          var userId = $("#userId").val();
          var journeyType = $("#journeyType").val();
          var userJounarytype = $("#userJounarytype").val();
          var bookingPicupDate = $("#bookingPicupDate").val();
          var hours = $("#hours").val();
          var kms = $("#kms").val();
          var bookingDropDate = $("#bookingDropDate").val();
          var sendTime = $("#sendTime").val();
          var bookedFrom = $("#bookedFrom").val();
          var bookedTo = $("#bookedTo").val();
          var numbersOfseat = $("#numbersOfseat").val();
          var latitude = $("#latitude").val();
          var longitude = $("#longitude").val();
          var routeType = $("#routeType").val();
          var pageNo = $("#pageNo").val();
          var totalkms = $("#totalkms").val();
          var city_latitude = $("#city_latitude").val();
          var city_longitude = $("#city_longitude").val();
 
window.location="<?php echo base_url(); ?>vehicle_detail?userId=" + userId + "&journeyType=" + journeyType + "&userJounarytype=" + userJounarytype + "&bookingPicupDate=" + bookingPicupDate + "&hours=" + hours + "&kms=" + kms + "&bookingDropDate=" + bookingDropDate + "&sendTime=" + sendTime + "&bookedFrom=" + bookedFrom + "&bookedTo=" + bookedTo + "&numbersOfseat=" + numbersOfseat + "&latitude=" + latitude + "&longitude=" + longitude + "&routeType=" + routeType +"&totalkms=" + totalkms + "&city_latitude=" + city_latitude + "&city_longitude=" + city_longitude + "&vehicleId=" + vehicleId + "&transporterId=" + transporterId +"&segmentsIds=" + segmentsIds +"&segmentsName=" + segmentsName +"&vehicletype=" + vehicletype +"&totalcharge=" + totalcharge +"&totalchargebydays="+totalchargebydays +"&totalchargebykm=" + totalchargebykm + "&discountprice=" + discountprice +"&baseprice=" + baseprice +"&addnightcharges="+addnightcharges +"&adddrivercharge="+ adddrivercharge +"&companyName="+ companyName +"&avgrate="+ avgrate +"&afterdiscountPrice="+ afterdiscountPrice +"&ajax_booknow_request=" + 1;

       //alert(window.location);   
     
    });
   </script>
         
  <script type="text/javascript">
      
             localStorage.setItem('clickrating_value','');
             function transport_search(id, clickevent){ 
          var userId = $("#userId").val();
          var journeyType = $("#journeyType").val();
          var userJounarytype = $("#userJounarytype").val();
          var bookingPicupDate = $("#bookingPicupDate").val();
          var hours = $("#hours").val();
          var kms = $("#kms").val();
          var bookingDropDate = $("#bookingDropDate").val();
          var sendTime = $("#sendTime").val();
          var bookedFrom = $("#bookedFrom").val();
          var bookedTo = $("#bookedTo").val();
          var numbersOfseat = $("#numbersOfseat").val();
          var latitude = $("#latitude").val();
          var longitude = $("#longitude").val();
          var routeType = $("#routeType").val();
          var pageNo = $("#pageNo").val();
          var totalkms = $("#totalkms").val();
          var city_latitude = $("#city_latitude").val();
           var city_longitude = $("#city_longitude").val();
          var modelId = $("#modelId").val();
          var segmentId = $("#segmentId").val();
          
          
           var pageid = $(".paginationshow").attr("id");
            if (clickevent == 'pricetype') {
            
            var pricefilter = id;
        } else {

            var pricefilter = $('#pricetype option:selected').val();
        }
        
        
       if (clickevent == 'ratetype') {
            
            var rating = id;
        } else {

            var rating = $('#ratetype option:selected').val();
        }
        
        
          if (clickevent == 'starrate') {
            
            var ratingfilter = id;
            localStorage.setItem('clickrating_value',ratingfilter);
        } else {
                 localStorage.getItem('clickrating_value');
            var ratingfilter = localStorage.getItem('clickrating_value');//$('#ratetype option:selected').val();
        }
        
        if (pricefilter != "") {

            pageid = 1;

        }
         if (pricefilter != "" && clickevent=='-100' ) {

             var pageid = id;

        }
        
        if (pricefilter == "" && clickevent=='-100' ) {

             var pageid = id;

        }
        
               
         var dataString = 'pricevalue='+ pricefilter + '&userId=' + userId + '&journeyType=' + journeyType + '&userJounarytype=' + userJounarytype + '&bookingPicupDate=' + bookingPicupDate + '&hours=' + hours + '&kms=' + kms + '&bookingDropDate=' + bookingDropDate + '&sendTime=' + sendTime + '&bookedFrom=' + bookedFrom + '&bookedTo=' + bookedTo + '&numbersOfseat=' + numbersOfseat + '&latitude=' + latitude + '&longitude=' + longitude + '&routeType=' + routeType + '&totalkms=' + totalkms + '&city_latitude=' + city_latitude + '&city_longitude=' + city_longitude + '&modelId=' + modelId + '&segmentId=' + segmentId +  '&ratingsearchvalue=' + rating +  '&ratingId=' + ratingfilter + "&pageNo=" + pageid + '&ajax_price_request=' + 1 ;
       // alert(dataString);
        $.ajax({ 
            type: 'POST',
            url: "<?php echo base_url(); ?>vehiclelist_second",
            data: dataString,
            success: function(data){ 
               
               $('.transporter').html(data) ;
             
             
            }
          }); 
          
         
             }
             </script>
             