<form method="post" action="<?php echo base_url(); ?>users_detail/update_user">     
 <div class="label-white white-lable-m">
			<div class="container">
				
                <div class="clearfix profile-container">
                <div class="row">
                <div class="col-md-6 col-xs-6"><h1 class="h1 btmmrgn">Edit Profile</h1></div>
                </div>
                   
               <div class="form-group row noborder">
    <label class="col-sm-3 form-control-label">Name</label>
    <div class="col-sm-9">
      <input type="text" name="userName"class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo $details['userName'] ?>">
    </div>
  </div>
  
    <div class="form-group row noborder">
    <label class="col-sm-3 form-control-label">Phone No.</label>
    <div class="col-sm-9">
      <input type="text" name="mobileNumber" class="form-control" id="inputEmail3" placeholder="9999999999" value="<?php echo $details['mobileNumber'] ?>">
    </div>
  </div>
  
    <div class="form-group row noborder">
    <label class="col-sm-3 form-control-label">Email</label>
    <div class="col-sm-9">
      <input type="text" name="email"class="form-control" id="inputEmail3" placeholder="abc@xyz.com" value="<?php echo $details['emailId'] ?>">
    </div>
  </div>
  
    <div class="form-group row noborder">
    <label class="col-sm-3 form-control-label">City</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" name="city" id="city_profile_edit" placeholder="Greater Noida" value="<?php echo $details['city'] ?>">
    </div>
    <input type="hidden" name="edit_Latitude" id="edit_Latitude">
    <input type="hidden" name="edit_Longitude" id="edit_Longitude">
  </div>
  
    <div class="form-group row noborder">
   
   <div class="col-md-3">&nbsp;</div>
   <div class="col-md-9">
   	<input type="submit" name="submit" class="btn btn-primary commonbtn">
   </div>
                   
  </div>
                 </div>
			</div> 
		</div>
      </form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"> </script> 
 <script type="text/javascript">
        function initialize_edit_profile() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('city_profile_edit'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress_edit_profile(address);
                }
            });
        }
        function codeAddress_edit_profile() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("city_profile_edit").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#edit_Latitude').val(Latitude);
                    $('#edit_Longitude').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize_edit_profile);


    </script>