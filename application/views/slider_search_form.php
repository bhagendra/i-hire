		
<!-- Booking now form wrapper html start --> 
<div class="booking-form-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="row">
                    <div class="form-wrap">
                        <div class="form-headr"></div>
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a data-toggle="tab" href="#cab"><i class="fa fa-car"></i> Cab</a></li>
                            <li><a data-toggle="tab" href="#coach"><i class="fa fa-bus"></i> Coach</a></li>
                            <li><a data-toggle="tab" href="#self"><i class="fa fa-car"></i> Self Drive</a></li>
                        </ul>


                        <div class="tab-content">

                            <!-- Cab Section starts-->                             
                            <div id="cab" class="tab-pane fade in active">

<div class="mCustomScrollbar scrollcontent fluid light" data-mcs-theme="inset-2-dark">

                            <form method="post" id="cabform"action="<?php echo base_url(); ?>vehiclelist">
                                <div class="form-select">
                              <input type="hidden" name="journeyType" value="cab">
                                    <div class="radiowrap">
                                        <div class="col-sm-4">
                                            <label><input type="radio" id="local" name="userJounarytype" value="local" checked="checked" class="cabfill"> Local</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><input type="radio" id="outstation"name="userJounarytype" value="outstation" class="cabfill"> OutStation</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="local" id="caboutstation">



                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 way">
                                            <div class="row">
                                                <select  name="routeType"class="selectpicker" data-live-search="false" >
                                                    <option value="OnewayOutratePerKm">One Way</option>
                                                    <option value="RoundTripOutratePerKm">Round Trip</option>

                                                </select>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-sm-12 custom-select-box tec-domain-cat1">
                                            <!--city search from Google Api Start-->
                                            <div class="row">
                                            <input type="text" class="form-control fromcity"name="city" id="location" value="New Delhi, Delhi, India"/>
                                           
                      <input type="hidden"  id="Latitude" name="Latitude" value="28.6139391">
                            <input type="hidden"  id="Longitude" name="Longitude" value="77.20902120000005">
                                            </div>
                                             <!--city search from Google Api Start-->
                                        </div>

                                        <div class="clearfix has-error" id="fromcity_help" ></div>

                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" id="from_date"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" id="from_time">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix" id="from_date_help"></div>
                                        <div class="clearfix" id="from_time_help"></div>

                                        <div class="col-sm-10 custom-select-box tec-domain-cat1 tocity">
                                            <div class="row">
                                                <input type="text" class="form-control tolocationcab"name="tocity" id="tolocation" value=""/>

    
                                            </div>
                                        </div>
   
                                        <div class="col-sm-2 custom-select-box tec-domain-cat1 morecabbuttons">
                                            <div class="row ">
                                                <div class="clearfix" ></div>
                                                <button type="button" class="btn blue" id="addButton">
			<i class="fa fa-plus-circle"></i>  </button>
                       
                                                </div>
                                        </div> 
                                                                             <div class="clearfix has-error" id="compare_help" ></div>
                                        <div class="clearfix"></div>
                                         
                  
                             <!--Dynamic City Add Start-->             
                                        <div class="clearfix morecity" id="TextBoxesGroup"></div>
                                      

<!--Dynamic City Add Ends-->

                                        <div class="col-sm-12 custom-select-box">

                                            <div class="row">
                                                <div class='input-group date inputdate'>
                                                    <input type='text' class="form-control custom-select-box" placeholder="To Date" name="to_date" id="to_date"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="clearfix" id="to_date_help"></div>




                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 package">
                                            <div class="row" overflow="true">
                                                <select class="selectpicker" name="package"data-live-search="false" >
                                                    <!--option>Select Package</option-->
                                                    <option value="4hrs">4 hrs. - 40 kms</option>
                                                    <option value="8hrs" selected>8 hrs. - 80 kms</option>
                                                    <option value="24hrs">Full day</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div> 
                                        <div class="form-button">
                                            
                                                                                       

  <button type="submit" name="submit" class="btn form-btn btn-lg btn-block" id="submitcab">Show Fares</button> 


                                        </div>

                                    </div>
                                





                                </div>
</form>
                            </div>
                        </div>

                            <!-- Cab Section Ends-->  
                            <!-- Coach Section starts--> 
                            <div id="coach" class="tab-pane fade">
<div class="mCustomScrollbar scrollcontent fluid light" data-mcs-theme="inset-2-dark">

<form method="post" id="coachform" action="<?php echo base_url(); ?>vehiclelist">
                                <div class="form-select">
 <input type="hidden" name="journeyType" value="coach">
                                    <div class="radiowrap">
                                        <div class="col-sm-4">
                                            <label><input type="radio" id="localcoach" name="userJounarytype" value="local" checked="checked"> Local</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><input type="radio" id="outstationcoach"name="userJounarytype" value="outstation"> OutStation</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="local" id="coachoutstation">



                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 coachway">
                                            <div class="row">
                                                <select class="selectpicker" data-live-search="false" name="routeType" >
                                                    <option value="OnewayOutratePerKm">One Way</option>
                                                    <option value="RoundTripOutratePerKm">Round Trip</option>

                                                </select>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-sm-12 custom-select-box tec-domain-cat1">
                                            <div class="row">
                                                      
                                            <input type="text" class="form-control coach_from_city"name="city" id="location_coach" value="New Delhi, Delhi, India"/>
                      <input type="hidden"  id="Latitude_coach" name="Latitude" value="28.6139391">
                            <input type="hidden"  id="Longitude_coach" name="Longitude" value="77.20902120000005">
                                            
                                            </div>
                                        </div>

                                        <div class="clearfix" id="coach_from_city_help"></div>

                                         <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" id="coach_from_date"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" id="coach_from_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix" id="coach_from_date_help"></div>

                                        <div class="col-sm-8 custom-select-box tec-domain-cat1 moretocitycoach">
                                            <div class="row">
                                                 <input type="text" class="form-control toloccoach"name="tocity" id="tolocationcoach" value="" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2 custom-select-box tec-domain-cat1 morebuttoncoach">
                                            <div class="row">
                                                <div class="clearfix" ></div>
                                                <button type="button" class="btn blue" id="addButton2">
			<i class="fa fa-plus-circle"></i>  </button>
                        
                                                </div>
                                        </div>  
                           
                               <div class="clearfix" id="coach_compare"></div>         
                                                                            
                              <div class="clearfix"></div>
                               
                               
                               
                             <!--Dynamic City Add Start-->             
                                        <div class="clearfix coachtocity" id="TextBoxesGroup2"></div>
                                        

<!--Dynamic City Add Ends-->

                                         <div class="col-sm-12 custom-select-box">

                                            <div class="row">
                                                <div class='input-group date inputdate'>
                                                    <input type='text' class="form-control custom-select-box" placeholder="To Date" name="to_date" id="coach_to_date"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="clearfix" id="coach_to_date_help"></div>
    <div class="col-sm-12 custom-select-box tec-domain-cat1 packagecoach">
                                            <div class="row">
                                                <select class="selectpicker" name="package"data-live-search="false" >
                                                    <option>Select Package</option>
                                                    <option value="4hrs">4 hrs. - 40 kms</option>
                                                    <option value="8hrs" selected>8 hrs. - 80 kms</option>
                                                    <option value="24hrs">Full day</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-sm-12 custom-select-box tec-domain-cat2">
                                            <div class="row">
                                                <select class="selectpicker" data-live-search="false" name="passenger" >
<?php
    for ($i=1; $i<=100; $i++)
    {
        ?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php
    }
?>
</select>
                                             
                                            </div>
                                        </div>
                                        
                                        

                                        
                                        
                                        
                                        <div class="clearfix"></div>

                                        <div class="form-button">
                                           
                                            
 <button type="submit" class="btn form-btn btn-lg btn-block" id="submitcoach">Show Fares</button>  

                                        </div>

                                    </div>





                                </div>


</form>
      </div>
                            </div>
                            <!-- Coach Section Ends-->                              
                            <!-- Self Drive Section starts-->                             
                            <div id="self" class="tab-pane fade">
<div class="mCustomScrollbar scrollcontent fluid light" data-mcs-theme="inset-2-dark">

                                <form method="get" action="<?php echo base_url(); ?>selfdrivelist"> 
                                    <input type="hidden" name="journeyType" value="selfdrive">

                                <div class="form-select">
                                    <div class="selfdrive">

                                        <div class="col-sm-12">
                                            <label>I am in</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12 custom-select-box tec-domain-cat1">
                                            

 <!--city search from Google Api Start-->
                                            <div class="row">
                                            <input type="text" class="form-control"name="city" id="locationself" value="New Delhi, Delhi, India"/>
                      <input type="hidden"  id="Latitudeself" name="Latitude" value="28.6139391">
                            <input type="hidden"  id="Longitudeself" name="Longitude" value="77.20902120000005">
                                            </div>
                                             <!--city search from Google Api Start-->
                                            </div>
                                       
 <div class="col-sm-12">
                                            <label>From Date</label>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<div class="col-sm-12">
                                            <label>To Date</label>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="to_date" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="to_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-sm-12">
                                            <label>Pick & Drop Charges</label>
                                        </div>
                                        <div class="clearfix"></div>

      <div class="col-sm-12 custom-select-box tec-domain-cat1">
          <div class="row">
          <select class="selectpicker" name="pick_drop_charges" data-live-search="false" >
          
              <option value="0">No</option>
              <option value="1">Yes</option>
          </select>
       </div>
      </div>

 </div>
                                        <div class="form-button">

  <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>



                                        </div>
                                    </div>
                                </div>
</form>
                            </div>
                             </div>
                            <!-- Self Drive Section Ends-->  
                        </div>




                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>


<!-- Booking now form wrapper html Exit -->		
