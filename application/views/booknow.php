<?php $this->load->view('slider_map'); ?>
<?php $this->load->view('slider_search_form'); ?>
<div class="container searchtour">
    <div class="clearfix"></div>
    <div class="row datetime">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4">
            <a href="#" class="pull-right btn btn-default">Modify Search</a>
        </div>
    </div>
</div>
<!-- label white html start -->
<div class="container"id="midsection">
    <div class="row">
        <h3 style="text-align:center;">Book a <?php echo $posted_data['journeyType']?>( <?php echo $company_details['companyname']?>)</h3>
        <div class="page29-wrap" >				
            <div class="col-sm-6">
                <div class="row booking">
                    <div class="short-banner">
                        <div class="short-banner-img"><img class="img-responsive" src="<?php echo base_url('assets/images/short-banner.jpg') ?>" alt=""/></div> 
                        <div class="short-banner-text"><a href=""><h4>I'm In The <?php echo $posted_data['bookedFrom']?> <br/> For A Few Days & I Need A <?php echo $posted_data['journeyType']?></h4></a></div> 
                    </div>
                    <h3>Passenger Details</h3>
                    <h5>Give complete Booking details</h5>
                    <form class="form-horizontal" method="get" action="<?php echo base_url(); ?>userbooking">
                        <div class="form-group"> 
                            <div class=" col-sm-12 third-form-wrap">

                                <select class="form-control"  name="numberOfPassengers_change_values" >

                                    <?php
                                    if ($posted_data['journeyType'] == 'coach') {
                                        for ($i = 1; $i <= 100; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>" 
                                            <?php
                                            if ($posted_data['numberOfPassengers'] == $i) {

                                                echo'selected';
                                            }
                                            ?>><?php echo $i; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>


                                    <?php
                                    if ($posted_data['journeyType'] == 'cab') {
                                        for ($i = 1; $i <= 4; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>" 
                                            <?php
                                            if ($i == 4) {

                                                echo'selected';
                                            }
                                            ?>><?php echo $i; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                </select> 


                            </div>
                        </div>

                        <div class="form-group"> 
                            <div class=" col-sm-12 third-form-wrap">
                                <input type="text"value="<?php echo $posted_data['customerName'] ?>" class="form-control" id="inputname2" name="customer_name" placeholder="First Name*" required> 
                            </div>
                        </div>


                        <div class="form-group"> 
                            <div class=" col-sm-12 third-form-wrap"> 
                                <input type="text" value="<?php echo $posted_data['customerMobileNumber'] ?>"class="form-control"  name="mobile_number" id="inputnumber2" placeholder="Mobile Number*" required>
                            </div>
                        </div> 

                        <div class="form-group"> 
                            <div class=" col-sm-12 third-form-wrap"> 
                                <input type="text" name="address" class="form-control" id="inputcountry2" placeholder="Reporting Address*" required>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class=" col-sm-12 third-form-wrap"> 
                                <textarea class="form-control" rows="3" value="Special Instruction" name="other_details"></textarea>
                            </div>
                        </div>


                        <!--input hidden types-->

                        <input type="hidden" name="userId"  value="<?php echo $posted_data['userId'] ?>">
                        <input type="hidden" name="transporterId"  value="<?php echo $posted_data['transporterId'] ?>" >
                        <input type="hidden" name="vehicleId"  value="<?php echo $posted_data['vehicleId'] ?>" >

                        <input type="hidden" name="journeyType"  value="<?php echo $posted_data['journeyType'] ?>" >  
                        <input type="hidden" name="bookingPicupDate"  value="<?php echo $posted_data['bookingPicupDate'] ?>" > 


                        <input type="hidden" name="bookingDropDate"  value="<?php echo $posted_data['bookingDropDate'] ?>" >
                        <input type="hidden" name="numberOfPassengers"  value="<?php echo $posted_data['numberOfPassengers'] ?>" >
                        <input type="hidden" name="hours"  value="<?php echo $posted_data['hours'] ?>" >
                        <input type="hidden" name="kms"  value="<?php echo $posted_data['kms'] ?>" >
                        <input type="hidden" name="bookedFrom"  value="<?php echo $posted_data['bookedFrom'] ?>" >
                        <input type="hidden" name="bookedTo"  value="<?php echo $posted_data['bookedTo'] ?>" >
                        <input type="hidden" name="customerMobileNumber"  value="<?php echo $posted_data['customerMobileNumber'] ?>" >
                        <input type="hidden" name="userJounarytype"  value="<?php echo $posted_data['userJounarytype'] ?>" >
                        <input type="hidden" name="vehicleModel"  value="<?php echo $posted_data['vehicleModel'] ?>" >
                        <input type="hidden" name="vehicleType"  value="<?php echo $posted_data['vehicleType'] ?>" >
                        <input type="hidden" name="vehicleSegments"  value="<?php echo $posted_data['vehicleSegments'] ?>" >
                        <input type="hidden" name="extrakmCharses"  value="<?php echo $posted_data['extrakmCharses'] ?>" >
                        <input type="hidden" name="extrahourseCharses"  value="<?php echo $posted_data['extrahourseCharses'] ?>" >
                        <input type="hidden" name="outstationbasePrice"  value="<?php echo $posted_data['outstationbasePrice'] ?>" >
                        <input type="hidden" name="outstationminKms"  value="<?php echo $posted_data['outstationminKms'] ?>" >
                        <input type="hidden" name="outstationNightCharges"  value="<?php echo $posted_data['outstationNightCharges'] ?>" >

                        <input type="hidden" name="totalCharge"  value="<?php echo $posted_data['totalCharge'] ?>" >
                        <input type="hidden" name="customerName"  value="<?php echo $posted_data['customerName'] ?>" >
                        <input type="hidden" name="email"  value="<?php echo $posted_data['email'] ?>" >
                        <input type="hidden" name="seeting"  value="<?php echo $posted_data['seeting'] ?>" >
                        <input type="hidden" name="segments"  value="<?php echo $posted_data['segments'] ?>" >
                        <input type="hidden" name="totalkms"  value="<?php echo $posted_data['totalkms'] ?>" >
                        <input type="hidden" name="modelName"  value="<?php echo $posted_data['modelName'] ?>" >
                        <input type="hidden" name="brandName"  value="<?php echo $posted_data['brandName'] ?>" >
                        <input type="hidden" name="companyname" value=" <?php echo $company_details['companyname']?>">
                <input type="hidden" name="avgrate" value=" <?php echo $company_details['avgrate']?>">




                        <!--input hidden forms ends-->
                        <div class="form-group">
                            <p>By Clicking Conform & Pay,I Agree with IHIRE<a href="<?php echo base_url();?>pages/terms" target="_blank"> terms of use</a></p>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <div class="completing-form-btnwrap completing-form-btnwrap2"><button type="submit" class="btn form-btn  btn-block">Conform & Pay</button></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="contact-details-wrap">
                        <div class="contact-details-wrap-top">
                            <div class="contact-details-car-wrap"><a href=""><img class="minicar4" src="<?php echo base_url('assets/images/minicar.png') ?>" alt=""/></a></div> 
                            <h4><?php echo $posted_data['brandName'] ?> <?php echo $posted_data['modelName'] ?></h4>
                        </div>
<?php if ($posted_data['journeyType'] == 'coach') { ?>
                            <div class="contact-details-wrap-middle text-center">
                                <div class="col-sm-offset-4 col-sm-2">
                                    <div class="item-details-man5 item-details-man3 "><a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/profile.png') ?>" alt=""></a></div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="clipart5 clipart3 "><a href="#" class=""><?php echo $posted_data['numberOfPassengers'] ?></a></div> 
                                </div> 

                            </div>
<?php } ?>

                        <div class="contact-details-wrap-bottom Car-Type-wrap-margin"> 
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Car Type:</span></div>
                                    <div class="Private-Car"><span><?php echo $posted_data['brandName'] ?> <?php echo $posted_data['modelName'] ?>(<?php echo $posted_data['vehicleSegments'] ?> <?php echo $posted_data['seeting'] ?> Seater)</span></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Booked By:</span></div>
                                    <div class="Private-Car"><span><?php echo $_SESSION['user_data']['userName'] ?></span></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Journey Type:</span></div>
                                    <div class="Private-Car"><span><?php echo $posted_data['userJounarytype'] ?></span></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>From:</span></div>
                                    <div class="Private-Car"><span><?php echo $posted_data['bookedFrom'] ?></span></div>
                                </div>
                            </div>
<?php if ($posted_data['userJounarytype'] == 'outstation') { ?> 
                                <div class="row">


                                    <div class="col-sm-12">
                                        <div class="Car-Type"><span>To:</span></div>
                                        <div class="Private-Car"><span>
                                                <?php
                                                if (!empty($_SESSION['destination']['destination']) && empty($_SESSION['destination']['destination1']) && empty($_SESSION['destination']['destination2']) && empty($_SESSION['destination']['destination3'])) {

                                                    echo $_SESSION['destination']['destination'];
                                                } elseif (!empty($_SESSION['destination']['destination']) && !empty($_SESSION['destination']['destination1']) && empty($_SESSION['destination']['destination2']) && empty($_SESSION['destination']['destination3'])) {

                                                    echo $_SESSION['destination']['destination1'];
                                                } elseif (!empty($_SESSION['destination']['destination']) && !empty($_SESSION['destination']['destination1']) && !empty($_SESSION['destination']['destination2']) && empty($_SESSION['destination']['destination3'])) {


                                                    echo $_SESSION['destination']['destination2'];
                                                } elseif (!empty($_SESSION['destination']['destination']) && !empty($_SESSION['destination']['destination1']) && !empty($_SESSION['destination']['destination2']) && !empty($_SESSION['destination']['destination3'])) {


                                                    echo $_SESSION['destination']['destination3'];
                                                }
                                                ?> 





                                            </span></div>
                                    </div>                                                                    
                                </div>
<?php } ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Pickup Date: </span></div>
                                    <div class="Private-Car"><span> <?php
                                            $currentDateTime = $book['bookingPicupDate'];
                                            $newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));

                                            echo $newDateTime
                                            ?></span></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Return Date:</span></div>
                                    <div class="Private-Car"><span><?php
                                            $currentDateTime = $book['bookingDropDate'];
                                            $newDateTime = date('M- d,Y', strtotime($currentDateTime));

                                            echo $newDateTime
                                            ?></span></div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Pickup Time:</span></div>
                                    <div class="Private-Car"><span><?php
                                            $currentDateTime = $book['bookingPicupDate'];
                                            $newDateTime = date('h:i A', strtotime($currentDateTime));

                                            echo $newDateTime
                                            ?></span></div>
                                </div>
                            </div> <br/>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Fare Details:</span></div>
                                    <div class="Private-Car">

<?php if ($posted_data['userJounarytype'] == 'outstation') { ?>

                                            <span><?php echo $posted_data['totalkms'] ?></span> 

                                        <?php } ?>

<?php if ($posted_data['userJounarytype'] == 'local') { ?>

                                            <span><?php echo $posted_data['hours'] ?>hrs <?php echo $posted_data['kms'] ?>kms</span> 

<?php } ?>

                                        <br/>
                                        
                                        
                                       
                                        <span>Per Km charge = <i class="fa fa-rupee"></i> 
                                            
                                            
                                          <?php  //echo $company_details['OnewayOutratePerKm'];?> 
                                            <?php if($posted_data['userJounarytype'] =='local') {
                                            
                                            echo $posted_data['extrakmCharses'];
                                                    
                                            }
                                            ?>
                                            <?php 
                                             //echo  $company_details['routeType'];
                                    if($posted_data['userJounarytype'] =='outstation'){
                                
                                      
                                   if(trim($company_details['routeType'])=='OnewayOutratePerKm'){
                                       
                                      echo  $company_details['OnewayOutratePerKm'];
                                      //echo "kk";
                                   }    
                                   
                                    if(trim($company_details['routeType'])=='RoundTripOutratePerKm'){
                                       
                                      echo  $company_details['RoundTripOutratePerKm'];
                                       // echo "mmmmm";
                                   }
                                        
                                          
                                            }
?>
                                        
                                            
                                            
                                        </span> <br/>
                                        
                                        
                                        
                                        
                                        
                                        <span>No. of days =   <?php
//Our dates
                                            $date1 = $book['bookingPicupDate'];
                                            $date2 = $book['bookingDropDate'];

//Convert them to timestamps.
                                            $date1Timestamp = strtotime($date1);
                                            $date2Timestamp = strtotime($date2);
                                            $date3 = date('Y-m-d', $date1Timestamp);

                                            $date4 = date('Y-m-d', $date2Timestamp);

                                            $datefirst = date_create($date3);
                                            $datelast = date_create($date4);
                                            $diff = date_diff($datefirst, $datelast);
                                            $totalday = $diff->format("%a");
                                            $totaldays=$totalday+1;  


//echo $days = floor($difference / (60*60*24) );
//echo $days;
                                            ?><?php echo$totaldays; ?> days</span> <br/>
                                        <?php if ($posted_data['userJounarytype'] == 'outstation') { ?>
                                            <span>Minimum billable kms per day = <?php echo $posted_data['outstationminKms'] ?> kms</span> <br/>
<?php } ?>

                                    </div>
                                </div>
                            </div> 

                            <!--	<div class="row">
                                            <div class="col-sm-12">
                                                    <div class="Car-Type"><span>Extra Charges:</span></div>
                                                    <div class="Private-Car">
                                                            <span>Tolls, parking and state permits as per actuals</span> <br/> 
                                                            <span>Extra Km beyond 3000 kms = <i class="fa fa-rupee"></i> 10.0/km</span> 
                                                    </div>
                                            </div>
                                    </div> -->

                        </div>

                        <div class="contact-details-wrap-footer">
                            <span>        <?php
                                if ($posted_data['userJounarytype'] == 'outstation') {

                                    if (!empty($_SESSION['routeType']['routeType'] == 'RoundTripOutratePerKm')) {

                                        echo 'Round Trip';
                                    } elseif (!empty($_SESSION['routeType']['routeType'] == 'OnewayOutratePerKm')) {

                                        echo 'One Way';
                                    }
                                }
                                ?> Fare:</span>
                            <h2><i class="fa fa-rupee"></i> <?php echo $book['totalCharge'] ?></h2>
                            <p>(Inclusive of All Taxes)</p>
                        </div>
                    </div>
                </div>
            </div>				
        </div>  
    </div>
</div>

<!-- label white html exit --> 


<?php $this->load->view('google_api_details'); ?>
<?php $this->load->view('script'); ?>

