<?php 
// Turn off all error reporting
error_reporting(0);?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>ihire</title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no"> 
		 
		<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/bootstrap-select.css')?>" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/css/bootstrap-datepicker.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/bootstrap-timepicker.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet"> 
		<link href="<?php echo base_url('assets/css/color.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/custom-responsive.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/slide-component.css')?>" rel="stylesheet">
        
		<link href="<?php echo base_url('assets/css/default.css')?>" rel="stylesheet">
                
                <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.mCustomScrollbar.css')?>">

		<!-- font awesome this template -->
		<link href="<?php echo base_url('assets/fonts/css/font-awesome.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/fonts/css/font-awesome.min.css')?>" rel="stylesheet">
			
	</head>
	<body>
		<div id="preloader">
			<div class="preloader-container">
				<img src="<?php echo base_url('assets/images/preloader.gif')?>" class="preload-gif" alt="preload-image">
			</div>
		</div>
		<div class="map-wapper-opacity">
			<div class="container">
				<div class="row">
					<div class="row">
						<div class="col-sm-4">
							<div class="language-opt tec-domain-cat8" id="translateElements">
								
                                <a href="#" class="applink" data-toggle="modal" data-target="#downloadpopup">Download ihire app</a>
                                
                                
							</div>
							<div class="call-us call-us2">
								<span class="img-circle img-circle01"><i class="fa fa-phone"></i></span> 
								<p>Call Us at xxxxxxxxx</p>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="logo-wraper">
								<div class="logo">
									<a href="<?php echo site_url();?>">
										<img src="<?php echo base_url('assets/images/logo.png')?>" alt="">
									</a>
								</div>
							</div>	
						</div>
						<div class="col-sm-4">
							<div id="languages" class="resister-social"> 
								
								<div class="login-register login">
                                                                    <?php
if(isset($_SESSION['user_data']['id']))
{
  echo '<a href="'.base_url().'login/logout" >Logout</a> '; 
}else
{
  echo '<a href="#" data-toggle="modal" data-target="#loginpopup">Login</a> '; 
  echo'<a href="#" data-toggle="modal" data-target="#registerpopup">Register</a>';
}

?>	
                                         
						
                                    
								</div>
								<div class="social-icon social-icon2">
									<a href="#" class="img-circle">
										<i class="fa fa-facebook"></i>
									</a>
									<a href="#" class="img-circle">
										<i class="fa fa-twitter"></i>
									</a>
									<a href="#" class="img-circle">
										<i class="fa fa-tumblr"></i>
									</a>
                                    <a href="#" class="img-circle">
										<i class="fa fa-instagram"></i>
									</a>
                                    <a href="#" class="img-circle">
										<i class="fa fa-youtube"></i>
									</a>
								</div>
							</div>
						</div>
                                            
                                                                                                                <?php
if(isset($_SESSION['user_data']['id']))
{ ?>
                                            
						<div class="col-sm-2">
							<div class="header-menu-wrap">
								<nav class="navbar navbar-default" role="navigation"> 
									<div class="main  collapse navbar-collapse"> 
										<div class="column">
											<div id="dl-menu" class="dl-menuwrapper">
												<a href="#" class="dl-trigger" data-toggle="dropdown">My Account <i class="fa fa-angle-down"></i></a> 
												<ul class="dl-menu">
													<li>
														<a href="<?php echo base_url(); ?>upcoming">My Trips</a>
													</li>
													<li>
														<a href="<?php echo base_url(); ?>users_detail/userdetail">My Profile</a>
													</li>
                                                                                              <li>
														<a href="<?php echo base_url(); ?>users_detail/change_password">Change Password</a>
													</li>          
                                                                                                        
													<li>
														<a href="#">My Reviews</a>
														
													</li>
													
												</ul>
											</div> 
										</div>
									</div> 
								</nav>
							</div>
						</div>
                                            
<?php } ?>
					</div>
				</div>
			</div>
		</div>