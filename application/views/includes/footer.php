	<!-- ================ footer html start ================ -->
		<div class="footer custom-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="row">
							<div class="menu-wrap">
								<div class="menu-col">
									<div class="menu-item">
										<ul>
											<li><a href="http://ihire.in" target="_blank">Corporate Website</a></li>
											<li><a href="#">Services</a></li>
											<li><a href="#">Media</a></li>
                                            <li><a href="#">Careers</a></li>
											<li><a href="#">Contact Us</a></li>
										</ul>
									</div> 
								</div>
								<div class="menu-col"> 
									<div class="menu-item">
										<ul>
											<li><a href="#">Affiliate/ Alliances</a></li>
											<li><a href="<?php echo base_url();?>pages/cancellation_policy">Cancellation policy</a></li>
											<li><a href="<?php echo base_url();?>pages/terms">Terms of use</a></li>
											<li><a href="<?php echo base_url();?>pages/privacy_policy">Privacy Policy</a></li>
                                            <li><a href="<?php echo base_url();?>pages/website_disclaimer">Disclaimer</a></li>
											
										</ul>
									</div> 
								</div>
								<div class="menu-col">
									<div class="menu-item">
										<ul>
											<li><a href="#">App Downloads</a></li>
											
										</ul>
									</div> 
								</div>
								<div class="menu-col">
									<div class="menu-item">
										<ul>
											<li><a href="">Transporter Zone</a></li>
										</ul>
									</div> 
								</div>
							</div>	
						</div>
					</div>
					<!-- <div class="col-sm-5">
						<div class="row">
							<div class="footer-map-bg">
								
							</div>
						</div>
					</div> -->
				</div>
				<div class="row">
					<div class="social-item">
						<a href="#" class="img-circle">
							<i class="fa fa-facebook"></i>
						</a>
						<a href="#" class="img-circle">
							<i class="fa fa-twitter"></i>
						</a>
						<a href="#" class="img-circle">
							<i class="fa fa-tumblr"></i>
						</a>
						<a href="#" class="img-circle">
							<i class="fa fa-instagram"></i>
						</a>
						<a href="#" class="img-circle">
							<i class="fa fa-youtube"></i>
						</a>
					</div>
					<div class="copy-right">
						<p><span><img src="<?php echo base_url('assets/images/footer-logo.png')?>"></span> &copy; Copyright 2016 | All Rights Rerved.</p>
					</div>
				</div>
			</div>
		</div>


<!-- Download Modal -->
<div id="downloadpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Download</h4>
      </div>
      <div class="modal-body">
        	
            <div class="form-group">
                	
                     <div class="right-inner-addon">
        <i class="fa fa-mobile-phone"></i>
        <input type="text" class="form-control" placeholder="Mobile" />
    </div>
                    
               
            </div>
            
            <div class="form-group">
            <div class="right-inner-addon">
        <i class="fa fa-user"></i>
        <input type="text" class="form-control" placeholder="Name" />
    </div>
                
            </div>
            
              <div class="form-group">
                
                <p>Choose your OS</p>                   
                
                <div class="row">
                	<div class="col-md-3">
                    	<label> <input type="radio" name="os"> <img src="<?php echo site_url('assets/images/andriod-logo.png')?>"></label> &nbsp;&nbsp;&nbsp;&nbsp; <label> <input type="radio" name="os"> <img src="<?php echo site_url('assets/images/ios-logo.png')?>"></label>
                    </div>
                    
                 
                    
                </div>
                
            	    
            </div>
            
              <div class="form-group">
                                
                                <input type="submit" value="Download" class="form-btn">
               
            </div>
            
            
      </div>
    
    </div>

  </div>
</div>

<!-- Login Modal -->
<div id="loginpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <form method="post"  id="login_form" name="form">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
        	
            <div class="form-group">
                	
                     <div class="right-inner-addon">
        <i class="fa fa-user"></i>
        <input type="text" class="form-control" name="email" id="email" placeholder="Username / Phone no." />
    </div>
                    
               
            </div>
            
            <div class="form-group">
                                     <div class="right-inner-addon">
        <i class="fa fa-lock"></i>
        <input type="password" class="form-control" name="password" id="password" placeholder="Password" />
    </div>
                
            </div>
            
             <div class="form-group">
           	<a href="#" data-toggle="modal" data-target="#forgotpopup">Forgot Password</a>    
                </div>
              <div class="form-group">                
        <input type="submit" value="Login" id="submit_login"class="form-btn">
               
            </div>
          
<span class="error" style="display:none"> Please Enter Valid Data</span>
<span class="login_success" style="color:#ff0000;"> </span>
   
      </div>

    </div>
      
      </form >
      
      
        			
  </div>
</div>

<!-- Register Modal -->
<div id="registerpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
	 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Register</h4>
      </div>
      <div class="modal-body">
          
            <form method="post" id="register_form" name="form">
        	
           <div class="form-group">
                	
                     <div class="right-inner-addon">
        <i class="fa fa-user"></i>
        <input type="text" class="form-control" name="userName" id="userName"placeholder="Name" />
    </div>
                    
               
            </div>
            
            <div class="form-group">
                                     <div class="right-inner-addon">
        <i class="fa fa-map-marker"></i>
        <input type="text" class="form-control" id="location_register" name="city" />
        <input type="hidden"  id="Latitude_register" name="Latitude" >
        <input type="hidden"  id="Longitude_register" name="Longitude">
    </div>
               
            </div>
            
              <div class="form-group">
                                     <div class="right-inner-addon">
        <i class="fa fa-mobile"></i>
        <input type="text" class="form-control"  name="mobileNumber" id="mobileNumber"placeholder="Add 10 digit mobile number" />
    </div>
               
            </div>
            
              <div class="form-group">
                                     <div class="right-inner-addon">
        <i class="fa fa-envelope"></i>
        <input type="text" class="form-control" placeholder="Email" name="email_register" id="email_register" />
    </div>
               
            </div>
            
              <div class="form-group">
                                     <div class="right-inner-addon">
        <i class="fa fa-lock"></i>
        <input type="password" class="form-control" placeholder="Password" id="password_register"  name="password_register"/>
    </div>
               
            </div>
                
                <span class="error" style="display:none"> Please Enter All Feilds</span>
<span class="register_success" style="color:#ff0000;"> </span>
            
               <div class="form-group">
         <input type="submit" value="Sign up" id="submit_register" class="form-btn">
               
            </div>
            
            
          </form>
            
      </div>
     
    </div>

  </div>
</div>

<!--Forgot Modal-->
        
<div id="forgotpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
	 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
      <div class="modal-body">
          
            <form method="post" id="forgot_form" name="forgot_form">
        	
           <div class="form-group">
                	
                     <div class="right-inner-addon">
        <i class="fa fa-user"></i>
        <input type="text" class="form-control" name="forgot_password" id="forgot_password"placeholder="Name" />
    </div>
                    
               
            </div>
            
            
                
                <span class="error" style="display:none"> Please Enter Feilds</span>
<span class="forgot_success" style="color:#ff0000;"> </span>
            
               <div class="form-group">
         <input type="submit" value="Send" id="submit_forgotpassword" class="form-btn">
               
            </div>
            
            
          </form>
            
      </div>
     
    </div>

  </div>
</div>

	<!-- ================ footer html Exit ================ -->
		<script src="<?php echo base_url('assets/js/jquery.js')?>"></script> 
		<script src="<?php echo base_url('assets/js/menu/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('assets/js/menu/modernizr.custom.js')?>"></script>
		<script src="<?php echo base_url('assets/js/menu/jquery.dlmenu.js')?>"></script>
		  
		<script src="<?php echo base_url('assets/js/bg-slider/jquery.imagesloaded.min.js')?>"></script>
		<script src="<?php echo base_url('assets/js/bg-slider/cbpBGSlideshow.min.js')?>"></script>  
		<script src="<?php echo base_url('assets/js/uikit.js')?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/jquery.stellar.js')?>"></script> 
		<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
		<script src="<?php echo base_url('assets/js/bootstrap-select.js')?>"></script>
		<script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/custom.js')?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/custom2.js')?>" type="text/javascript"></script> 
		<script src="<?php echo base_url('assets/js/menu/custom-menu.js')?>"></script>
		 
         <script type="text/javascript">
         	function setModalMaxHeight(element) {
  this.$element     = $(element);  
  this.$content     = this.$element.find('.modal-content');
  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight     = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
      'overflow': 'hidden'
  });
  
  this.$element
    .find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
  });
}

$('.modal').on('show.bs.modal', function() {
  $(this).show();
  setModalMaxHeight(this);
});

$(window).resize(function() {
  if ($('.modal.in').length != 0) {
    setModalMaxHeight($('.modal.in'));
  }
});

         </script>
 
  <script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js')?>"></script>  
    <script src="<?php echo base_url('assets/js/bootstrap-timepicker.min.js')?>"></script>  
  <script type="text/javascript">
   		$(function(){
			
			 $('.inputdate').datepicker({
                             autoclose: true,
							 startDate: '-0d'
                            });
			 
			  $('.inputtime').timepicker();
			
         
			});
   </script>
 <script src="<?php echo base_url('assets/js/jquery.mCustomScrollbar.concat.min.js')?>"></script>        
<!--modal Login script Start-->
<script type="text/javascript">

$("#submit_login").click(function() {
var email = $("#email").val();
var password = $("#password").val();
var dataString = 'email='+ email + '&password=' + password + '&ajax_request=' + 1;

//alert(dataString);

if(email==''  || password=='')
{
$('.success').fadeOut(200).hide();
$('.error').fadeOut(200).show();
}
else
{
$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>login",
data: dataString,
success: function(result){
console.log(result);
//alert(result);
$('.login_success').html(result);

}
});

setTimeout(function () {
       window.location.reload(); //will redirect to your blog page (an ex: blog.html)
    }, 500);
}
return false;
});

</script>

 <!--modal Login Script Ends--> 
 
 <!--modal Register Script Start-->

</script>
<script type="text/javascript" >

$("#submit_register").click(function() {
    
    //alert('register_form');
var userName = $("#userName").val();
var location_register = $("#location_register").val();
var Latitude_register = $("#Latitude_register").val();
var Longitude_register = $("#Longitude_register").val();
var email_register = $("#email_register").val();
var password_register = $("#password_register").val();
var mobileNumber = $("#mobileNumber").val();


var dataString = 'userName='+ userName + '&city=' + location_register + '&latitude=' + Latitude_register + '&longitude=' + Longitude_register + '&email='+ email_register + '&password=' + password_register + '&mobileNumber=' + mobileNumber + '&ajax_register_request=' + 1;
//alert(dataString);
if(userName=='' || location_register=='' || Latitude_register=='' || Longitude_register=='' || email=='' || password=='' ||mobileNumber=='')
{
$('.error').fadeOut(200).show();
}
else
{
$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>users/register",
data: dataString,
success: function(result){
console.log(result);
//alert(result);
$('.register_success').html(result);
}
});
}
return false;
});

</script>

<!--#############################################################-->
<script type="text/javascript">
        function initializeregister() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('location_register'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddressregister(address);
                }
            });
        }
        function codeAddressregister() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("location_register").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_register').val(Latitude);
                    $('#Longitude_register').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initializeregister);


    </script>

 <style>
     
     .pac-container { z-index: 100000; }
 </style>
 
  <!--modal Register Script Ends-->
  
  <!--modal Forgot Password Start-->
  <script type="text/javascript" >


$("#submit_forgotpassword").click(function() {
    
var forgot_password = $("#forgot_password").val();

var dataString = 'email='+ forgot_password + '&ajax_forgot_request=' + 1;
//alert(dataString);
if(forgot_password=='' )
{
$('.success').fadeOut(200).hide();
$('.error').fadeOut(200).show();
}
else
{
$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>users/forgot_password",
data: dataString,
success: function(result){
console.log(result);
//alert(result);
$('.forgot_success').html(result);
}
});
}
return false;
});
</script>
  <!--modal Forgot Password Ends-->
  
  <!--pagination link -->
   <style>
        #content
{
width: 900px;
margin: 0 auto;
font-family:Arial, Helvetica, sans-serif;
}
.page
{
margin: 0;
padding: 0;
}
.page li
{
list-style: none;
display:inline-block;
}
.page li a, .current
{
display: block;
padding: 5px;
text-decoration: none;
color: #8A8A8A;
}
.current
{
font-weight:bold;
color: #000;
}
.button
{
padding: 5px 15px;
text-decoration: none;
background: #333;
color: #F3F3F3;
font-size: 13PX;
border-radius: 2PX;
margin: 0 4PX;
display: block;
float: left;
}
    </style>
  <!--pagination link-->
  
  <!--script to load the mid section-->
    <script type="text/javascript">
$(document).ready(function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#midsection').offset().top
    }, 'fast');
});
</script>
  
  <!--Script to load the Mid Section ends-->
        </body> 
</html>