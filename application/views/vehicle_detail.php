<?php $this->load->view('slider_map'); ?>
<?php $this->load->view('slider_search_form');

//prd($posted_data);
?>
<div class="container searchtour">
    <div class="clearfix"></div>
    <div class="row datetime">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4">
            <a href="#" class="pull-right btn btn-default">Modify Search</a>
        </div>
    </div>
</div>
<!-- label white html start -->
<div class="label-white white-lable-m"id="midsection">

    <div class="container searchtour">

        <h3><?php echo $company_details['companyName'] ?>

<?php if ($rates['avgrate'] != '' && $rates['avgrate'] != '0') { ?>

                (


    <?php if ($company_details['avgrate'] > 0 && $company_details['avgrate'] <= 2) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 

                <?php } ?>
    <?php if ($company_details['avgrate'] >= 2 && $company_details['avgrate'] < 3) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                <?php } ?>

    <?php if ($company_details['avgrate'] >= 3 && $company_details['avgrate'] < 4) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 

                <?php } ?>
    <?php if ($company_details['avgrate'] >= 4 && $company_details['avgrate'] < 5) { ?>
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 

                <?php } ?>
    <?php if ($company_details['avgrate'] >= 5) { ?>


                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                    <i class="fa fa-star" aria-hidden="true"></i> 
                <?php } ?>)

            <?php } else {
                ?>

                (No Rating given yet)

            <?php }
            ?>
        </h3>
        <div class="clearfix"></div>
        <div class="searchcat-tab booking-tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#BookingInfo">Booking Info</a></li>
                <li><a data-toggle="tab" href="#FareDetails">Fare Details</a></li>
                <li><a data-toggle="tab" href="#VehicleDetails">Vehicle Details</a></li>
            </ul>

            <form method="get" action="<?php echo base_url() ?>booknow">
                <div class="tab-content">
                    <div id="BookingInfo" class="tab-pane fade in active">
                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Base Fare</h3>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">

<?php if ($terms['discountprice'] == '0') { ?>   

                                    <span class="price"><i class="fa fa-rupee"></i> <?php echo $bookinginfo['totalCharge'] ?></span>
                                <?php } else {
                                    ?>

                                    <span class="price"><i class="fa fa-rupee"></i> <?php echo (round($company_details['afterdiscountPrice'])) ?></span>
                                    <?php } ?>
                                <br>
                                (Plus service tax)<br>
                                All states service taxes, toll fees and parking will be extra on actual
                            </div>

                        </div>

                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Depart</h3>
                                <p class="detailtxt">


                                    <?php
                                    $currentDateTime = $bookinginfo['bookingPicupDate'];
                                    $newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));

                                    echo $newDateTime
                                    ?></p>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <?php if ($posted_data['userJounarytype'] == 'local') { ?>
                                    <p class="detailtxt"><?php echo $bookinginfo['hours'] ?>hrs - <?php echo $bookinginfo['kms'] ?>kms</p>

                                <?php } ?>
                                <h3>Return</h3>
                                <p class="detailtxt"><?php
                                $currentDateTime = $bookinginfo['bookingDropDate'];
                                $newDateTime = date('M- d,Y', strtotime($currentDateTime));

                                echo $newDateTime
                                ?></p>


                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <?php
                                if ($posted_data['userJounarytype'] == 'outstation') {

                                    if (!empty($_SESSION['routeType']['routeType'] == 'RoundTripOutratePerKm')) {

                                        echo '<h3>' . 'Round Trip' . '</h3>';
                                    } elseif (!empty($_SESSION['routeType']['routeType'] == 'OnewayOutratePerKm')) {

                                        echo '<h3>' . 'One Way' . '</h3>';
                                    }
                                }
                                ?>
                            </div>  
                        </div>

                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Pickup from</h3>

                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">

                                <p class="detailtxt"><?php echo $posted_data['bookedFrom']; ?></p>


                            </div>

                        </div>

<?php if ($posted_data['userJounarytype'] == 'outstation') { ?>
                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Drop</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">

                                    <p class="detailtxt">
    <?php
    if (!empty($_SESSION['destination']['destination']) && empty($_SESSION['destination']['destination1']) && empty($_SESSION['destination']['destination2']) && empty($_SESSION['destination']['destination3'])) {

        echo $_SESSION['destination']['destination'];
    } elseif (!empty($_SESSION['destination']['destination']) && !empty($_SESSION['destination']['destination1']) && empty($_SESSION['destination']['destination2']) && empty($_SESSION['destination']['destination3'])) {

        echo $_SESSION['destination']['destination1'];
    } elseif (!empty($_SESSION['destination']['destination']) && !empty($_SESSION['destination']['destination1']) && !empty($_SESSION['destination']['destination2']) && empty($_SESSION['destination']['destination3'])) {


        echo $_SESSION['destination']['destination2'];
    } elseif (!empty($_SESSION['destination']['destination']) && !empty($_SESSION['destination']['destination1']) && !empty($_SESSION['destination']['destination2']) && !empty($_SESSION['destination']['destination3'])) {


        echo $_SESSION['destination']['destination3'];
    }
    ?> 





                                    </p>


                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-2">
                                    <h3><?php echo $_SESSION['destination']['source']; ?></h3>

                                </div>
                                <div class="col-xxs-12 col-xs-6 col-md-1">
                                    <h3>&rarr;</h3>
                                    <h3> <?php echo $_SESSION['distance_cal']['distance_calculation1']; ?> </h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-2">

                                    <p class="detailtxt"><?php echo $_SESSION['destination']['destination']; ?></p>


                                </div>
    <?php if (!empty($_SESSION['destination']['destination1'])) { ?>
                                    <div class="col-xxs-12 col-xs-6 col-md-1">
                                        <h3>&rarr;</h3>
                                        <h3> <?php echo $_SESSION['distance_cal']['distance_calculation2']; ?> </h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-2">

                                        <p class="detailtxt"><?php echo $_SESSION['destination']['destination1']; ?></p>


                                    </div>
    <?php } ?>
    <?php if (!empty($_SESSION['destination']['destination2'])) { ?>
                                    <div class="col-xxs-12 col-xs-6 col-md-1">
                                        <h3>&rarr;</h3>
                                        <h3> <?php echo $_SESSION['distance_cal']['distance_calculation3']; ?> </h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-2">

                                        <p class="detailtxt"><?php echo $_SESSION['destination']['destination2']; ?></p>


                                    </div>
    <?php } ?>
                                <?php if (!empty($_SESSION['destination']['destination3'])) { ?>
                                    <div class="col-xxs-12 col-xs-6 col-md-1">
                                        <h3>&rarr;</h3>
                                        <h3> <?php echo $_SESSION['distance_cal']['distance_calculation4']; ?> </h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-2">

                                        <p class="detailtxt"><?php echo $_SESSION['destination']['destination3']; ?></p>


                                    </div>
    <?php }
    ?>

                                <?php if ($_SESSION['routeType']['routeType'] == 'RoundTripOutratePerKm') { ?>

                                    <div class="col-xxs-12 col-xs-6 col-md-1">
                                        <h3>&rarr;</h3>
                                        <h3> <?php echo $_SESSION['return_distances']['return_distances'];
                            ; ?> </h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-2">

                                        <h3><?php echo $_SESSION['destination']['source']; ?></h3>


                                    </div>
                                <?php } ?>


                            </div>
<?php } ?>

                    </div>

                    <div id="FareDetails" class="tab-pane fade">
                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Base Fare</h3>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">

<?php if ($terms['discountprice'] == '0') { ?>   

                                    <span class="price"><i class="fa fa-rupee"></i> <?php echo $bookinginfo['totalCharge'] ?></span>
                        <?php } else {
                            ?>

                                    <span class="price"><i class="fa fa-rupee"></i> <?php echo (round($company_details['afterdiscountPrice'])) ?> <strike><?php echo $bookinginfo['totalCharge']; ?> </strike></span>
<?php } ?>
                                <br>
                                (Plus service tax)<br>
                                All states service taxes, toll fees and parking will be extra on actual
                            </div>

                        </div>


                        <!-- Calculate the Days by difference between two dates-->
                                <?php
//Our dates
                                $date1 = $bookinginfo['bookingPicupDate'];
                                $date2 = $bookinginfo['bookingDropDate'];

//Convert them to timestamps.
                                $date1Timestamp = strtotime($date1);
                                $date2Timestamp = strtotime($date2);
                                $date3 = date('Y-m-d', $date1Timestamp);

                                $date4 = date('Y-m-d', $date2Timestamp);

                                $datefirst = date_create($date3);
                                $datelast = date_create($date4);
                                $diff = date_diff($datefirst, $datelast);
                                $totalday = $diff->format("%a");
                                 $totaldays=$totalday+1;


//echo $days = floor($difference / (60*60*24) );
//echo $days;
                                ?>

                        <!--Calculate Days between two dates endss-->


                        <div class="row brdrrow">
                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <h3>Package Fare</h3>
                                
                                <?php $base_calculate=$terms['baseprice']/$totaldays?>
                                
                                <p><?php echo $base_calculate ?>x <?php echo $totaldays; ?>days</p>
                            </div>

                            <div class="col-xxs-12 col-xs-6 col-md-3">
                                <span class="price"><i class="fa fa-rupee"></i> <?php echo $terms['baseprice'] ?></span>
                            </div>

                        </div>
                         <?php if ($posted_data['journeyType'] == 'cab') { ?>
                              <?php if ($posted_data['userJounarytype'] == 'local') { ?>
                                    <?php if ($terms['nightCharges'] != '0') { ?>
                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Night Charges</h3>
                                    
                                  
                                    
                                    <p><?php echo $terms['nightCharges'] ?>x <?php echo $totaldays; ?>days</p>
                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <span class="price"><i class="fa fa-rupee"></i> <?php 
                                    
                                    echo $total_night_charge=$terms['nightCharges']*$totaldays;
                                    ?></span>
                                </div>

                            </div>
                         <?php } } }?>

                        <?php if ($terms['adddrivercharge'] != '0') { ?>
                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Driver Charges</h3>
                                    
                                    <?php $adddrivercharge_calculate=$terms['adddrivercharge']/$totaldays?>
                                    
                                    <p><?php echo $adddrivercharge_calculate ?>x <?php echo $totaldays; ?>days</p>
                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <span class="price"><i class="fa fa-rupee"></i> <?php echo $terms['adddrivercharge'] ?></span>
                                </div>

                            </div>
<?php } ?>
<?php if ($terms['discountprice'] != '0') { ?>
                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Discount</h3>
                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <span class="price"><i class="fa fa-rupee"></i>  <?php echo
    (round($terms['discountprice']));
    ?></span>
                                </div>

                            </div>
<?php } ?>

                        <div class="row rowheading">
                            <div class="col-md-12">
                                <h2 class="">Inclutions &amp; Extras </h2>
                            </div>
                        </div>

                        <?php if ($posted_data['userJounarytype'] == 'outstation') { ?>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Total Journey KM</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><?php echo $terms['totalkms'] ?> </p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Minimum KM/Day</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><?php echo $terms['MinKmPerDay'] ?> Kms</p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Total MInimum KM Chargeable</h3>
                                    <p><?php echo $terms['MinKmPerDay'] ?>x <?php echo $totaldays; ?>days</p>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt">
    <?php $minkm = $terms['MinKmPerDay'];
    $total = $minkm * $totaldays
    ?>
    <?php echo $total ?> Kms</p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Billed Km</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"> <?php
                                    if($total>$terms['totalkms']){
                                        
                                    echo $total;
                                    
                                    }
                                    else{
                                    echo $terms['totalkms'];   
                                        
                                    }
                                    ?>  </p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Extra Km Rate</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><i class="fa fa-rupee"></i>  <?php 
                                    
                                    if($company_details['routeType']=='OnewayOutratePerKm')
                                    
                                    echo $terms['OnewayOutratePerKm']; ?> 
                                    <?php 
                                    
                                    if($company_details['routeType']=='RoundTripOutratePerKm')
                                    
                                    echo $terms['RoundTripOutratePerKm']; ?></p>
                                </div>

                            </div>
                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Driver Charge</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><i class="fa fa-rupee"></i>  <?php echo $terms['DriverCharges']; ?>  </p>
                                </div>

                            </div>

<?php } ?>
<?php if ($posted_data['userJounarytype'] == 'local') { ?>
                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Package </h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><?php echo $terms['hours'] ?> hrs <?php echo $terms['kms'] ?> Kms</p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Extra Km Rate</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><i class="fa fa-rupee"></i> <?php echo $terms['extraKmsCharges'] ?></p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Extra Hour Rate</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><i class="fa fa-rupee"></i> <?php echo $terms['extraHourCharges'] ?></p>
                                </div>

                            </div>

                            <div class="row brdrrow">
                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <h3>Night Charge</h3>

                                </div>

                                <div class="col-xxs-12 col-xs-6 col-md-3">
                                    <p class="detailtxt"><i class="fa fa-rupee"></i> <?php echo $terms['nightCharges'] ?></p>
                                </div>

                            </div>

<?php } ?>
                    </div>
                    <div id="VehicleDetails" class="tab-pane fade">

                        <div class="row">
                            <div class="col-md-7">  <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>Segment</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php echo $posted_data['vehicleSegments'] ?></p>
                                    </div>

                                </div>
<?php if ($posted_data['journeyType'] == 'cab') { ?>
                                    <div class="row brdrrow">
                                        <div class="col-xxs-12 col-xs-6 col-md-5">
                                            <h3>Vehicle</h3>

                                        </div>

                                        <div class="col-xxs-12 col-xs-6 col-md-5">
                                            <p class="detailtxt"><?php echo$vehicledetails['brandName'] ?> <?php echo$vehicledetails['modelName'] ?></p>
                                        </div>
                                    </div>
<?php } ?> 

                                <div class="row brdrrow">
                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <h3>Seating Capacity</h3>

                                    </div>

                                    <div class="col-xxs-12 col-xs-6 col-md-5">
                                        <p class="detailtxt"><?php echo$vehicledetails['numbersOfseat'] ?></p>
                                    </div>

                                </div></div>

                            <div class="col-md-5">
                                <div class="imgsection">
                                    <img src="http://ihire.in/iHire_api/assets/upload/model/<?php echo strtolower($vehicledetails['ModelImage']); ?>" class="img-responsive">
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <input type="hidden" name="userId" value="<?php echo $posted_data['userId'] ?>"> 

                <input type="hidden" name="transporterId" value="<?php echo $posted_data['transporterId'] ?>">    
                <input type="hidden" name="vehicleId" value="<?php echo $posted_data['vehicleId'] ?>">         <input type="hidden" name="journeyType" value="<?php echo $posted_data['journeyType'] ?>">    
                <input type="hidden" name="bookingPicupDate" value="<?php echo $posted_data['bookingPicupDate'] ?>"> 
                <input type="hidden" name="bookingDropDate" value="<?php echo $posted_data['bookingDropDate'] ?>"> 

                <input type="hidden" name="bookingDropDate" value="<?php echo $posted_data['bookingDropDate'] ?>"> 

                <input type="hidden" name="numberOfPassengers" value="<?php echo $posted_data['numberOfPassengers'] ?>"> 

                <input type="hidden" name="hours" value="<?php echo $posted_data['hours'] ?>"> 
                <input type="hidden" name="kms" value="<?php echo $posted_data['kms'] ?>">

                <input type="hidden" name="bookedFrom" value="<?php echo $posted_data['bookedFrom'] ?>"> 
                <input type="hidden" name="bookedTo" value="<?php echo $posted_data['bookedTo'] ?>"> 

                
                <input type="hidden" name="userJounarytype" value="<?php echo $posted_data['userJounarytype'] ?>"> 
                <input type="hidden" name="vehicleModel" value="<?php echo $vehicledetails['vehicleModel'] ?>"> 

                <input type="hidden" name="vehicleType" value="<?php echo $posted_data['journeyType'] ?>"> 
                <input type="hidden" name="vehicleSegments" value="<?php echo $posted_data['vehicleSegments'] ?>"> 

                <input type="hidden" name="vehicleId" value="<?php echo $posted_data['vehicleId'] ?>"> 

                <input type="hidden" name="extrakmCharses" value="<?php echo $terms['extraKmsCharges'] ?>"> 

                <input type="hidden" name="extrahourseCharses" value="<?php echo $terms['extraHourCharges'] ?>">
                <input type="hidden" name="outstationbasePrice" value="<?php echo $terms['outBasePrice'] ?>">

                <input type="hidden" name="outstationminKms" value="<?php echo $terms['MinKmPerDay'] ?>">
                <input type="hidden" name="outstationNightCharges" value="<?php echo $terms['nightCharges'] ?>">
                <input type="hidden" name="totalCharge" value="<?php echo $terms['totalCharge'] ?>">
                <input type="hidden" name="totalCharge" value="<?php echo $terms['totalCharge'] ?>">

                <input type="hidden" name="seeting" value="<?php echo$vehicledetails['numbersOfseat'] ?> ">
                <input type="hidden" name="segments" value="<?php echo$vehicledetails['segments'] ?> "> 
                <input type="hidden" name="totalkms" value="<?php echo $terms['totalkms'] ?> ">
                <input type="hidden" name="brandName" value="<?php echo $vehicledetails['brandName'] ?> ">           
                <input type="hidden" name="modelName" value="<?php echo $vehicledetails['modelName'] ?> ">   
                <input type="hidden" name="companyname" value=" <?php echo $company_details['companyName']?>">
                <input type="hidden" name="avgrate" value=" <?php echo $company_details['avgrate']?>">
                <input type="hidden" name="routeType" value="<?php echo $company_details['routeType'] ?> "/> 
                
                <input type="hidden" name="OnewayOutratePerKm" value="<?php echo $terms['OnewayOutratePerKm'] ?> "/>  
                
                
                <input type="hidden" name="RoundTripOutratePerKm" value="<?php echo $terms['RoundTripOutratePerKm'] ?> "/>
                
                <?php if(!empty ($_SESSION['user_data']['id'])){ ?>
                
                <input type="hidden" name="customerMobileNumber" value="<?php echo $_SESSION['user_data']['mobileNumber'] ?>"> 
                
                <input type="hidden" name="customerName" value="<?php echo $_SESSION['user_data']['userName'] ?>">
                <input type="hidden" name="email" value="<?php echo $_SESSION['user_data']['emailid'] ?>">

                <?php } ?>


                <p>NOTE: Transporter Will Charge Garage to Garage</p>              

                <div class="clearfix"></div>
                <div>
                    
                     <?php if(!empty($_SESSION['user_data']['id'])) {  ?>
                    <input type="submit" value="BOOK NOW" class="btn btn-default">
                
                <?php }
          else
          {?>
             <a href="#" class="btn btn-default"data-toggle="modal" data-target="#loginpopup">Login</a>
              
       <?php    }
          
          
          ?>
                
                </div>
            </form>
        </div>

    </div>
</div>

<!-- label white html exit --> 


<?php $this->load->view('google_api_details'); ?>
<?php $this->load->view('script'); ?>
