<!--Slider Content Start-->
<div class="google-image">
    <div class="bg-ground"> 
        <div class="main">
            <ul id="cbp-bislideshow" class="cbp-bislideshow">
                <li>
                    <div class="slider-img-wrap"><img src="<?php echo base_url('assets/images/1.jpg') ?>" alt="image01"/></div>
                    <div class="slider-text-content"> 
                        <h1>ANYTIME,   <br/>ANYWHERE!</h1>
                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Proin gravida nibh vel velit auctor aliquet sollicitudin.</p>
                            <p><i class="fa fa-custom fa-circle-o"></i>Qnec sagittis bibendum auctor sem nibh id.</p>
                            <p><i class="fa fa-custom fa-circle-o"></i>Rit amet nibh vulputate cursus nisi elit.</p>
                        </div> 
                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="<?php echo base_url('assets/images/2.jpg') ?>" alt="image01"/></div>
                    <div class="slider-text-content"> 
                        <h1>book your taksi <br/>online now</h1> 
                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="<?php echo base_url('assets/images/3.jpg') ?>" alt="image01"/></div>
                    <div class="slider-text-content"> 
                        <h1>a design that<br/>made with love</h1> 
                    </div>
                </li>

            </ul> 
        </div>
    </div>
</div>

<!--Slider Content Ends-->		
<!-- Booking now form wrapper html start --> 
<div class="booking-form-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="row">
                    <div class="form-wrap">
                        <div class="form-headr"></div>
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a data-toggle="tab" href="#cab"><i class="fa fa-car"></i> Cab</a></li>
                            <li><a data-toggle="tab" href="#coach"><i class="fa fa-bus"></i> Coach</a></li>
                            <li><a data-toggle="tab" href="#self"><i class="fa fa-car"></i> Self Drive</a></li>
                        </ul>


                        <div class="tab-content">

                            <!-- Cab Section starts-->                             
                            <div id="cab" class="tab-pane fade in active">



                            <form method="post" action="<?php echo base_url(); ?>vehiclelist">
                                <div class="form-select">
                              <input type="hidden" name="journeyType" value="cab">
                                    <div class="radiowrap">
                                        <div class="col-sm-4">
                                            <label><input type="radio" id="local" name="userJounarytype" value="local" checked="checked"> Local</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><input type="radio" id="outstation"name="userJounarytype" value="outstation"> OutStation</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="local" id="caboutstation">



                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 way">
                                            <div class="row">
                                                <select class="selectpicker" data-live-search="false" >
                                                    <option>One Way</option>
                                                    <option>Round Trip</option>

                                                </select>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-sm-12 custom-select-box tec-domain-cat1">
                                            <!--city search from Google Api Start-->
                                            <div class="row">
                                            <input type="text" class="form-control fromcity"name="city" id="location"/>
                                            <div class="row">
                                            <span id="fromcity_help" class="help-block has-error"> </span>
                                            </div>
                      <input type="hidden"  id="Latitude" name="Latitude" >
                            <input type="hidden"  id="Longitude" name="Longitude">
                                            </div>
                                             <!--city search from Google Api Start-->
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-sm-8 custom-select-box tec-domain-cat1 tocity">
                                            <div class="row">
                                                <input type="text" class="form-control"name="tocity" id="tolocation" value=""/>

    
                                            </div>
                                        </div>
                                        <div class="col-sm-4 custom-select-box tec-domain-cat1 morecabbuttons">
                                            <div class="row ">
                                                <div class="clearfix" ></div>
                                                <button type="button" class="btn blue" id="addButton">
			<i class="fa fa-plus-circle"></i>  </button>
                        <button type="button" class="btn blue" id="removeButton">
			<i class="fa fa-remove"></i> </button>
                                                </div>
                                        </div> 
                                        <div class="clearfix"></div>
                                         
                  
                             <!--Dynamic City Add Start-->             
                                        <div class="clearfix morecity" id="TextBoxesGroup"></div>
                                      

<!--Dynamic City Add Ends-->

                                        <div class="col-sm-12 custom-select-box">

                                            <div class="row">
                                                <div class='input-group date inputdate'>
                                                    <input type='text' class="form-control custom-select-box" placeholder="To Date" name="to_date"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="clearfix"></div>




                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 package">
                                            <div class="row">
                                                <select class="selectpicker" name="package"data-live-search="false" >
                                                    <option>Select Package</option>
                                                    <option value="4hrs">4 hrs. - 40 kms</option>
                                                    <option value="8hrs">8 hrs. - 80 kms</option>
                                                    <option value="24hrs">Full day</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div> 
                                        <div class="form-button">
                                            <button type="submit" name="submit" class="btn form-btn btn-lg btn-block" id="submitcab">Show Fares</button>
                                        </div>

                                    </div>
                                





                                </div>
</form>
                            </div>

                            <!-- Cab Section Ends-->  
                            <!-- Coach Section starts--> 
                            <div id="coach" class="tab-pane fade">

<form method="post" action="<?php echo base_url(); ?>vehiclelist">
                                <div class="form-select">
 <input type="hidden" name="journeyType" value="coach">
                                    <div class="radiowrap">
                                        <div class="col-sm-4">
                                            <label><input type="radio" id="localcoach" name="userJounarytype" value="local" checked="checked"> Local</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><input type="radio" id="outstationcoach"name="userJounarytype" value="outstation"> OutStation</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="local" id="coachoutstation">



                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 coachway">
                                            <div class="row">
                                                <select class="selectpicker" data-live-search="false" >
                                                    <option>One Way</option>
                                                    <option>Round Trip</option>

                                                </select>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-sm-12 custom-select-box tec-domain-cat1">
                                            <div class="row">
                                                      
                                            <input type="text" class="form-control"name="city" id="locationcoach"/>
                      <input type="hidden"  id="Latitudecoach" name="Latitude" >
                            <input type="hidden"  id="Longitudecoach" name="Longitude">
                                            
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                         <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-sm-12 custom-select-box tec-domain-cat1 moretocitycoach">
                                            <div class="row">
                                                 <input type="text" class="form-control"name="tocity" id="tolocationcoach" value=""/>
                                            </div>
                                        </div>
                                        
                           
                                        
                                        
                             <!--Dynamic City Add Start-->             
                                        <div class="clearfix" id="TextBoxesGroup2"></div>
                                      
<div class="col-sm-12 custom-select-box tec-domain-cat1 morebuttoncoach">
                                            <div class="row">
                                                <div class="clearfix" ></div>
                                                <button type="button" class="btn blue" id="addButton2">
			<i class="fa fa-plus-circle"></i>  </button>
                        <button type="button" class="btn blue" id="removeButton2">
			<i class="fa fa-remove"></i>  </button>
                                                </div>
                                        </div> 
<!--Dynamic City Add Ends-->

                                         <div class="col-sm-12 custom-select-box">

                                            <div class="row">
                                                <div class='input-group date inputdate'>
                                                    <input type='text' class="form-control custom-select-box" placeholder="To Date" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
    <div class="col-sm-12 custom-select-box tec-domain-cat1 packagecoach">
                                            <div class="row">
                                                <select class="selectpicker" data-live-search="false" >
                                                    <option>Select Package</option>
                                                    <option>4 hrs. - 0 kms</option>
                                                    <option>8 hrs. - 80 kms</option>
                                                    <option>Full day</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-sm-12 custom-select-box tec-domain-cat2">
                                            <div class="row">
                                                <select class="selectpicker" data-live-search="false"  >
                                                    <option>No. of Pessengers</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>10</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        

                                        
                                        
                                        
                                        <div class="clearfix"></div>

                                        <div class="form-button">
                                            <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                                        </div>

                                    </div>





                                </div>


</form>
                            </div>
                            <!-- Coach Section Ends-->                              
                            <!-- Self Drive Section starts-->                             
                            <div id="self" class="tab-pane fade">

                                <form>
                                <div class="form-select">
                                    <div class="selfdrive">

                                        <div class="col-sm-12">
                                            <label>I am in</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12 custom-select-box tec-domain-cat1">
                                            

 <!--city search from Google Api Start-->
                                            <div class="row">
                                            <input type="text" class="form-control"name="city" id="locationself"/>
                      <input type="hidden"  id="Latitudeself" name="Latitude" >
                            <input type="hidden"  id="Longitudeself" name="Longitude">
                                            </div>
                                             <!--city search from Google Api Start-->
                                            </div>
                                       
 <div class="col-sm-12">
                                            <label>From Date</label>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<div class="col-sm-12">
                                            <label>To Date</label>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-7 padding-left0">

                                                    <div class='input-group date inputdate'>
                                                        <input type='text' class="form-control custom-select-box" placeholder="From Date" name="from_date" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="col-sm-5 custom-select-box tec-domain-cat6">
                                                    <div class="row">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input type="text" class="form-control inputtime input-small custom-select-box" name="from_time" >
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-button">
                                            <button type="submit" class="btn form-btn btn-lg btn-block">Show Fares</button>
                                        </div>
                                    </div>
                                </div>
</form>
                            </div>
                            <!-- Self Drive Section Ends-->  
                        </div>




                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<!-- Booking now form wrapper html Exit -->		

<!-- label white html start -->
<div class="label-white white-lable-m">
    <div class="container">
        <div class="row">
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row"> 
                    <div class="label-item">
                        <div class="containt-font"> 
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/lock.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>Secure Booking</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row"> 
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/reliable.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>Reliable Service</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row"> 
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/customer.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>Customer Service</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6 " data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row float-right"> 
                    <div class="label-item ">
                        <div class="containt-font" >
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/hidden.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>No Hidden Charges</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<!-- label white html exit -->

<!-- label yellow html start -->
<div class="yellow-label-wrapper2"> 
    <div class="label-yellow stellar"  data-stellar-background-ratio="0.5" data-stellar-vertical-offset="" > 
        <div class="container">
            <div class="row">
                <div class="destination">
                    <h2>Destinations You'd Love</h2>
                    <h4>Look at the wonderful places</h4>
                </div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
                    <div class="slider-btn">
                        <a class="right-cursor1" href="#carousel-example-generic" data-slide="prev"></a> 
                        <a class="left-cursor1" href="#carousel-example-generic" data-slide="next"></a> 
                    </div>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider1.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider2.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item homepage-sllider-m">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider3.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider4.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div>  

                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider5.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 
                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item homepage-sllider-m">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider6.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>
                        </div>  

                        <div class="item">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider7.jpg') ?>"/></div> 
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider8.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item homepage-sllider-m">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider9.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                        </div>  

                    </div> 
                </div>							
            </div>
        </div>  
    </div>
</div>
<!-- label yellow html exit -->

<!-- label white2 html start -->
<div class="label-white2">
    <div class="container">
        <div class="row">
            <div class="car-item-wrap">
                <div class="car-type">
                    <div class="car-wrap"><img class="private-car" src="<?php echo base_url('assets/images/private-car.png') ?>" alt=""/></div>
                    <h5>Private Car</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="morotbike-car" src="<?php echo base_url('assets/images/motorbike.png') ?>" alt=""/></div> 
                    <h5>Motorbike</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="minicar-car" src="<?php echo base_url('assets/images/minicar.png') ?>" alt=""/></div> 
                    <h5>Minicar</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="mini-track-car" src="<?php echo base_url('assets/images/mini-track.png') ?>" alt=""/></div> 
                    <h5>Mini Truck</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="boat-car" src="<?php echo base_url('assets/images/boat.png') ?>" alt=""/></div>
                    <h5>Boat</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="snow-car" src="<?php echo base_url('assets/images/snow-bike.png') ?>" alt=""/></div> 
                    <h5>Snow Bike</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="tractor-car" src="<?php echo base_url('assets/images/tractor.png') ?>" alt=""/></div> 
                    <h5>Tractor</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="vihicel-car" src="<?php echo base_url('assets/images/vihicel.png') ?>" alt=""/></div> 
                    <h5>Large Vehicle</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="morotbike-car" src="<?php echo base_url('assets/images/motorbike.png') ?>" alt=""/></div> 
                    <h5>Motorbike</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="big-track-car" src="<?php echo base_url('assets/images/big-track.png') ?>" alt=""/></div>
                    <h5>Big Truck</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

            </div> 


        </div>
    </div>
</div>
<!-- label white2 html Exit --> 

<!-- page21 html start -->
<div class="page19-Featued-Drivers"> 
    <div class="page19-wrap stellar"  data-stellar-background-ratio="0.5" data-stellar-vertical-offset="" >
        <div class="container">
            <div class="row">
                <div class="page19-span"><h2>Featued Drivers of the Week</h2></div>
                <div class="page19-array2"><h4>These drivers are featured as best</h4></div> 

                <div id="carousel-example-generic5" class="carousel slide" data-ride="carousel"> 
                    <div class="slider-btn">
                        <a class="right-cursor2" href="#carousel-example-generic5" data-slide="prev">
                            <div class="featured-drivers"> <i class="fa fa-angle-left"></i></div>
                        </a> 
                        <a class="left-cursor2" href="#carousel-example-generic5" data-slide="next">
                            <div class="featured-drivers featured22"><i class="fa fa-angle-right"></i> </div>
                        </a> 
                    </div>
                    <div class="featured-drivers-wrapper">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team1.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>John Doe</h4></a>
                                        <span>Romania</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team2.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>Stephy Doe</h4></a>
                                        <span>Los Angeles</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team3.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Charlotte Doe</h4></a>
                                        <span>California</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team4.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Jonathan Doe</h4></a>
                                        <span>Spain</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2 featured22">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team5.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>James Doe</h4></a>
                                        <span>Canada</span>
                                    </div>
                                </div>
                            </div>


                            <div class="item">
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team1.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>John Doe</h4></a>
                                        <span>Romania</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team2.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>Stephy Doe</h4></a>
                                        <span>Los Angeles</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team3.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Charlotte Doe</h4></a>
                                        <span>California</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team4.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Jonathan Doe</h4></a>
                                        <span>Spain</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2 featured22">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team5.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>James Doe</h4></a>
                                        <span>Canada</span>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>	

            </div>	 
        </div>
    </div>
</div>
<!-- page21 html exit -->


<!-- Script For Cab SEction-->
<!--##############################################################################-->
<!--Script For Location city search for City name and Latitude and Longitude start-->

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"> </script> 
    <script>
// $("#tolocation").onchange(function(){
//     var value = $("#Latitude_from").val()
//     if(value.length > 0)
//     {
//         alert("hello");
//     }
// });
      
var countercab = 1;
$("#addButton").click(function () {
if(countercab>3){
        alert("Only 3 textboxes allow");
        return false;
}   

var newTextBoxDiv = $(document.createElement('div'))
     .attr("id", 'TextBoxDiv' + countercab);

newTextBoxDiv.after().html('<div class="col-sm-12 custom-select-box tec-domain-cat1 morecity">'+'<div class="row">'+'<input type="text" class="form-control" id="tolocation' + countercab +'" name="tocity' + countercab + '"  >'+'</div>'+'</div>');

newTextBoxDiv.appendTo("#TextBoxesGroup");


countercab++;
 });
 $("#removeButton").click(function () {
if(countercab==1){
      alert("No more textbox to remove");
      return false;
   }   

countercab--;

    $("#TextBoxDiv" + countercab).remove();

 });
 </script>
    <!-- script for from city-->
    <script type="text/javascript">
        function initialize() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('location'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress(address);
                }
            });
        }
        function codeAddress() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("location").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude').val(Latitude);
                    $('#Longitude').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>
    <!-- script for From city Ends-->
    <!-- script for to city start-->
    <script type="text/javascript">
        function initialize2() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress2(address);
                }
            });
        }
        function codeAddress2() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize2);
    </script>
    
    
    <!-- script for to city start-->
     <script type="text/javascript">
        function initialize3() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation1'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress3(address);
                }
            });
        }
        function codeAddress3() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation1").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize3);
    </script>
    
    <script type="text/javascript">
        function initialize4() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation2'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress4(address);
                }
            });
        }
        function codeAddress4() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation2").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize4);
    </script>
    
     <script type="text/javascript">
        function initialize5() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation3'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress5(address);
                }
            });
        }
        function codeAddress5() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation3").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize5);
    </script>

        <!-- script for to city Ends-->
    <!--Script For Location city search for City name and Latitude and Longitude Ends-->
  <script type="text/javascript">
                 $('.way').hide('fast');
                 $('.morecity').hide('fast');
                 $('.morecabbuttons').hide('fast');
                 
                 
                 $('.tocity').hide('fast');
                 $(document).ready(function () {
                    $('#local').click(function () {
                    $('.way').hide('fast');
                    $('.morecity').hide('fast');
                    $('.morecabbuttons').hide('fast');
                    $('.tocity').hide('fast');
                    $('.package').show('fast');
                });
                $('#outstation').click(function () {
                      //$('#div1').hide('fast');
                      $('.way').show('fast');
                      $('.morecity').show('fast');
                       $('.morecabbuttons').show('fast');
                      $('.tocity').show('fast');
                      $('.package').hide('fast');
                 });
               });
</script>

<!-- script For validation in Cab Form Start-->

<script>
    $("#submitcab").click(function ()
    {
        $('.has-error').html('');
        var has_error = false;


        var fromcity = $('.fromcity').val();
        if (fromcity == '')
        {
            $('#fromcity_help').html('Please Select From City');
            has_error = true;
        }

        if (has_error)
        {
            return false;
        }
    });

</script>

<!-- Script For Validation in Cab Form Ends-->

<!--##############################################################################-->

<!--Script For Cab Section Ends-->


<!--Script For Coach Section Start-->

<!--##############################################################################-->
 <script>
// $("#tolocation").onchange(function(){
//     var value = $("#Latitude_from").val()
//     if(value.length > 0)
//     {
//         alert("hello");
//     }
// });
      
var countercoach = 1;
$("#addButton2").click(function () {
if(countercoach>3){
        alert("Only 3 textboxes allow");
        return false;
}   

var newTextBoxDiv = $(document.createElement('div'))
     .attr("id", 'TextBoxDiv2' + countercoach);

newTextBoxDiv.after().html('<div class="col-sm-12 custom-select-box tec-domain-cat1 morecity">'+'<div class="row">'+'<input type="text" class="form-control" id="tolocationcoach' + countercoach +'" name="tocity' + countercoach + '"  >'+'</div>'+'</div>');

newTextBoxDiv.appendTo("#TextBoxesGroup2");


countercoach++;
 });
 $("#removeButton2").click(function () {
if(countercoach==1){
      alert("No more textbox to remove");
      return false;
   }   

countercoach--;

    $("#TextBoxDiv2" + countercoach).remove();

 });
 </script>



 <script type="text/javascript">
        function initialize6() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('locationcoach'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress6(address);
                }
            });
        }
        function codeAddress6() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("locationcoach").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitudecoach').val(Latitude);
                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize6);


    </script>
<script type="text/javascript">
        function initialize7() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress7(address);
                }
            });
        }
        function codeAddress6() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize7);


    </script>
    
 <script type="text/javascript">
        function initialize8() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach1'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress8(address);
                }
            });
        }
        function codeAddress8() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach1").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize8);


    </script>   
    
 <script type="text/javascript">
        function initialize9() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach2'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress9(address);
                }
            });
        }
        function codeAddress9() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach2").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize9);


    </script>  
    
     <script type="text/javascript">
        function initialize10() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach3'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress10(address);
                }
            });
        }
        function codeAddress10() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach3").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize10);


    </script>  
    
    
      <script type="text/javascript">
                 $('.coachway').hide('fast');
                  $('.moretocitycoach').hide('fast');
                  $('.morebuttoncoach').hide('fast');
            
                         
            
            
            
            
                 $(document).ready(function () {
                    $('#localcoach').click(function () {
                    $('.coachway').hide('fast');
                    $('.moretocitycoach').hide('fast');
                    $('.morebuttoncoach').hide('fast');
                    $('.packagecoach').show('fast');
                    
                });
                $('#outstationcoach').click(function () {
                      //$('#div1').hide('fast');
                      $('.coachway').show('fast');
                      $('.moretocitycoach').show('fast');
                      $('.morebuttoncoach').show('fast');
                      $('.packagecoach').hide('fast');
                 });
               });
</script>
<!--##############################################################################-->

<!--Script For Coach Section Start-->


<!--Script For Self Drive Start-->
<!--##############################################################################-->

<script type="text/javascript">
        function initialize11() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('locationself'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress11(address);
                }
            });
        }
        function codeAddress11() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("locationself").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitudeself').val(Latitude);
                    $('#Longitudeself').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize11);


    </script>
<!--##############################################################################-->

 <!--Script For Self Drive Ends-->