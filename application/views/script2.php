
<!-- Script For Cab SEction-->
<!--##############################################################################-->
<!--Script For Location city search for City name and Latitude and Longitude start-->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyB4gMiuXIcDP4Gk0PJLVHVepO56YurMMBU&callback=initMap"> </script> 
    <script>
// $("#tolocation").onchange(function(){
//     var value = $("#Latitude_from").val()
//     if(value.length > 0)
//     {
//         alert("hello");
//     }
// });
      
var countercab = 1;
$("#addButton").click(function () {
if(countercab>3){
        alert("Only 3 textboxes allow");
        return false;
}   

var newTextBoxDiv = $(document.createElement('div'))
     .attr("id", 'TextBoxDiv' + countercab);

newTextBoxDiv.after().html('<div class="col-sm-12 custom-select-box tec-domain-cat1 morecity">'+'<div class="row">'+'<input type="text" class="form-control" id="tolocation' + countercab +'" name="tocity' + countercab + '"  >'+'</div>'+'</div>');

newTextBoxDiv.appendTo("#TextBoxesGroup");


countercab++;
 });
 $("#removeButton").click(function () {
if(countercab==1){
      alert("No more textbox to remove");
      return false;
   }   

countercab--;

    $("#TextBoxDiv" + countercab).remove();

 });
 </script>
    <!-- script for from city-->
    <script type="text/javascript">
        function initialize() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('location'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress(address);
                }
            });
        }
        function codeAddress() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("location").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude').val(Latitude);
                    $('#Longitude').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>
    <!-- script for From city Ends-->
    <!-- script for to city start-->
    <script type="text/javascript">
        function initialize2() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress2(address);
                }
            });
        }
        function codeAddress2() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize2);
    </script>
    
    
    <!-- script for to city start-->
     <script type="text/javascript">
        function initialize3() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation1'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress3(address);
                }
            });
        }
        function codeAddress3() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation1").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize3);
    </script>
    
    <script type="text/javascript">
        function initialize4() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation2'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress4(address);
                }
            });
        }
        function codeAddress4() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation2").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize4);
    </script>
    
     <script type="text/javascript">
        function initialize5() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocation3'));
            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');

                    codeAddress5(address);
                }
            });
        }
        function codeAddress5() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocation3").value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());

                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_to').val(Latitude);
                    $('#Latitude_from').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize5);
    </script>

        <!-- script for to city Ends-->
    <!--Script For Location city search for City name and Latitude and Longitude Ends-->
  <script type="text/javascript">
                 $('.way').hide('fast');
                 $('.morecity').hide('fast');
                 $('.morecabbuttons').hide('fast');
                 
                 
                 $('.tocity').hide('fast');
                 $(document).ready(function () {
                    $('#local').click(function () {
                    $('.way').hide('fast');
                    $('.morecity').hide('fast');
                    $('.morecabbuttons').hide('fast');
                    $('.tocity').hide('fast');
                    $('.package').show('fast');
                });
                $('#outstation').click(function () {
                      //$('#div1').hide('fast');
                      $('.way').show('fast');
                      $('.morecity').show('fast');
                       $('.morecabbuttons').show('fast');
                      $('.tocity').show('fast');
                      $('.package').hide('fast');
                 });
               });
</script>

<!-- script For validation in Cab Form Start-->

<script>
    $("#submitcab").click(function ()
    {
        $('.has-error').html('');
        var has_error = false;


        var fromcity = $('.fromcity').val();
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
//        alert(from_date);
//        alert(to_date);
        if (fromcity == '')
        {
            $('#fromcity_help').html('Please Select Cab From City');
            has_error = true;
        }

       if(from_date == '')
        {
            $('#from_date_help').html('Please Enter Cab Journey Date');
            has_error = true;
        }
         if (to_date == '')
        {
            $('#to_date_help').html('Please Enter Cab Journey Upto Date');
            has_error = true;
        }
//        if(to_date>=from_date ){
//             $('#to_date_help').html('End Date must be Grater than or Equal to Journey Date');
//            has_error = true;
//            
//        }
        if (has_error)
        {
 
            return false;
        }
 
    });

</script>

<!-- Script For Validation in Cab Form Ends-->

<!--##############################################################################-->

<!--Script For Cab Section Ends-->


<!--Script For Coach Section Start-->

<!--##############################################################################-->
 <script>
// $("#tolocation").onchange(function(){
//     var value = $("#Latitude_from").val()
//     if(value.length > 0)
//     {
//         alert("hello");
//     }
// });
      
var countercoach = 1;
$("#addButton2").click(function () {
if(countercoach>3){
        alert("Only 3 textboxes allow");
        return false;
}   

var newTextBoxDiv = $(document.createElement('div'))
     .attr("id", 'TextBoxDiv2' + countercoach);

newTextBoxDiv.after().html('<div class="col-sm-12 custom-select-box tec-domain-cat1 morecity">'+'<div class="row">'+'<input type="text" class="form-control" id="tolocationcoach' + countercoach +'" name="tocity' + countercoach + '"  >'+'</div>'+'</div>');

newTextBoxDiv.appendTo("#TextBoxesGroup2");


countercoach++;
 });
 $("#removeButton2").click(function () {
if(countercoach==1){
      alert("No more textbox to remove");
      return false;
   }   

countercoach--;

    $("#TextBoxDiv2" + countercoach).remove();

 });
 </script>



  <script type="text/javascript">
        function initialize_coach() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('location_coach'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress_coach(address);
                }
            });
        }
        function codeAddress_coach() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("location_coach").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitude_coach').val(Latitude);
                    $('#Longitude_coach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize_coach);


    </script>
<script type="text/javascript">
        function initialize7() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress7(address);
                }
            });
        }
        function codeAddress6() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize7);


    </script>
    
 <script type="text/javascript">
        function initialize8() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach1'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress8(address);
                }
            });
        }
        function codeAddress8() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach1").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize8);


    </script>   
    
 <script type="text/javascript">
        function initialize9() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach2'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress9(address);
                }
            });
        }
        function codeAddress9() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach2").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize9);


    </script>  
    
     <script type="text/javascript">
        function initialize10() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('tolocationcoach3'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress10(address);
                }
            });
        }
        function codeAddress10() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("tolocationcoach3").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
//                    var Latitude = results[0].geometry.location.lat();
//                    var Longitude = results[0].geometry.location.lng();
//                    $('#Latitudecoach').val(Latitude);
//                    $('#Longitudecoach').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'click', initialize10);


    </script>  
    
    
      <script type="text/javascript">
                 $('.coachway').hide('fast');
                  $('.moretocitycoach').hide('fast');
                  $('.morebuttoncoach').hide('fast');
                     $('.coachtocity').hide('fast');
                         
            
            
            
            
                 $(document).ready(function () {
                    $('#localcoach').click(function () {
                    $('.coachway').hide('fast');
                    $('.moretocitycoach').hide('fast');
                    $('.morebuttoncoach').hide('fast');
                    $('.packagecoach').show('fast');
                    $('.coachtocity').hide('fast');
                    
                });
                $('#outstationcoach').click(function () {
                      //$('#div1').hide('fast');
                      $('.coachway').show('fast');
                      $('.moretocitycoach').show('fast');
                      $('.morebuttoncoach').show('fast');
                      $('.packagecoach').hide('fast');
                      $('.coachtocity').show('fast');
                 });
               });
</script>

<script>
    $("#submitcoach").click(function ()
    {
       // alert('helloo');
        $('.has-error').html('');
        var has_error = false;


        var coach_from_city = $('.coach_from_city').val();
        var coach_from_date = $('#coach_from_date').val();
        var coach_to_date = $('#coach_to_date').val();

        if (coach_from_city == '')
        {
            $('#coach_from_city_help').html('Please Select Coach From City');
            has_error = true;
        }
        
          if (coach_from_date == '')
        {
            $('#coach_from_date_help').html('Please Select Coach Journey Date');
            has_error = true;
        }
           if (coach_to_date == '')
        {
            $('#coach_to_date_help').html('Please Select Coach Journey To Date');
            has_error = true;
        }
        
        

//       if(from_date == '')
//        {
//            $('#from_date_help').html('Please Enter Cab Journey Date');
//            has_error = true;
//        }
//         if (to_date == '')
//        {
//            $('#to_date_help').html('Please Enter Cab Journey Upto Date');
//            has_error = true;
//        }
////        if(to_date>=from_date ){
//             $('#to_date_help').html('End Date must be Grater than or Equal to Journey Date');
//            has_error = true;
//            
//        }
        if (has_error)
        {
 
            return false;
        }
 
    });

</script>
<!--##############################################################################-->

<!--Script For Coach Section Start-->


<!--Script For Self Drive Start-->
<!--##############################################################################-->

<script type="text/javascript">
        function initialize11() {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "ind"}
            };
            var address = (document.getElementById('locationself'));

            var autocomplete = new google.maps.places.Autocomplete(address, options);
            autocomplete.setTypes(['geocode']);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                    codeAddress11(address);
                }
            });
        }
        function codeAddress11() {
            geocoder = new google.maps.Geocoder();
            var address = document.getElementById("locationself").value;
            console.log(address);
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //alert("Latitude: "+results[0].geometry.location.lat());
                    // alert("Longitude: "+results[0].geometry.location.lng());
                    console.log(results);
                    var Latitude = results[0].geometry.location.lat();
                    var Longitude = results[0].geometry.location.lng();
                    $('#Latitudeself').val(Latitude);
                    $('#Longitudeself').val(Longitude);

                    //alert(""+Latitude);
                    //alert(""+Longitude);

                }

                else {
                    //alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize11);


    </script>
<!--##############################################################################-->

 <!--Script For Self Drive Ends-->
 