<?php $this->load->view('slider_common'); ?>
<?php $this->load->view('slider_search_form'); ?>
<!-- label white html start -->
<div class="label-white white-lable-m"id="midsection">
  <div class="container searchtour">
    <div class="clearfix"></div>
    <div class="searchcat-tab booking-tabs">
     <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Upcoming">Upcoming</a></li>
        <li><a data-toggle="tab" href="#History" userId="<?php echo $_SESSION['user_data']['id'] ?>" class="bookhistory">History</a></li>
      </ul>
      
     <div class="tab-content">
   	 <div id="Upcoming" class="tab-pane fade in active"> 
             
       <?php foreach($list as $vehicle_list){  ?>           
     <div class="repeatbox clearfix">
     
      <div class="row rowheading">
          <div class="col-md-6">
          <h2><?php echo $vehicle_list['bookingcode'] ?></h2>
          </div>
          <div class="col-md-6">
          <h2 class="text-right">
              <?php
                $currentDateTime = $vehicle_list['bookingPicupDate'];
$newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));
                    
                    echo $newDateTime?>
              
               </h2>
          </div>
          </div>
    
    <div class="row brdrrow">
          <div class="col-md-4">
          	
            <div class="row">
            	<div class="col-md-12">
                <h2><?php echo $vehicle_list['companyName'] ?></h2>
                <p><?php echo $vehicle_list['address'] ?> ,<?php echo $vehicle_list['cityfrom'] ?></p>
                </div>
            </div>
            
            <div class="row">
            	<div class="col-md-12">
                <h2><?php echo $vehicle_list['vehicleModelName'] ?>,<?php echo $vehicle_list['vehicleBrandName'] ?> </h2>
                <p><?php echo $vehicle_list['vehicleSegments'] ?>, <?php echo $vehicle_list['seeting'] ?> seater</p>
                
                </div>
            </div>
            
            
          </div>
          
          <div class="col-md-8">
          	
            	
            <div class="row">
            	<div class="col-md-12">
                <h2><i class="fa fa-map-marker"></i><?php echo $vehicle_list['cityfrom'] ?> </h2>
                <p>
                <?php
                $currentDateTime = $vehicle_list['bookingPicupDate'];
$newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));
                    
                    echo $newDateTime?></p>
                </div>
            </div>
            
            <div class="row">
            	<div class="col-md-12">
                <h2><i class="fa fa-map-marker"></i><?php echo $vehicle_list['cityto'] ?> </h2>
                <p>
                 <?php
                $currentDateTime = $vehicle_list['bookingDropDate'];
$newDateTime = date('M- d,Y h:i A', strtotime($currentDateTime));
                    
                    echo $newDateTime?></p>
                </div>
            </div>
            
            
            
          </div>
          
          </div>

    
          
            <div class="row">
            <div class="col-md-12">
             	<h3>Transaction Id: <span class="lightfont"><?php echo $vehicle_list['paymentId'] ?></span></h3>
            </div>
            
          </div>
          
             <div class="row brdrrow">
          
          <div class="col-xs-6 col-md-4">
          	<h3>Payment Status: <span class="lightfont"><?php if($vehicle_list['paymentstatus']=='1') {
                    
                    echo'Success';
                    
                }
                else{
                    
                    echo'Failed';
                }
?></span></h3>
          </div>
          
           <div class="col-xs-6 col-md-4">
          	<h3>Booking Status: <span class="lightfont">
                    <?php if($vehicle_list['bookingStatus']=='1') {
                    
                    echo'Success';
                    
                }
                else{
                    
                    echo'Failed';
                }
?></span></h3>
          </div>
          
            
          </div>
        

          
          
            <div class="row">
            <div class="col-xxs-12 col-xs-6 col-md-4">
             	<h3>Base Fare: <span class="lightfont"><i class="fa fa-rupee"></i> <?php echo $vehicle_list['totalCharge'] ?> </span></h3>
                 <p>Amount Paid: <i class="fa fa-rupee"></i> <?php echo $vehicle_list['paymentMoney'] ?> </p>
              
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-md-8">
            
	    <p class="text-right">
                <?php if($vehicle_list['bookingStatus']=='1') { ?>
                <a href="<?php echo base_url() . 'upcoming/cancel_trip/' . $vehicle_list['bookedId'] ?>" class="btn btn-primary">Cancel Trip</a>   
       <?php } ?>
                
                <?php if($vehicle_list['bookingStatus']=='4') {?>
                
                <span> Cancelled By <?php  echo $_SESSION['user_data']['userName'];?></span>
                <?php  } ?>
                   </p> 
            
            </div>
            
            
          </div>
          </div>
          
           <?php } ?> 
          
       </div>
       
       	 <div id="History" class="tab-pane fade history">
     
 
          
       </div>
       
    </div>
    
    <div class="btmcancel">
    	You can cancel your booking within 24 hours. Click to view our <a href="#">Cancellation Policy</a>.
    </div>
    
    </div>
    
   
    
  </div>
</div>
<?php $this->load->view('google_api_common'); ?>
<?php $this->load->view('script'); ?>
<!-- label white html exit --> 
<!-- Script For Booking History Start-->
<script>
    $(".bookhistory").click(function () { 
           userId = 0;  
           userId = $(this).attr('userId'); 
          
           var dataString = 'userId='+ userId + '&ajax_history_request=' + 1 ;;
            //alert(dataString);
            
                $.ajax({ 
            type: 'POST',
            url: "<?php echo base_url(); ?>upcoming/booking_history",
            data: dataString,
            success: function(data){ 
               
               $('.history').html(data) ;
             
            }
          }); 
    });
    </script>
    <!-- Script For Booking History Ends-->
<!-- Rate delivery popup -->

