<?php $this->load->view('slider_common'); ?>
<?php $this->load->view('slider_search_form'); ?>
<!-- label white html start -->
<div class="label-white white-lable-m">
    <div class="container">
        <div class="row">
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row"> 
                    <div class="label-item">
                        <div class="containt-font"> 
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/lock.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>Secure Booking</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row"> 
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/reliable.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>Reliable Service</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row"> 
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/customer.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>Customer Service</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6 " data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row float-right"> 
                    <div class="label-item ">
                        <div class="containt-font" >
                            <a href="#" class="img-circle"><img src="<?php echo base_url('assets/images/hidden.png') ?>" alt=""/></a>  
                        </div>
                        <div class="containt-text">
                            <h3>No Hidden Charges</h3>
                            <span>We ensure safest booking!</span>
                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio cidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<!-- label white html exit -->

<!-- label yellow html start -->
<div class="yellow-label-wrapper2"> 
    <div class="label-yellow stellar"  data-stellar-background-ratio="0.5" data-stellar-vertical-offset="" > 
        <div class="container">
            <div class="row">
                <div class="destination">
                    <h2>Destinations You'd Love</h2>
                    <h4>Look at the wonderful places</h4>
                </div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
                    <div class="slider-btn">
                        <a class="right-cursor1" href="#carousel-example-generic" data-slide="prev"></a> 
                        <a class="left-cursor1" href="#carousel-example-generic" data-slide="next"></a> 
                    </div>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider1.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider2.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item homepage-sllider-m">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider3.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider4.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div>  

                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider5.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 
                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item homepage-sllider-m">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider6.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>
                        </div>  

                        <div class="item">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider7.jpg') ?>"/></div> 
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item ">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider8.jpg') ?>"/></div> 

                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 


                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div> 

                                    </div> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="slider-item homepage-sllider-m">
                                        <div class="slider-img"><img class="img-responsive" alt="First slide" src="<?php echo base_url('assets/images/slider/slider9.jpg') ?>"/></div>  
                                        <div class="slider-text-hover">
                                            <div class="slider-hover-content"></div> 
                                            <div class="Orange">
                                                <div class="slider-hover-content2">
                                                    <h4>Orange Skies</h4>
                                                    <p>Save upto 50%</p>
                                                </div>
                                                <div class="slider-hover-content3">
                                                    <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="slider-text">
                                            <div class="slider-text1">
                                                <h4>Orange Skies</h4>
                                                <p>Save upto 50%</p>
                                            </div>
                                            <div class="slider-text2">
                                                <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                        </div>  

                    </div> 
                </div>							
            </div>
        </div>  
    </div>
</div>
<!-- label yellow html exit -->

<!-- label white2 html start -->
<div class="label-white2">
    <div class="container">
        <div class="row">
            <div class="car-item-wrap">
                <div class="car-type">
                    <div class="car-wrap"><img class="private-car" src="<?php echo base_url('assets/images/private-car.png') ?>" alt=""/></div>
                    <h5>Private Car</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="morotbike-car" src="<?php echo base_url('assets/images/motorbike.png') ?>" alt=""/></div> 
                    <h5>Motorbike</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="minicar-car" src="<?php echo base_url('assets/images/minicar.png') ?>" alt=""/></div> 
                    <h5>Minicar</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="mini-track-car" src="<?php echo base_url('assets/images/mini-track.png') ?>" alt=""/></div> 
                    <h5>Mini Truck</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="boat-car" src="<?php echo base_url('assets/images/boat.png') ?>" alt=""/></div>
                    <h5>Boat</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="snow-car" src="<?php echo base_url('assets/images/snow-bike.png') ?>" alt=""/></div> 
                    <h5>Snow Bike</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="tractor-car" src="<?php echo base_url('assets/images/tractor.png') ?>" alt=""/></div> 
                    <h5>Tractor</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="vihicel-car" src="<?php echo base_url('assets/images/vihicel.png') ?>" alt=""/></div> 
                    <h5>Large Vehicle</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="morotbike-car" src="<?php echo base_url('assets/images/motorbike.png') ?>" alt=""/></div> 
                    <h5>Motorbike</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"> <img class="big-track-car" src="<?php echo base_url('assets/images/big-track.png') ?>" alt=""/></div>
                    <h5>Big Truck</h5>
                    <div class="car-type-btn">
                        <a href="Results_4.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

            </div> 


        </div>
    </div>
</div>
<!-- label white2 html Exit --> 

<!-- page21 html start -->
<div class="page19-Featued-Drivers"> 
    <div class="page19-wrap stellar"  data-stellar-background-ratio="0.5" data-stellar-vertical-offset="" >
        <div class="container">
            <div class="row">
                <div class="page19-span"><h2>Featued Drivers of the Week</h2></div>
                <div class="page19-array2"><h4>These drivers are featured as best</h4></div> 

                <div id="carousel-example-generic5" class="carousel slide" data-ride="carousel"> 
                    <div class="slider-btn">
                        <a class="right-cursor2" href="#carousel-example-generic5" data-slide="prev">
                            <div class="featured-drivers"> <i class="fa fa-angle-left"></i></div>
                        </a> 
                        <a class="left-cursor2" href="#carousel-example-generic5" data-slide="next">
                            <div class="featured-drivers featured22"><i class="fa fa-angle-right"></i> </div>
                        </a> 
                    </div>
                    <div class="featured-drivers-wrapper">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team1.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>John Doe</h4></a>
                                        <span>Romania</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team2.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>Stephy Doe</h4></a>
                                        <span>Los Angeles</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team3.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Charlotte Doe</h4></a>
                                        <span>California</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team4.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Jonathan Doe</h4></a>
                                        <span>Spain</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2 featured22">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team5.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>James Doe</h4></a>
                                        <span>Canada</span>
                                    </div>
                                </div>
                            </div>


                            <div class="item">
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team1.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>John Doe</h4></a>
                                        <span>Romania</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team2.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div> 
                                    <div class="featured-text">
                                        <a href="">	<h4>Stephy Doe</h4></a>
                                        <span>Los Angeles</span> 
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team3.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Charlotte Doe</h4></a>
                                        <span>California</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team4.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>Jonathan Doe</h4></a>
                                        <span>Spain</span>
                                    </div>
                                </div>
                                <div class="featured-drivers2 featured22">
                                    <div class="featured">
                                        <div class="featured-images"><a href=""><img src="<?php echo base_url('assets/images/team5.jpg') ?>" alt=""/></a></div>
                                        <div class="featured-hover"></div>
                                    </div>
                                    <div class="featured-text">
                                        <a href="">	<h4>James Doe</h4></a>
                                        <span>Canada</span>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>	

            </div>	 
        </div>
    </div>
</div>
<!-- page21 html exit -->

<?php $this->load->view('google_api_common'); ?>
<?php $this->load->view('script'); ?>