<!DOCTYPE html>
    <html> 
    <head> 
       <meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
       <title>Google Maps API v3 Directions Example</title> 
      
      
               <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4gMiuXIcDP4Gk0PJLVHVepO56YurMMBU&callback=initMap"
  type="text/javascript"></script>
    </head> 
    <body style="font-family: Arial; font-size: 12px;"> 
       <div style="width: 600px;">
         <div id="map" style="width: 1250px; height: 400px; float: left;"></div> 
         
       </div>
       
       <script type="text/javascript"> 
      function initMap(){
         var directionsService = new google.maps.DirectionsService();
         var directionsDisplay = new google.maps.DirectionsRenderer();
    
         var map = new google.maps.Map(document.getElementById('map'), {
           zoom:7,
           mapTypeId: google.maps.MapTypeId.ROADMAP
         });
               

         directionsDisplay.setMap(map);
         directionsDisplay.setPanel(document.getElementById('panel'));
        
         var request = {
           origin: 'New Delhi, Delhi, India', 
           destination: 'Jaipur, Rajasthan, India',
           travelMode: google.maps.DirectionsTravelMode.DRIVING
         };
          //alert('map');
           directionsService.route(request, function(response, status) {
           if (status == google.maps.DirectionsStatus.OK) {
             directionsDisplay.setDirections(response);
           }
         });}
       </script> 
    </body> 
    </html>