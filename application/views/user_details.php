
	<!-- label white html start -->
		<div class="label-white white-lable-m">
			<div class="container">
		
                <div class="clearfix profile-container">
                <div class="row">
                <div class="col-md-6 col-xs-6"><h1 class="h1 btmmrgn">My Profile</h1></div>
                <div class="col-md-6 col-xs-6"><div class="edit-on-heading"><a href="<?php echo base_url(); ?>users_detail/user_edit_view"><i class="fa fa-edit"></i> Edit</a></div></div>
                </div>
                
	<div class="form-group row">
    <label class="col-sm-3 form-control-label">Name</label>
    <div class="col-sm-9">
      <label class="form-control-label labeltext"><?php echo $details['userName'] ?></label>
    </div>
  </div>
  
    <div class="form-group row">
    <label class="col-sm-3 form-control-label">Phone No.</label>
    <div class="col-sm-9">
      <label class="form-control-label labeltext"><?php echo $details['mobileNumber'] ?></label>
    </div>
  </div>
  
    <div class="form-group row">
    <label class="col-sm-3 form-control-label">Email</label>
    <div class="col-sm-9">
    <label class="form-control-label labeltext"><?php echo $details['emailId'] ?></label>

    </div>
  </div>
  
    <div class="form-group row">
    <label class="col-sm-3 form-control-label">City</label>
    <div class="col-sm-9">
        <label class="form-control-label labeltext"><?php echo $details['city'] ?></label>
    </div>
  </div>

                 </div>
			</div> 
		</div>
		<!-- label white html exit -->
		
		