<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Userbooking extends MY_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
    }
    
    function index(){
        //prd($_GET);
            if (!empty($this->input->get())) {
                
                $customer_original_name=$this->input->get('customerName');
                $customer_changed_name=$this->input->get('customer_name');
                
                if($customer_original_name==$customer_changed_name){
                    
                    $booked_customer=$customer_original_name;
                }
                if($customer_original_name!=$customer_changed_name){
                    
                    $booked_customer=$customer_changed_name;
                }
                //echo $booked_customer;die;
                 $customer_original_number=$this->input->get('customerMobileNumber');
                 $customer_changed_number=$this->input->get('mobile_number');
                 
                 if($customer_original_number==$customer_changed_number){
                    
                    $booked_number=$customer_original_number;
                }
                if($customer_original_number!=$customer_changed_number){
                    
                    $booked_number=$customer_changed_number;
                }
                
                $no_original_passenger=$this->input->get('numberOfPassengers');
                $no_changed_passenger=$this->input->get('numberOfPassengers_change_values');
                
                  if($no_original_passenger==$no_changed_passenger){
                    
                    $booked_passenger=$no_original_passenger;
                }
                
                 if($no_original_passenger!=$no_changed_passenger){
                    
                    $booked_passenger=$no_changed_passenger;
                }
                
                //echo $booked_number;die;
                
            if ($this->input->get('journeyType') == 'cab') {
                if ($this->input->get('userJounarytype') == 'local') {
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $booked_passenger,
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'customerMobileNumber' => $booked_number,                        
                        'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $booked_customer,
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                         'modelName'=>$this->input->get('modelName'),
                         'brandName'=>$this->input->get('brandName'),
                         'address1'=>$this->input->get('address'),
                         'address2'=>'',
                         'landmark'=>'',
                         'instruction'=>$this->input->get('other_details'),
                    );
                }
                else{
                    
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $booked_passenger,
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                         'customerMobileNumber' => $booked_number,                                                    'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $booked_customer,
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                         'modelName'=>$this->input->get('modelName'),
                         'brandName'=>$this->input->get('brandName'),
                         'address1'=>$this->input->get('address'),
                         'address2'=>'',
                         'landmark'=>'',
                         'instruction'=>$this->input->get('other_details'),
                    );
                    
                }
                
                
            }

                 if ($this->input->get('journeyType') == 'coach') {
                if ($this->input->get('userJounarytype') == 'local') {
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $booked_passenger,
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                         'customerMobileNumber' => $booked_number,                         
                        'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $booked_customer,
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                         'modelName'=>$this->input->get('modelName'),
                         'brandName'=>$this->input->get('brandName'),
                        'address1'=>$this->input->get('address'),
                         'address2'=>'',
                         'landmark'=>'',
                         'instruction'=>$this->input->get('other_details'),
                    );
                }
                else{
                    
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $booked_passenger,
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                         'customerMobileNumber' => $booked_number,                                                    'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $booked_customer,
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                        'modelName'=>$this->input->get('modelName'),
                        'brandName'=>$this->input->get('brandName'),
                        'address1'=>$this->input->get('address'),
                        'address2'=>'',
                        'landmark'=>'',
                        'instruction'=>$this->input->get('other_details'),
                    );
                    
                }
                
                
            }
            
            //prd($posted_data);
            //Comment Part
            $user_api = user_api;
           
            $url = "$user_api/userbooking";
          
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            
            $data2 = json_decode($data, true);

            //prd($data2);
                $mydata['id']=$data2['payload'];
                $mydata['data'] = $data2;
            
               //Comment Part ends
                 $mydata['posted_data']=$posted_data;
                  $company_details=array(
                
                'companyname'=>$this->input->get('companyname'),
                'avgrate'=>$this->input->get('avgrate'),
                 
            );
             //prd($company_details);
             $mydata['company_details']=$company_details;
                
                //set session data for payment detaway
                
                set_session_payment($mydata);
              
            
            if ($data2['type'] == 'OK') {
                
                 redirect ('userbooking/payment_getaway');
//               
//               $data1['body']=$this->load->view('user_booking',$mydata,true); 
//               
//               $this->load->view('admin_template',$data1);
            }

            if ($data2['type'] == 'ERROR') {
                
                redirect ('userbooking/payment_getaway');
//               $data1['body']=$this->load->view('user_booking',$mydata,true); 
//               
//               $this->load->view('admin_template',$data1);
            }
        } else {

            
            redirect('homepage');
//           $data1['body']=$this->load->view('user_booking','',true); 
//               
//               $this->load->view('admin_template',$data1);
        }
        
    }
    
    
     function payment_getaway(){
        
               $data1['body']=$this->load->view('user_booking','',true); 
               
               $this->load->view('admin_template',$data1);
    }
    
}

