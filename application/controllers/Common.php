<?php

defined('BASEPATH') OR exit('No Direct Script Access Allowed');

class Common extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');
    }

    function index() {
        
    }

    //Get segment data
    function segment() {

        if (empty($this->input->post(''))) {

            $data = array(
                'vehicleType' => 'cab',
            );
            $user_api = user_api;
            $url = "$user_api/segment";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
            //prd($data2);


            $mydata['list'] = $data2['payload'];
            $mydata['data'] = $data2;
            //$data = array();
            if ($data2['type'] == 'OK') {

                $this->load->view('segment_list', $mydata);
            }

            if ($data2['type'] == 'ERROR') {


                $this->load->view('segment_list', $mydata);
            }
        } else {

            $this->load->view('segment_list');
        }
    }

    //Get State List
    function getstate() {

        $user_api = user_api;
        $url = "$user_api/getstate";
        //prd($url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '');
        @curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        $data = curl_exec($ch);
        curl_close($ch);
        //print_r($data);die;
        $data2 = json_decode($data, true);
        //prd($data2);


        $mydata['list'] = $data2['payload'];
        $mydata['data'] = $data2;
        //$data = array();
        if ($data2['type'] == 'OK') {

            $this->load->view('state_list', $mydata);
        }

        if ($data2['type'] == 'ERROR') {


            $this->load->view('state_list', $mydata);
        }
    }

    //Get City List Accourding to State
    function getcity() {

        if (empty($this->input->post(''))) {

            $data = array(
                'stateId' => '3',
            );
            // prd($data);
            $user_api = user_api;
            $url = "$user_api/getcity";
            //prd($url);


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
            //prd($data2);



            $mydata['list'] = $data2['payload'];
            $mydata['data'] = $data2;
            //$data = array();
            if ($data2['type'] == 'OK') {

                $this->load->view('city_list', $mydata);
            }

            if ($data2['type'] == 'ERROR') {


                $this->load->view('city_list', $mydata);
            }
        } else {

            $this->load->view('city_list');
        }
    }

    //GET all City List
    function getcitylist() {

        $user_api = user_api;
        $url = "$user_api/getcitylist";


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '');
        @curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        $data = curl_exec($ch);
        curl_close($ch);
        //print_r($data);die;
        $data2 = json_decode($data, true);
        //prd($data2);


        $mydata['list'] = $data2['payload'];
        $mydata['data'] = $data2;
        //$data = array();
        if ($data2['type'] == 'OK') {

            $this->load->view('city_listall', $mydata);
        }

        if ($data2['type'] == 'ERROR') {


            $this->load->view('city_listall', $mydata);
        }
    }

    function getbrand() {

        if (empty($this->input->post(''))) {
            $data = array(
                'segmentsId' => '3'
            );

            $user_api = user_api;

            $url = "$user_api/getbrand";
            //prd($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
            // prd($data2);


        

            $mydata['list'] = $data2['payload'];
            $mydata['data'] = $data2;
            //$data = array();
            if ($data2['type'] == 'OK') {

                $this->load->view('brand_list', $mydata);
            }

            if ($data2['type'] == 'ERROR') {


                $this->load->view('brand_list', $mydata);
            }
        } else {

            $this->load->view('brand_list');
        }
    }

    function getmodel() {

        if (empty($this->input->post(''))) {

            $data = array(
                'brandId' => '1',
                'segmentsId' => '4',
            );

            $user_api = user_api;

            $url = "$user_api/getmodel";
            //CURL Function
             //curl_function($url,$data);
               //prd($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
             //prd($data2);
             $mydata['list'] = $data2['payload'];
            $mydata['data'] = $data2;
            //$data = array();
            if ($data2['type'] == 'OK') {

                $this->load->view('model_list', $mydata);
            }

            if ($data2['type'] == 'ERROR') {


                $this->load->view('model_list', $mydata);
            }
         
            
        } else {
            
            $this->load->view('model_list');
        }
    }

}
