<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiclelist extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
    }
    
//    function index(){
//           $data['body']=$this->load->view('vehiclelist','',true);
//            $this->load->view('admin_template',$data);
//        
//    }

    function index() {
        //Check the Condition for not empty post data
//prd($_GET);
        if (!empty($this->input->post())) {

            $source_address = $this->input->post('city');
            $destination_address = $this->input->post('tocity');
            $destination_address1 = $this->input->post('tocity1');
            $destination_address2 = $this->input->post('tocity2');
            $destination_address3 = $this->input->post('tocity3');

//            echo $source_address;
//           echo $destination_address;
//           echo $destination_address1;
//           echo $destination_address2;
//           echo $destination_address3;die;
            
            //Google API Distance Search Calculate
            if (!empty($source_address && $destination_address)) {
                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=" . str_replace(' ', '+', $source_address) . "&destination=" . str_replace(' ', '+', $destination_address) . "&sensor=false";
                //prd($url);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_all = json_decode($response);
                // print_r($response);
                $distance1 = $response_all->routes[0]->legs[0]->distance->text;
            }

            //Google API Distance Search
            if (!empty($destination_address && $destination_address1)) {
                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=" . str_replace(' ', '+', $destination_address) . "&destination=" . str_replace(' ', '+', $destination_address1) . "&sensor=false";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_all = json_decode($response);
                // print_r($response);
                $distance2 = $response_all->routes[0]->legs[0]->distance->text;
            }
            if (!empty($destination_address1 && $destination_address2)) {
                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=" . str_replace(' ', '+', $destination_address1) . "&destination=" . str_replace(' ', '+', $destination_address2) . "&sensor=false";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_all = json_decode($response);
                // print_r($response);
                $distance3 = $response_all->routes[0]->legs[0]->distance->text;
            }

            if (!empty($destination_address2 && $destination_address3)) {
                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=" . str_replace(' ', '+', $destination_address2) . "&destination=" . str_replace(' ', '+', $destination_address3) . "&sensor=false";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_all = json_decode($response);
                // print_r($response);
                $distance4 = $response_all->routes[0]->legs[0]->distance->text;
            }
      @$disrance_km1 = str_replace(',', '', @$distance1);
      @$disrance_km2 = str_replace(',', '', @$distance2);
      @$disrance_km3 = str_replace(',', '', @$distance3);
      @$disrance_km4 = str_replace(',', '', @$distance4);
            //prd($distance);
             $total_distance = @$disrance_km1 + @$disrance_km2 + @$disrance_km3 + @$disrance_km4;
             //echo $total_distance;die;
            // prd($total_distance);
             $distance= $total_distance .' '.'km';
            //Google API Distance Search Calculate Ends
           
             //Set Session data for City multiple Selection and Distance Calculation Start
          if (empty($_POST['ajax_segment_request'])){
            
            $destination['source']=$source_address;
            $destination['destination']=$destination_address;
            $destination['destination1']=$destination_address1;
            $destination['destination2']=$destination_address2;
            $destination['destination3']=$destination_address3;
            $distance_cal['distance_calculation1']=@$distance1;
            $distance_cal['distance_calculation2']=@$distance2;
            $distance_cal['distance_calculation3']=@$distance3;
            $distance_cal['distance_calculation4']=@$distance4;
            $distance_cal['total_distance']=@$distance;
            
            set_destination_distance($destination,$distance_cal); 
            //prd($_SESSION);
          }
              //Set Session data for City multiple Selection and Distance Calculation Ends
             
             
            //Check the journeytype and  userjourneytype
            //journeytype=cab,coach,selfdrive
            //userjourneytype=local,outstation

            $journey_type = $this->input->post('journeyType');
            $userJounarytype = $this->input->post('userJounarytype');

            //Common feild for Local section for both cab and coach

            $journey_type = $this->input->post('journeyType');
            $userJounarytype = $this->input->post('userJounarytype');
            $from_city = $this->input->post('city');
            $Longitude = $this->input->post('Longitude');
            $Latitude = $this->input->post('Latitude');
            //Check the Package Section 
            $hours = $this->input->post('package');
            if ($hours == '4hrs') {
                $hour = '4';
                $km = '40';
            } elseif ($hours == '8hrs') {
                $hour = '8';
                $km = '80';
            } else {
                $hour = '24';
                $km = '250';
            }
            //Check the Booking date Format And time 
            $from_date = $this->input->post('from_date');
            $book_date = date('Y-m-d', strtotime($from_date));
            $from_time = $this->input->post('from_time');
            $from_time_book = date("H:i:s", strtotime($from_time));
            $bookingPicupDate = $book_date . ' ' . $from_time_book;
            $to_date = $this->input->post('to_date');
            $bookingDropDate = date('Y-m-d H:i:s', strtotime($to_date));

            $no_seat = $this->input->post('passenger');
            $routeType = $this->input->post('routeType');
            
            if(!empty($routeType)){
                
                $routeTypes['routeType']=$routeType;
                set_route_type($routeTypes);
            }
            
            
//            if(!empty($this->input->post('tocity'))){
//              $to_city = $this->input->post('tocity');  
//            }else{
//                $to_city = '';
//            }
            
            //Gettimg the To city search value
            
            if(!empty($_SESSION['destination']['destination'])&& empty($_SESSION['destination']['destination1'])&& empty($_SESSION['destination']['destination2'])&& empty($_SESSION['destination']['destination3'])){
                
                $to_city= $_SESSION['destination']['destination'];
          } 
          elseif(!empty($_SESSION['destination']['destination'])&&!empty($_SESSION['destination']['destination1'])&& empty($_SESSION['destination']['destination2'])&& empty($_SESSION['destination']['destination3']))
              
          {
              
            $to_city= $_SESSION['destination']['destination1'];   
          }
              
              elseif(!empty($_SESSION['destination']['destination'])&&!empty($_SESSION['destination']['destination1'])&&!empty($_SESSION['destination']['destination2'])&& empty($_SESSION['destination']['destination3']))
              
          {  
              
              
               $to_city= $_SESSION['destination']['destination2'];   
          } 
              
              elseif(!empty($_SESSION['destination']['destination'])&&!empty($_SESSION['destination']['destination1'])&&!empty($_SESSION['destination']['destination2'])&&!empty($_SESSION['destination']['destination3']))
              
          {  
              
              
               $to_city= $_SESSION['destination']['destination3'];   
          } 
           else{
               
               $to_city='';
           } 
   
            //Getting the TO City Search Value Ends
           
           
           //Getting the Roundtrip case the Distance Calculate Start
           
              //Google API Distance Search Calculate
            if (!empty($source_address && $to_city)) {
                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=" . str_replace(' ', '+', $to_city) . "&destination=" . str_replace(' ', '+', $source_address) . "&sensor=false";
                //prd($url);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_all = json_decode($response);
                // print_r($response);
                $return_distance = $response_all->routes[0]->legs[0]->distance->text;
                
                $return_distances['return_distances']=$return_distance;
                
                set_return_distance($return_distances);
            }
           
            //Setting the Distance Calculate in Round trip
            
            
            
            
            
           //Getting the Roundtrip case the Distance Calculate Ends
            
            
            
            if(!empty($this->input->post('segment')))
            {
             $segment=$this->input->post('segment');
            }
            else{
                $segment = '0';
            }
            
         
    if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1){
        
                 if($_POST['userJounarytype'] == 'local'){
                 $posted_data=array(
                     'userId' => guest_user,
                     'journeyType'=>$this->input->post('journeyType'),
                     'userJounarytype'=>$this->input->post('userJounarytype'),
                     'bookingPicupDate'=>$this->input->post('bookingPicupDate'),
                     'kms'=>$this->input->post('kms'),
                     'hours'=>$this->input->post('hours'),
                     'segments'=>$this->input->post('segment'),
                     'bookingDropDate'=>$this->input->post('bookingDropDate'),
                     'sendTime'=>$this->input->post('sendTime'),
                     'bookedFrom'=>$this->input->post('bookedFrom'),
                     'bookedTo'=>$this->input->post('bookedTo'),
                     'numbersOfseat'=>$this->input->post('numbersOfseat'),
                     'latitude'=>$this->input->post('latitude'),
                     'longitude'=>$this->input->post('longitude'),
                     'routeType'=>$this->input->post('routeType'),
                     'pageNo'=>$this->input->post('pageNo'),
                     'totalkms'=>$this->input->post('totalkms'),
                     'city_latitude'=>$this->input->post('city_latitude'),
                     'city_longitude'=>$this->input->post('city_longitude'),
                       
                 );
                 }
                 else if($_POST['userJounarytype'] == 'outstation'){
                      $posted_data=array(
                     'userId' => guest_user,
                     'journeyType'=>$this->input->post('journeyType'),
                     'userJounarytype'=>$this->input->post('userJounarytype'),
                     'bookingPicupDate'=>$this->input->post('bookingPicupDate'),
                     'kms'=>'0',
                     'hours'=>'0',
                     'segments'=>$this->input->post('segment'),
                     'bookingDropDate'=>$this->input->post('bookingDropDate'),
                     'sendTime'=>$this->input->post('sendTime'),
                     'bookedFrom'=>$this->input->post('bookedFrom'),
                     'bookedTo'=>$this->input->post('bookedTo'),
                     'numbersOfseat'=>$this->input->post('numbersOfseat'),
                     'latitude'=>$this->input->post('latitude'),
                     'longitude'=>$this->input->post('longitude'),
                     'routeType'=>$this->input->post('routeType'),
                     'pageNo'=>$this->input->post('pageNo'),
                     'totalkms'=>$this->input->post('totalkms'),
                     'city_latitude'=>$this->input->post('city_latitude'),
                     'city_longitude'=>$this->input->post('city_longitude'),
                      
                 );
                     
                 }
             }
            elseif ($journey_type == 'cab') {

                if ($userJounarytype == 'local') {
                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $journey_type,
                        'userJounarytype' => $userJounarytype,
                        'bookingPicupDate' => $bookingPicupDate,
                        'hours' => $hour,
                        'kms' => $km,
                        'bookingDropDate' => $bookingDropDate,
                        'sendTime' => $from_time_book,
                        'segments' => $segment,
                        'bookedFrom' => $from_city,
                        'bookedTo' => '',
                        'numbersOfseat' => '0',
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'routeType' => '0',
                        'pageNo' => '1',
                        'totalkms' => '0',
                        'city_latitude' => $Latitude,
                        'city_longitude' => $Longitude,
                    );
                } else {
                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $journey_type,
                        'userJounarytype' => $userJounarytype,
                        'bookingPicupDate' => $bookingPicupDate,
                        'routeType' => $routeType,
                        'totalkms' => $distance,
                        'bookingDropDate' => $bookingDropDate,
                        'sendTime' => $from_time_book,
                        'segments' => $segment,
                        'bookedFrom' => $from_city,
                        'bookedTo' => $to_city,
                        'numbersOfseat' => '0',
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'pageNo' => '1',
                        'city_latitude' => $Latitude,
                        'city_longitude' => $Longitude,
                    );
                }
            }

            elseif ($journey_type == 'coach') {

                if ($userJounarytype == 'local') {
                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $journey_type,
                        'userJounarytype' => $userJounarytype,
                        'bookingPicupDate' => $bookingPicupDate,
                        'hours' => $hour,
                        'kms' => $km,
                        'bookingDropDate' => $bookingDropDate,
                        'sendTime' => $from_time_book,
                        'segments' => $segment,
                        'bookedFrom' => $from_city,
                        'bookedTo' => '',
                        'numbersOfseat' => $no_seat,
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'routeType' => '0',
                        'pageNo' => '1',
                        'totalkms' => '0',
                        'city_latitude' => $Latitude,
                        'city_longitude' => $Longitude,
                    );
                } else {
                    $posted_data = array('userId' => guest_user,
                        'journeyType' => $journey_type,
                        'userJounarytype' => $userJounarytype,
                        'bookingPicupDate' => $bookingPicupDate,
                        'routeType' => $routeType,
                        'totalkms' => $distance,
                        'bookingDropDate' => $bookingDropDate,
                        'sendTime' => $from_time_book,
                        'segments' => $segment,
                        'bookedFrom' => $from_city,
                        'bookedTo' => $to_city,
                        'numbersOfseat' => $no_seat,
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'pageNo' => '1',
                        'city_latitude' => $Latitude,
                        'city_longitude' => $Longitude,
                    );
                }
            }
         
     //prd($posted_data);
            $user_api = user_api;
            $url = "$user_api/vehiclemodellist";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            //prd($data);
            curl_close($ch);
            //prd($data);die;
            $data2 = json_decode($data, true);
         //  echo"<pre>" ; print_r($data2);die;
            //prd($data2);
            $mydata['posted_data']=$posted_data;
            
            @$mydata['list'] = $data2['payload']['modelList'];
            $mydata['data'] = $data2;
            $mydata['number'] = $data2['payload'];

           
            $data = $this->segment($journey_type);
            $mydata['segment'] = $data['mylist'];
            if ($data2['type'] == 'OK') {
              // echo"atendra<pre>" ; print_r($mydata['list']);
                
             if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1){
                 $this->load->view('ajax_vehicle_list', $mydata);
                    
                }
                else{
                    
                    $data['body']=$this->load->view('vehiclelist',$mydata,true); 
                    $this->load->view('admin_template',$data);
                }
               
               
                 
                
            }

            if ($data2['type'] == 'ERROR') {
                  if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1){
                 $this->load->view('ajax_vehicle_list', $mydata);
                    
                }
                else{
                    
                    $data['body']=$this->load->view('vehiclelist',$mydata,true); 
                    $this->load->view('admin_template',$data);
                }
            }
        } else {
            //$data['body']=$this->load->view('homepage',true);
            //$this->load->view('admin_template',$data);
//            $this->load->view('vehiclelist');
            redirect('homepage');
        }
    }
    
    function pagination_search(){
       // prd($_POST);
        
         if(!empty($this->input->post('segment')))
            {
             $segment=$this->input->post('segment');
            }
            else{
                $segment = '0';
            }
        
        $journey_type = $this->input->get('journeyType');
        $userJounarytype = $this->input->get('userJounarytype');
         if (!empty($this->input->get())||!empty($this->input->post())) {
             
              if (isset($_POST['ajax_segment_page_request']) && $_POST['ajax_segment_page_request'] == 1){
        
                 if($_POST['userJounarytype'] == 'local'){
                 $posted_data=array(
                     'userId' => guest_user,
                     'journeyType'=>$this->input->post('journeyType'),
                     'userJounarytype'=>$this->input->post('userJounarytype'),
                     'bookingPicupDate'=>$this->input->post('bookingPicupDate'),
                     'kms'=>$this->input->post('kms'),
                     'hours'=>$this->input->post('hours'),
                     'segments'=>$this->input->post('segment'),
                     'bookingDropDate'=>$this->input->post('bookingDropDate'),
                     'sendTime'=>$this->input->post('sendTime'),
                     'bookedFrom'=>$this->input->post('bookedFrom'),
                     'bookedTo'=>$this->input->post('bookedTo'),
                     'numbersOfseat'=>$this->input->post('numbersOfseat'),
                     'latitude'=>$this->input->post('latitude'),
                     'longitude'=>$this->input->post('longitude'),
                     'routeType'=>$this->input->post('routeType'),
                     'pageNo'=>$this->input->post('pageNo'),
                     'totalkms'=>$this->input->post('totalkms'),
                     'city_latitude'=>$this->input->post('city_latitude'),
                     'city_longitude'=>$this->input->post('city_longitude'),
                       
                 );
                 }
                 else if($_POST['userJounarytype'] == 'outstation'){
                      $posted_data=array(
                     'userId' => guest_user,
                     'journeyType'=>$this->input->post('journeyType'),
                     'userJounarytype'=>$this->input->post('userJounarytype'),
                     'bookingPicupDate'=>$this->input->post('bookingPicupDate'),
                     'kms'=>'0',
                     'hours'=>'0',
                     'segments'=>$this->input->post('segment'),
                     'bookingDropDate'=>$this->input->post('bookingDropDate'),
                     'sendTime'=>$this->input->post('sendTime'),
                     'bookedFrom'=>$this->input->post('bookedFrom'),
                     'bookedTo'=>$this->input->post('bookedTo'),
                     'numbersOfseat'=>$this->input->post('numbersOfseat'),
                     'latitude'=>$this->input->post('latitude'),
                     'longitude'=>$this->input->post('longitude'),
                     'routeType'=>$this->input->post('routeType'),
                     'pageNo'=>$this->input->post('pageNo'),
                     'totalkms'=>$this->input->post('totalkms'),
                     'city_latitude'=>$this->input->post('city_latitude'),
                     'city_longitude'=>$this->input->post('city_longitude'),
                       
                 );
                     
                 }
             }
         elseif ($journey_type == 'cab') {

                if ($userJounarytype == 'local') {
                    $posted_data = array(
                     'userId' => guest_user,
                     'journeyType'=>$this->input->get('journeyType'),
                     'userJounarytype'=>$this->input->get('userJounarytype'),
                     'bookingPicupDate'=>$this->input->get('bookingPicupDate'),
                     'kms'=>$this->input->get('kms'),
                     'hours'=>$this->input->get('hours'),
                     'segments'=>$segment,
                     'bookingDropDate'=>$this->input->get('bookingDropDate'),
                     'sendTime'=>$this->input->get('sendTime'),
                     'bookedFrom'=>$this->input->get('bookedFrom'),
                     'bookedTo'=>$this->input->get('bookedTo'),
                     'numbersOfseat'=>$this->input->get('numbersOfseat'),
                     'latitude'=>$this->input->get('latitude'),
                     'longitude'=>$this->input->get('longitude'),
                     'routeType'=>$this->input->get('routeType'),
                     'pageNo'=>$this->input->get('pageNo'),
                     'totalkms'=>$this->input->get('totalkms'),
                     'city_latitude'=>$this->input->get('city_latitude'),
                     'city_longitude'=>$this->input->get('city_longitude'),
                    );
                } else {
                    $posted_data = array(
                        'userId' => guest_user,
                     'journeyType'=>$this->input->get('journeyType'),
                     'userJounarytype'=>$this->input->get('userJounarytype'),
                     'bookingPicupDate'=>$this->input->get('bookingPicupDate'),
                     'kms'=>'0',
                     'hours'=>'0',
                     'segments'=>$segment,
                     'bookingDropDate'=>$this->input->get('bookingDropDate'),
                     'sendTime'=>$this->input->get('sendTime'),
                     'bookedFrom'=>$this->input->get('bookedFrom'),
                     'bookedTo'=>$this->input->get('bookedTo'),
                     'numbersOfseat'=>$this->input->get('numbersOfseat'),
                     'latitude'=>$this->input->get('latitude'),
                     'longitude'=>$this->input->get('longitude'),
                     'routeType'=>$this->input->get('routeType'),
                     'pageNo'=>$this->input->get('pageNo'),
                     'totalkms'=>$this->input->get('totalkms'),
                     'city_latitude'=>$this->input->get('city_latitude'),
                     'city_longitude'=>$this->input->get('city_longitude'),
                    );
                }
            }

            elseif ($journey_type == 'coach') {

                if ($userJounarytype == 'local') {
                    $posted_data = array(
                       'userId' => guest_user,
                     'journeyType'=>$this->input->get('journeyType'),
                     'userJounarytype'=>$this->input->get('userJounarytype'),
                     'bookingPicupDate'=>$this->input->get('bookingPicupDate'),
                     'kms'=>$this->input->get('kms'),
                     'hours'=>$this->input->get('hours'),
                     'segments'=>$segment,
                     'bookingDropDate'=>$this->input->get('bookingDropDate'),
                     'sendTime'=>$this->input->get('sendTime'),
                     'bookedFrom'=>$this->input->get('bookedFrom'),
                     'bookedTo'=>$this->input->get('bookedTo'),
                     'numbersOfseat'=>$this->input->get('numbersOfseat'),
                     'latitude'=>$this->input->get('latitude'),
                     'longitude'=>$this->input->get('longitude'),
                     'routeType'=>$this->input->get('routeType'),
                     'pageNo'=>$this->input->get('pageNo'),
                     'totalkms'=>$this->input->get('totalkms'),
                     'city_latitude'=>$this->input->get('city_latitude'),
                     'city_longitude'=>$this->input->get('city_longitude'),
                    );
                } else {
                    $posted_data = array(
                         'userId' => guest_user,
                     'journeyType'=>$this->input->get('journeyType'),
                     'userJounarytype'=>$this->input->get('userJounarytype'),
                     'bookingPicupDate'=>$this->input->get('bookingPicupDate'),
                     'kms'=>'0',
                     'hours'=>'0',
                     'segments'=>$segment,
                     'bookingDropDate'=>$this->input->get('bookingDropDate'),
                     'sendTime'=>$this->input->get('sendTime'),
                     'bookedFrom'=>$this->input->get('bookedFrom'),
                     'bookedTo'=>$this->input->get('bookedTo'),
                     'numbersOfseat'=>$this->input->get('numbersOfseat'),
                     'latitude'=>$this->input->get('latitude'),
                     'longitude'=>$this->input->get('longitude'),
                     'routeType'=>$this->input->get('routeType'),
                     'pageNo'=>$this->input->get('pageNo'),
                     'totalkms'=>$this->input->get('totalkms'),
                     'city_latitude'=>$this->input->get('city_latitude'),
                     'city_longitude'=>$this->input->get('city_longitude'),
                    );
                }
            }
         
        //prd($posted_data);
            $user_api = user_api;
            $url = "$user_api/vehiclemodellist";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            //prd($data);
            curl_close($ch);
            //prd($data);die;
            $data2 = json_decode($data, true);
           //echo"<pre>" ; print_r($data2);die;
            $mydata['posted_data']=$posted_data;
            
            @$mydata['list'] = $data2['payload']['modelList'];
            $mydata['data'] = $data2;
            $mydata['number'] = $data2['payload'];

           
            $data = $this->segment($journey_type);
            $mydata['segment'] = $data['mylist'];
            if ($data2['type'] == 'OK') {
              // echo"atendra<pre>" ; print_r($mydata['list']);
                
             if (isset($_POST['ajax_segment_page_request']) && $_POST['ajax_segment_page_request'] == 1){
                 $this->load->view('ajax_vehicle_list', $mydata);
                    
                }
                else{
                    
                    $data['body']=$this->load->view('vehiclelist',$mydata,true); 
                    $this->load->view('admin_template',$data);
                }
               
               
                 
                
            }

            if ($data2['type'] == 'ERROR') {
                  if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1){
                 $this->load->view('ajax_vehicle_list', $mydata);
                    
                }
                else{
                    
                    $data['body']=$this->load->view('vehiclelist',$mydata,true); 
                    $this->load->view('admin_template',$data);
                }
            }
        } else {
            //$data['body']=$this->load->view('homepage',true);
            //$this->load->view('admin_template',$data);
//            $this->load->view('vehiclelist');
            redirect('homepage');
        }
        
    }

    function segment($journey_type) {

        if (empty($this->input->post(''))) {

            $data = array(
                'vehicleType' => $journey_type,
            );
            //prd($data);
            $user_api = user_api;
            $url = "$user_api/segment";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
            //prd($data2);


            $mydata['mylist'] = $data2['payload'];
            //$mydata['mydata'] = $data2;

            return $mydata;

            //prd($mydata['list']);
//            //$data = array();
//            if ($data2['type'] == 'OK') {
//
//                $this->load->view('segment_list', $mydata);
//            }
//
//            if ($data2['type'] == 'ERROR') {
//
//
//                $this->load->view('segment_list', $mydata);
//            }
        }
//        else {
//
//            $this->load->view('segment_list');
//        }
    }

    function location() {

        //$this->load->view('city_location');
        $data['body']=$this->load->view('vehiclelist','',true);
            $this->load->view('admin_template',$data);
    }
    


 
}
