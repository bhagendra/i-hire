<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booknow extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
    }

    function index() {

       // prd($_GET);

        if (!empty($this->input->get())) {


            if ($this->input->get('journeyType') == 'cab') {
                if ($this->input->get('userJounarytype') == 'local') {
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $this->input->get('numberOfPassengers'),
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'customerMobileNumber' => $this->input->get('customerMobileNumber'),                         'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $this->input->get('customerName'),
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                         'modelName'=>$this->input->get('modelName'),
                         'brandName'=>$this->input->get('brandName'),
                    );
                }
                else{
                    
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $this->input->get('numberOfPassengers'),
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'customerMobileNumber' => $this->input->get('customerMobileNumber'),                         'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $this->input->get('customerName'),
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                         'modelName'=>$this->input->get('modelName'),
                         'brandName'=>$this->input->get('brandName'),
                    );
                    
                }
                
                
            }

 if ($this->input->get('journeyType') == 'coach') {
                if ($this->input->get('userJounarytype') == 'local') {
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $this->input->get('numberOfPassengers'),
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'customerMobileNumber' => $this->input->get('customerMobileNumber'),                         'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $this->input->get('customerName'),
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                         'modelName'=>$this->input->get('modelName'),
                         'brandName'=>$this->input->get('brandName'),
                    );
                }
                else{
                    
                    $posted_data = array(
                        'userId' => USER_ID,
                        'transporterId' => $this->input->get('transporterId'),
                        'vehicleId' => $this->input->get('vehicleId'),
                        'journeyType' => $this->input->get('journeyType'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'numberOfPassengers' => $this->input->get('numberOfPassengers'),
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'customerMobileNumber' => $this->input->get('customerMobileNumber'),                         'userJounarytype' => $this->input->get('userJounarytype'),
                        'vehicleModel' => $this->input->get('vehicleModel'),
                        'vehicleType' => $this->input->get('vehicleType'),
                        'vehicleSegments' => $this->input->get('vehicleSegments'),
                        'extrakmCharses' => $this->input->get('extrakmCharses'),
                        'extrahourseCharses' => $this->input->get('extrahourseCharses'),
                        'outstationbasePrice' => $this->input->get('outstationbasePrice'),
                        'outstationminKms' => $this->input->get('outstationminKms'),
                        'outstationNightCharges' => $this->input->get('outstationNightCharges'),
                        'totalCharge' => $this->input->get('totalCharge'),
                        'customerName' => $this->input->get('customerName'),
                        'email' => $this->input->get('email'),
                        'seeting' => $this->input->get('seeting'),
                        'segments' => $this->input->get('segments'),
                        'totalkms' => $this->input->get('totalkms'),
                        'modelName'=>$this->input->get('modelName'),
                        'brandName'=>$this->input->get('brandName'),
                    );
                    
                }
                
                
            }
//prd($posted_data);

            $user_api = user_api;

            $url = "$user_api/booknow";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
          // prd($posted_data);
//            echo "<pre>";
 //prd($data2);
             $company_details=array(
                
                'companyname'=>$this->input->get('companyname'),
                'avgrate'=>$this->input->get('avgrate'),
                'OnewayOutratePerKm'=>$this->input->get('OnewayOutratePerKm'),
                'RoundTripOutratePerKm'=>$this->input->get('RoundTripOutratePerKm'),
                 'routeType' =>  $this->input->get('routeType'),  

                 
            );
            // prd($company_details);
             $mydata['company_details']=$company_details;
            $mydata['posted_data']=$posted_data;
            $mydata['book'] = $data2['payload'];
            $mydata['data'] = $data2;
            $data = array();
            if ($data2['type'] == 'OK') {
                
               $data1['body']=$this->load->view('booknow',$mydata,true); 
               
               $this->load->view('admin_template',$data1);
                 

            }

            if ($data2['type'] == 'ERROR') {


               $data1['body']=$this->load->view('booknow',$mydata,true); 
               
               $this->load->view('admin_template',$data1);
            }
        } else {

           $data1['body']=$this->load->view('booknow','',true); 
               
               $this->load->view('admin_template',$data1);
        }
    }
    
 

}
