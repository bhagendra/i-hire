<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Selfdrivelist extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');
    }

    function index() {
        //prd($_GET);
        //Common paramete Set values
        $journeyType = $this->input->get('journeyType');
        //  echo $journeyType;die;
        $fromcity = $this->input->get('city');
        $tocity = '';
        $Latitude = $this->input->get('Latitude');
        $Longitude = $this->input->get('Longitude');

        $pick_drop_charges = $this->input->get('pick_drop_charges');
        //Change the Booking Date in Proper Format
        $from_date = $this->input->get('from_date');
        $from_time = $this->input->get('from_time');

        $book_date = date('Y-m-d', strtotime($from_date));

        $from_time_book = date("H:i:s", strtotime($from_time));


        $bookingPicupDate = $book_date . ' ' . $from_time_book;


        $to_date = $this->input->get('to_date');
        $to_time = $this->input->get('to_time');

        $book_todate = date('Y-m-d', strtotime($to_date));

        $to_time_book = date("H:i:s", strtotime($to_time));

        $bookingDropDate = $book_todate . ' ' . $to_time_book;

      // prd( $_POST);
        if(!empty($this->input->post('pageNo'))){
                
                $page_no=$this->input->post('pageNo');
            }
            else{
                
                $page_no='1';
            }
            
            //Setting session for self drive Source
             if (empty($_POST['ajax_segment_request'])&& empty($_POST['ajax_page_request'])){
            $selfdrive_sources['selfdrive_source']=$fromcity;
            
            set_selfdrive_seouce($selfdrive_sources); 
             }
        
        if (!empty($this->input->get())||!empty($this->input->post())) {
        if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1) {
           
            
           $posted_data= array(  
                'userId' => guest_user,
                'journeyType' => $this->input->post('journeyType'),
                'city_latitude' => $this->input->post('city_latitude'),
                'city_longitude' => $this->input->post('city_longitude'),
                'bookingPicupDate' => $this->input->post('bookingPicupDate'),
                'bookingDropDate' => $this->input->post('bookingDropDate'),
                'bookedFrom' => $this->input->post('bookedFrom'),
                'bookedTo' => $this->input->post('bookedTo'),
                'segments' => $this->input->post('segment'),
                'automatic' =>$this->input->post('automatic'),
                'pick_drop_charges' => $this->input->post('pick_drop_charges'),
                'pageNo' => $this->input->post('pageNo'),
            
            );
        
        }
    
        elseif (isset($_GET['ajax_page_request']) && $_GET['ajax_page_request'] == 1) {
           
            
           $posted_data= array(  
                'userId' => guest_user,
                'journeyType' => $this->input->get('journeyType'),
                'city_latitude' => $this->input->get('city_latitude'),
                'city_longitude' => $this->input->get('city_longitude'),
                'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                'bookingDropDate' => $this->input->get('bookingDropDate'),
                'bookedFrom' => $this->input->get('bookedFrom'),
                'bookedTo' => $this->input->get('bookedTo'),
                'segments' => $this->input->get('segment'),
                'automatic' =>$this->input->get('automatic'),
                'pick_drop_charges' => $this->input->get('pick_drop_charges'),
                'pageNo' => $this->input->get('pageNo'),
            
            );
        
        }

           
        
        elseif (!empty($this->input->get())) {
            
            
            
            $posted_data = array(
                'userId' => guest_user,
                'journeyType' => $journeyType,
                'city_latitude' => $Latitude,
                'city_longitude' => $Longitude,
                'bookingPicupDate' => $bookingPicupDate,
                'bookingDropDate' => $bookingDropDate,
                'bookedFrom' => $fromcity,
                'bookedTo' => $tocity,
                'segments' => '0',
                'automatic' => '0',
                'pick_drop_charges' => $pick_drop_charges,
                'pageNo' => '1',
            );
        }
           //prd($posted_data);
            $user_api = user_api;
            $url = "$user_api/selfdrivemodellist";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $mydata['posted_data'] = $posted_data;
            $mydata['data'] = $data2;
            $mydata['number'] = $data2['payload'];
            @$mydata['list'] = $data2['payload']['modelList'];

            $data = $this->segment($journeyType);
            $mydata['segment'] = $data['mylist'];

            //prd($mydata['segment']);
//             prd($data2);
            if ($data2['type'] == 'OK') {

                //echo 'hello';die;

                if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1) {
                    // echo 'ck';die;
                    $this->load->view('selfdrive/ajax_vehicle_list', $mydata);
                } else {

                    $data['body'] = $this->load->view('selfdrive/vehiclelist', $mydata, true);
                    $this->load->view('admin_template', $data);
                }
            }

            if ($data2['type'] == 'ERROR') {

                //echo 'hello';die;

                if (isset($_POST['ajax_segment_request']) && $_POST['ajax_segment_request'] == 1) {
                   
                    $this->load->view('selfdrive/ajax_vehicle_list', $mydata);
                } else {

                    $data['body'] = $this->load->view('selfdrive/vehiclelist', $mydata, true);
                    $this->load->view('admin_template', $data);
                }
            }
        } else {
            
            $data['body'] = $this->load->view('selfdrive/vehiclelist', '', true);
            $this->load->view('admin_template', $data);
        }
    }

    function segment($journeyType) {

        if (empty($this->input->get(''))) {

            $data = array(
                'vehicleType' => $journeyType,
            );
            //prd($data);
            $user_api = user_api;
            $url = "$user_api/segment";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
            //prd($data2);


            $mydata['mylist'] = $data2['payload'];
            //$mydata['mydata'] = $data2;

            return $mydata;
        }
    }

}
