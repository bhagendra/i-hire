<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('session','form_validation');
        $this->load->helper('url');
    }
    
//    function index(){
//        
//        
//    }
    function privacy_policy(){
    
        $data['body']=$this->load->view('pages/privacy_policy','',true);
        $this->load->view('admin_template',$data);
        
    }
     function website_disclaimer(){
    
        $data['body']=$this->load->view('pages/website_disclaimer','',true);
        $this->load->view('admin_template',$data);
        
    }
     function cancellation_policy(){
    
        $data['body']=$this->load->view('pages/cancellation_policy','',true);
        $this->load->view('admin_template',$data);
        
    }
     function terms(){
    
        $data['body']=$this->load->view('pages/terms','',true);
        $this->load->view('admin_template',$data);
        
    }
}
    
   