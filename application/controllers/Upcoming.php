<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upcoming extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');
    }

    function index() {

        if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
            );

            $user_api = user_api;
            $url = "$user_api/upcomming";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
//            echo "<pre>";
//            print_r($data2);
//            die;
            $mydata['data'] = $data2;
            $mydata['list'] = $data2['payload'];
            if ($data2['type'] == 'OK') {

                $data1['body']=$this->load->view('upcoming_list', $mydata,true);
                $this->load->view('admin_template',$data1);
            }

            if ($data2['type'] == 'ERROR') {


                 $data1['body']=$this->load->view('upcoming_list', $mydata,true);
                $this->load->view('admin_template',$data1);
            }
        } else {

             $data1['body']=$this->load->view('upcoming_list','',true);
                $this->load->view('admin_template',$data1);
        }
    }
    
    function booking_history(){
        
             if (!empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/bookingHistory";
            //echo $url;die;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
             //print_r($data);die;
            $data2 = json_decode($data, true);
//            echo "<pre>";
//            print_r($data2);
//            die;
            
            $mydata['data'] = $data2;
            $mydata['list']=$data2['payload'];
                 if ($data2['type'] == 'OK') {

                        
             if (isset($_POST['ajax_history_request']) && $_POST['ajax_history_request'] == 1){
                 $this->load->view('ajax_history_list', $mydata);
                    
                }
                else{
                    
                    $data['body']=$this->load->view('upcoming_list',$mydata,true); 
                    $this->load->view('admin_template',$data);
                }
            }

            if ($data2['type'] == 'ERROR') {

           if (isset($_POST['ajax_history_request']) && $_POST['ajax_history_request'] == 1){
                 $this->load->view('ajax_history_list', $mydata);
                    
                }
                else{
                    
                    $data['body']=$this->load->view('upcoming_list',$mydata,true); 
                    $this->load->view('admin_template',$data);
                }
            }
        } else {

             $data1['body']=$this->load->view('upcoming_list','',true);
                $this->load->view('admin_template',$data1);
        }
        
    }
    
    function cancel_trip($bookedId){
        
        //prd($bookedId);
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
                'bookedId'=>$bookedId,
         
            );
           //prd($data);
            $user_api = user_api;
            $url = "$user_api/canceltrip";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $mydata['data'] = $data2;
            $mydata['details'] = $data2['payload'];
             
               if ($data2['type'] == 'OK') {

                //$data1['body']=$this->load->view('upcoming_list', $mydata,true);
                //$this->load->view('admin_template',$data1);
                   redirect('upcoming');
            }

            if ($data2['type'] == 'ERROR') {

                redirect('upcoming');
                 //$data1['body']=$this->load->view('upcoming_list', $mydata,true);
                //$this->load->view('admin_template',$data1);
            }
        } else {
            
            redirect('upcoming');

             //$data1['body']=$this->load->view('upcoming_list','',true);
               // $this->load->view('admin_template',$data1);
        }
         
}



    function feedback(){
        //prd($_POST);
        //prd($bookedId);
             if (!empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
                'bookingId'=>$this->input->post('bookedId'),
                'title'=>'1dfvd',
                'message'=>$this->input->post('textmessage'),
                'rate'=>$this->input->post('ratevalue'),
         
            );
           //prd($data);
            $user_api = user_api;
            $url = "$user_api/feedback";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
           // prd($data2);
            $mydata['data'] = $data2;
            $mydata['details'] = $data2['payload'];
             
            
             if (isset($_POST['ajax_ratefeedback_request']) && $_POST['ajax_ratefeedback_request'] == 1) {
                if ($data2['type'] == 'ERROR') {
                    echo $data2['message'];
                }
                
                  else if ($data2['type'] == 'OK') {
                   echo $data2['message'];
            }
            }
            else{
               if ($data2['type'] == 'OK') {

                //$data1['body']=$this->load->view('upcoming_list', $mydata,true);
                //$this->load->view('admin_template',$data1);
                   redirect('upcoming');
            }

            if ($data2['type'] == 'ERROR') {

                redirect('upcoming');
                 //$data1['body']=$this->load->view('upcoming_list', $mydata,true);
                //$this->load->view('admin_template',$data1);
             }}
        } else {
            
            redirect('upcoming');

             //$data1['body']=$this->load->view('upcoming_list','',true);
               // $this->load->view('admin_template',$data1);
        }
         
}

 function map(){
     
     $this->load->view('routemap');
 }

}
