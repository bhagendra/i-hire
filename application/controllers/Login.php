<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
    }

    // Login user through CURL 
    function index() {

        if (isset($_POST) && !empty($_POST)) {
            $data = array('email' => $_POST['email'], 'password' => $_POST['password']);
            // prd($data);
            $user_api = user_api;

            $url = "$user_api/login";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);

            // cheak weather a ajax request or simple request

            if (isset($_POST['ajax_request']) && $_POST['ajax_request'] == 1) {
                if ($data2['type'] == 'ERROR') {
                    echo $data2['message'];
                } else if ($data2['type'] == 'OK') {
                    $user_data['id'] = $data2['payload']['id'];
                    $user_data['userName'] = $data2['payload']['userName'];
                    $user_data['mobileNumber'] = $data2['payload']['mobileNumber'];
                    $user_data['emailid'] = $data2['payload']['emailid'];
                    $user_data['city'] = $data2['payload']['city'];

                    //Send Data to Session Ends
                    $user_data['message'] = $data2['message'];
                    $user_data['type'] = $data2['type'];
                    setSession($user_data);
                    echo $user_data['message'];
                }
            } else {
                if ($data2['type'] == 'ERROR') {
                    //  $data['data'] = $data2;
                    $this->load->view('login', $data);
                }
                //Send Data To sesssion
                $user_data['id'] = $data2['payload']['id'];
                $user_data['userName'] = $data2['payload']['userName'];
                $user_data['mobileNumber'] = $data2['payload']['mobileNumber'];
                $user_data['emailid'] = $data2['payload']['emailid'];
                $user_data['city'] = $data2['payload']['city'];

                //Send Data to Session Ends
                $user_data['message'] = $data2['message'];
                $user_data['type'] = $data2['type'];
                $data = array();

                setSession($user_data);

                if ($user_data['type'] == 'OK') {
                    //  $data['data'] = $data2;
                    redirect('homepage', $user_data);
                }
            }
        } else {

            redirect('homepage');
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('homepage');
    }

}
