<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payu_payment extends MY_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('session','form_validation');
        $this->load->helper('url');
    }
    
    function index(){
    
        redirect('homepage');
        
        
    }
    
    function success(){
        
         
          $response_data=array(
              
              'userId'=>USER_ID,
              'transporterId'=>$_SESSION['mydata']['posted_data']['transporterId'],
              'bookingId'=>$_SESSION['mydata']['id']['bookingId'],
              'paymentId'=>$_POST['payuMoneyId'],
              'paymentType'=>$_POST['amount_split'],
              'paymentstatus'=>'1',
              'Paymentreason'=>$_POST['error_Message'],
              'paymentMoney'=>$_POST['net_amount_debit'],
              
          );
          
       //  prd($response_data);
         
           //prd($posted_data);
            //Comment Part
            $user_api = user_api;
           
            $url = "$user_api/paymentstatus";
          
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $response_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            
            $data2 = json_decode($data, true);
        //prd($data2);
        $mydata['details']=$data2['payload'];
        
        $data1['body']=$this->load->view('payumoney/success',$mydata,true);
        
        $this->load->view('admin_template',$data1);
    }
    
     function failure(){
        
         //prd($_POST);
         
          $response_data=array(
              
              'userId'=>USER_ID,
              'transporterId'=>$_SESSION['mydata']['posted_data']['transporterId'],
              'bookingId'=>$_SESSION['mydata']['id']['bookingId'],
              'paymentId'=>$_POST['payuMoneyId'],
              'paymentType'=>$_POST['amount_split'],
              'paymentstatus'=>'0',
              'Paymentreason'=>$_POST['error_Message'],
              'paymentMoney'=>$_POST['net_amount_debit'],
              
          );
          
         //prd($response_data);
         
           //prd($posted_data);
            //Comment Part
            $user_api = user_api;
           
            $url = "$user_api/paymentstatus";
          
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $response_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            
            $data2 = json_decode($data, true);
         
        // prd($data2);
            $mydata['details']=$data2['payload'];
        
      
         $data1['body']= $this->load->view('payumoney/failure',$mydata,true);
         $this->load->view('admin_template',$data1);
    }
}