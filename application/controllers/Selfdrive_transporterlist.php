<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Selfdrive_transporterlist extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');
    }

    function index() {



        if (!empty($this->input->get())||!empty($this->input->post())) {


            if (isset($_GET['ajax_transporter_request']) && $_GET['ajax_transporter_request'] == 1) {


                $posted_data = array(
                    'userId' => guest_user,
                    'journeyType' => $this->input->get('journeyType'),
                    'city_latitude' => $this->input->get('city_latitude'),
                    'city_longitude' => $this->input->get('city_longitude'),
                    'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                    'bookingDropDate' => $this->input->get('bookingDropDate'),
                    'bookedFrom' => $this->input->get('bookedFrom'),
                    'bookedTo' => $this->input->get('bookedTo'),
                    'modelId' => $this->input->get('modelId'),
                    'pick_drop_charges' => $this->input->get('pick_drop_charges'),
                    'pageNo' => $this->input->get('pageNo'),
                );
            }
            
            
            
            if (isset($_POST['ajax_search_request']) && $_POST['ajax_search_request'] == 1) {


                $posted_data = array(
                    'userId' => guest_user,
                    'journeyType' => $this->input->post('journeyType'),
                    'city_latitude' => $this->input->post('city_latitude'),
                    'city_longitude' => $this->input->post('city_longitude'),
                    'bookingPicupDate' => $this->input->post('bookingPicupDate'),
                    'bookingDropDate' => $this->input->post('bookingDropDate'),
                    'bookedFrom' => $this->input->post('bookedFrom'),
                    'bookedTo' => $this->input->post('bookedTo'),
                    'modelId' => $this->input->post('modelId'),
                    'pick_drop_charges' => $this->input->post('pick_drop_charges'),
                    'pageNo' => $this->input->post('pageNo'),
                    'pricefilter' => $this->input->post('pricefilter'),
                    'fuel_type' => $this->input->post('fuel_type'),
                    'rating' => $this->input->post('rating'),
                    'ratingfilter'=> $this->input->post('ratingfilter'),
                    
                    
                );
            }
        // prd($posted_data);
            $user_api = user_api;
            $url = "$user_api/selfdrivesortingvlist";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $mydata['posted_data'] = $posted_data;
            $mydata['data']   = $data2;
            $mydata['number'] = $data2['payload'];
            @$mydata['list']  = $data2['payload']['modelList'];

         //prd($data2);
            if ($data2['type'] == 'OK') {
                   if (isset($_POST['ajax_search_request']) && $_POST['ajax_search_request'] == 1) {
                    // echo 'ck';die;
                    $this->load->view('selfdrive/ajax_transporter_list', $mydata);
                }
               else{ 
                $data1['body'] = $this->load->view('selfdrive/transporter_list', $mydata, true);
                $this->load->view('admin_template', $data1);
               }
            }

            if ($data2['type'] == 'ERROR') {
                   if (isset($_POST['ajax_search_request']) && $_POST['ajax_search_request'] == 1) {
                    // echo 'ck';die;
                    $this->load->view('selfdrive/ajax_transporter_list', $mydata);
                }
               else{ 
                $data1['body'] = $this->load->view('selfdrive/transporter_list', $mydata, true);
                $this->load->view('admin_template', $data1);
               }
            }
        } else {

            $data1['body'] = $this->load->view('selfdrive/transporter_list', '', true);
            $this->load->view('admin_template', $data1);
        }
    }

}
