<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
        
    }
    
    function index(){
        
        
    }
    // NEW USERS Registration
    function register(){
        if (!empty($this->input->post())) {
            $data = array('userName' => $this->input->post('userName'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'state' => $this->input->post('state'),
                'password' => $this->input->post('password'),
            );
           //prd($data);
            $user_api = user_api;
           
            $url = "$user_api/userregation";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            //prd($data2);
          // Register of the Home page Popup and show message
             if (isset($_POST['ajax_register_request']) && $_POST['ajax_register_request'] == 1) {
                if ($data2['type'] == 'ERROR') {
                    echo $data2['message'];
                }
                
                  else if ($data2['type'] == 'OK') {
                   echo $data2['message'];
            }
            }
            // Register of the Other Pages and show message
            else{
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('user_register', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('user_register', $data);
            }
        } } else {

            $this->load->view('user_register');
        }
    }
    
    //UPDATE users Profiles
    
    function update_user(){

        if (!empty($this->input->post())) {
            $data = array('userName' => $this->input->post('userName'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'password' => $this->input->post('password'),
                'userId' => USER_ID,
            );
print_r($data);
            $user_api = user_api;
            $url = "$user_api/userupdate";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('update_user', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('update_user', $data);
            }
        } else {

            $this->load->view('update_user');
        }
    }
    
    //UPDATE/Changed  users password
    
    function change_password() {

        if (!empty($this->input->post())) {
            $data = array('oldpassword' => $this->input->post('oldpassword'),
                'newpassword' => $this->input->post('newpassword'),
                'userId' => USER_ID,
            );
print_r($data);
            $user_api = user_api;
            $url = "$user_api/changePassword";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('change_password', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('change_password', $data);
            }
        } else {

            $this->load->view('change_password');
        }
    }
    
    function userdetail(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
            
            $user_api = user_api;
            $url = "$user_api/userdetail";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
//            echo "<pre>"; print_r($data2); die;
             $mydata['data'] = $data2;
             $mydata['details'] = $data2['payload'];
             
            if ($data2['type'] == 'OK') {
                
                $this->load->view('user_details', $mydata);
            }

            if ($data2['type'] == 'ERROR') {

               
                $this->load->view('user_details', $mydata);
            }
        } else {

            $this->load->view('user_details');
        }
        
    }
    
    function forgot_password(){
             if (!empty($this->input->post())) {
            $data = array(
                'email' => $this->input->post('email'),
         
            );
            //prd($data);
            $user_api = user_api;
            $url = "$user_api/forgotPassword";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
//            echo "<pre>"; print_r($data2); die;
             $mydata['data'] = $data2;
             $mydata['details'] = $data2['payload'];
             
             
              // Register of the Home page Popup and show message
             if (isset($_POST['ajax_forgot_request']) && $_POST['ajax_forgot_request'] == 1) {
                if ($data2['type'] == 'ERROR') {
                    echo $data2['message'];
                }
                
                  else if ($data2['type'] == 'OK') {
                   echo $data2['message'];
            }
            }
            else{
            
             
            if ($data2['type'] == 'OK') {
                
                $this->load->view('forgot_password', $mydata);
            }

            if ($data2['type'] == 'ERROR') {

               
                $this->load->view('forgot_password', $mydata);
            }}
        } else {

            $this->load->view('forgot_password');
        }
        
    }
    
}

