<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_detail extends MY_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
        
    }
    
    function index(){
        
        
    }
    
    
    //UPDATE users Profiles
    
    function update_user(){

        if (!empty($this->input->post())) {
            $data = array('userName' => $this->input->post('userName'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'password' => $this->input->post('password'),
                'userId' => USER_ID,
            );
//print_r($data);die;
            $user_api = user_api;
            $url = "$user_api/userupdate";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $data = array();
            if ($data2['type'] == 'OK') {
                $data_message['data'] = $data2;
                redirect('users_detail/userdetail',$data_message);
            }

            if ($data2['type'] == 'ERROR') {

                $data_message['data'] = $data2;
                //prd($data);die;
                redirect('users_detail/user_edit_view',$data_message);
            }
        } else {

           redirect('users_detail/user_edit_view');
        }
    }
    
    //UPDATE/Changed  users password
    
    function change_password() {

        if (!empty($this->input->post())) {
            $data = array(
                'oldpassword' => $this->input->post('oldpassword'),
                'newpassword' => $this->input->post('newpassword'),
                'userId' => USER_ID,
            );
//print_r($data);
            $user_api = user_api;
            $url = "$user_api/changePassword";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $data1['body']=$this->load->view('change_password',$data,true);
                
                $this->load->view('admin_template', $data1);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $data1['body']=$this->load->view('change_password',$data,true);
                
                $this->load->view('admin_template', $data1);
            }
        } else {

            $data1['body']=$this->load->view('change_password','',true);
                
                $this->load->view('admin_template', $data1);
        }
    }
    
    function userdetail(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
            
            $user_api = user_api;
            $url = "$user_api/userdetail";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
           //echo "<pre>"; print_r($data2); die;
             $mydata['data'] = $data2;
             $mydata['details'] = $data2['payload'];
             
            if ($data2['type'] == 'OK') {
                
                $data1['body']=$this->load->view('user_details',$mydata,true);
                
                $this->load->view('admin_template', $data1);
            }

            if ($data2['type'] == 'ERROR') {

               
                $data1['body']=$this->load->view('user_details',$mydata,true);
                
                $this->load->view('admin_template', $data1);
            }
        } else {

           
                $data1['body']=$this->load->view('user_details','',true);
                
                $this->load->view('admin_template', $data1);
        }
        
    }
    
    
    
    function user_edit_view(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
            
            $user_api = user_api;
            $url = "$user_api/userdetail";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
           //echo "<pre>"; print_r($data2); die;
             $mydata['data'] = $data2;
             $mydata['details'] = $data2['payload'];
             
            if ($data2['type'] == 'OK') {
                
                $data1['body']=$this->load->view('myprofile_edit',$mydata,true);
                
                $this->load->view('admin_template', $data1);
            }

            if ($data2['type'] == 'ERROR') {

                  

                $data1['body']=$this->load->view('myprofile_edit',$mydata,true);
                
                $this->load->view('admin_template', $data1);
            }
        } else {

           
                $data1['body']=$this->load->view('myprofile_edit','',true);
                
                $this->load->view('admin_template', $data1);
        }
        
    }
    
}

