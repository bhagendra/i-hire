<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
        echo USER_NAME;echo USER_MOBILE;echo USER_EMAIL;echo USER_CITY;
    }

    // Login user through CURL 
    public function index() {
        //echo user_api;
        if (isset($_POST) && !empty($_POST)) {
            $data = array('email' => $_POST['email'], 'password' => $_POST['password']);
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/login";
//            echo $url;die;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            print_r($data2);
            // //// die;
            $data = array();

            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('curl', $data);
            }

            if ($data2['type'] == 'ERROR') {
                $data['data'] = $data2;
                $this->load->view('curl', $data);
            }
        } else {

            $this->load->view('curl');
        }

        // $this->load->view();
    }

    // Register user through CURL 

    public function register() {
        if (!empty($this->input->post())) {
            $data = array('userName' => $this->input->post('userName'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'password' => $this->input->post('password'),
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/userregation";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/userregation";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            //print_r($data2);
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('curl_register', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('curl_register', $data);
            }
        } else {

            $this->load->view('curl_register');
        }
    }

    // Update user through CURL 

    function update_user() {

        if (!empty($this->input->post())) {
            $data = array('userName' => $this->input->post('userName'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'password' => $this->input->post('password'),
                'userId' => USER_ID,
            );
print_r($data);
            //$url = "http://apistaging.ihire.in/api_user_third/userupdate";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/userupdate";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            //print_r($data2);die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('update_user', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('update_user', $data);
            }
        } else {

            $this->load->view('update_user');
        }
    }

    // Change Password of user through CURL 
    function change_password() {

        if (!empty($this->input->post())) {
            $data = array('oldpassword' => $this->input->post('oldpassword'),
                'newpassword' => $this->input->post('newpassword'),
                'userId' => USER_ID,
            );
print_r($data);
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/changePassword";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            //print_r($data2);die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('change_password', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('change_password', $data);
            }
        } else {

            $this->load->view('change_password');
        }
    }

    //Search vechile List through CURL 
    function vechile_list() {

        if (empty($this->input->post())) {
            $data = array(
                'userId' => '64',
                'journeyType' => 'cab',
                'userJounarytype' => 'local',
                'bookingPicupDate' => '2012-06-09 01:01:01',
                'hours' => '4',
                'kms' => '40',
                'bookingDropDate' => '2012-06-10 01:01:01',
                'sendTime ' => '8:00:00',
                'segments' => '0',
                'bookedFrom' => 'New Delhi',
                'bookedTo' => '',
                'numbersOfseat' => '0',
                'latitude' => '20.00',
                'longitude' => '12.00',
                'routeType' => '0',
                'pageNo' => '1',
                'totalkms' => '0',
                'city_latitude' => '28.38 ',
                'city_longitude' => '77.12',
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/vehiclemodellist";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            // print_r($data);die;
            $data2 = json_decode($data, true);
            echo "<pre>";
            print_r($data2);
            die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
    }
    
    //Search vechile list second page through curl
     function vechile_list_second_screen() {

        if (empty($this->input->post())) {
            $data = array(
                'userId' => '64',
                'journeyType' => 'cab',
                'userJounarytype' => 'local',
                'bookingPicupDate' => '2012-06-09 01:01:01',
                'hours' => '4',
                'kms' => '40',
                'bookingDropDate' => '2012-06-10 01:01:01',
                'sendTime ' => '8:00:00',
                'segments' => '0',
                'bookedFrom' => 'New Delhi',
                'bookedTo' => '',
                'numbersOfseat' => '0',
                'latitude' => '20.00',
                'longitude' => '12.00',
                'routeType' => '0',
                'pageNo' => '1',
                'totalkms' => '0',
                'city_latitude' => '28.38 ',
                'city_longitude' => '77.12',
                'modelId'=>'106',
                
                'modelId'=>'106',
                'makeYear'=>'2010',
                'ratingfilter'=>'',
                'pricefilter'=>'2',
                'rating'=>'2',
                
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/sortingvehiclelist";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            // print_r($data);die;
            $data2 = json_decode($data, true);
            echo "<pre>";
            print_r($data2);
            die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
    }
    
    // vechile list Details  page through curl
    function vechile_details() {

        if (empty($this->input->post())) {
            $data = array(
                'userId' => '64',
                'vehicleId' => '956',
                'transporterId' => '41',
                'journeyType' => 'cab',
                'bookingPicupDate' => '2012-01-15 01:01:01',
                'bookingDropDate' => '2012-01-15 01:01:01',
                'numberOfPassengers' => '3',
                'bookedFrom ' => 'New Delhi',
                'bookedTo' => '',
                'userJounarytype' => 'local',
                'vehicleModel' => '',
                'vehicleType' => 'cab',
                //
                'vehicleSegments' => '',
                'hours' => '4',
                'kms' => '40',
                'segments' => '1',
                'totalkms' => '0',
                'totalCharge' => '4000 ',
                
                
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/vehicledetail";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            // print_r($data);die;
            $data2 = json_decode($data, true);
            echo "<pre>";
            print_r($data2);
            die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
    }

    
    // Book now page through curl Hitting
    
    function booknow() {

        if (empty($this->input->post())) {
            $data = array(
                'userId' => '64',
                'transporterId' => '41',
                'journeyType' => 'cab',
                
                 'bookingPicupDate' => '2012-01-15 01:01:01',
                 'bookingDropDate' => '2012-01-15 01:01:01',
                 'numberOfPassengers' => '10',
                 'hours' => '4',
                 'kms' => '40',
                 'bookedFrom' => 'New Delhi',
                 'bookedTo' => '',
                 'userJounarytype' => 'local',
                 'vehicleType' => 'cab',
                 'vehicleSegments' => '',
                 'vehicleId' => '956',
                 'segments' => '2',
                 'totalCharge' => '4000',
                'customerName' => 'manish',
                'email' => 'manish.kumar@sirez.com',
                 'seeting' => '5',
                 'vehicleModel'=>'1',
                
                
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/booknow";
            //echo $url;die;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
             //print_r($data);die;
            $data2 = json_decode($data, true);
            echo "<pre>";
            print_r($data2);
            die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
    }
    
    function userbooking(){
        
            if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
                'transporterId' => '41',
                
                'journeyType' => 'cab',
                'bookingPicupDate' => ' 2016-11-15 01:01:01',
                'bookingDropDate' => '2016-11-16 01:01:01',
                'numberOfPassengers' => '10',
                'hours' => '4',
                'kms' => '40',
                'bookedFrom' => 'New Delhi',
                'bookedTo' => '',
                'customerMobileNumber' => '5623984565',
                'address1' => 'ggg',
                'address2' => 'gyyy',
                
                'landmark' => 'hyyjjj',
                'userJounarytype' => 'local',
                'vehicleModel' => '1',
                'vehicleType' => 'cab',
                'vehicleSegments' => '',
                'totalCharge' => '4000',
                'extrakmCharses' => '100',
                'extrahourseCharses' => '100',
                
                'outstationbasePrice' => '100',
                'outstationminKms' => '100',
                'outstationNightCharges' => '100',
                'customerName' => 'manish',
                'email' => 'manish.kumar22@sirez.com',
                'seeting' => '5',
                
                
                
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/userbooking";
            //echo $url;die;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
             //print_r($data);die;
            $data2 = json_decode($data, true);
            echo "<pre>";
            print_r($data2);
            die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
        
    }
    
    function bookingHistory(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/bookingHistory";
            //echo $url;die;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
             //print_r($data);die;
            $data2 = json_decode($data, true);
            echo "<pre>";
            print_r($data2);
            die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
        
    }
    
    
    function userdetail(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
            
            $user_api = user_api;
            $url = "$user_api/userdetail";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            echo "<pre>"; print_r($data2); die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
        
    }
    
    
    function upcomming(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
            
            $user_api = user_api;
            $url = "$user_api/upcomming";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            echo "<pre>"; print_r($data2); die;
            $data = array();
            if ($data2['type'] == 'OK') {
                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('vechile_list', $data);
            }
        } else {

            $this->load->view('vechile_list');
        }
        
    }
    
    
    
}
