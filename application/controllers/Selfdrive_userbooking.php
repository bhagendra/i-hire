<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Selfdrive_userbooking extends MY_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
    }
    
    function index(){
        //prd($_GET);
            if (!empty($this->input->get())) {
                
                $customer_original_name=$this->input->get('customerName');
                $customer_changed_name=$this->input->get('customer_name');
                
                if($customer_original_name==$customer_changed_name){
                    
                    $booked_customer=$customer_original_name;
                }
                if($customer_original_name!=$customer_changed_name){
                    
                    $booked_customer=$customer_changed_name;
                }
                //echo $booked_customer;die;
                 $customer_original_number=$this->input->get('customerMobileNumber');
                 $customer_changed_number=$this->input->get('mobile_number');
                 
                 if($customer_original_number==$customer_changed_number){
                    
                    $booked_number=$customer_original_number;
                }
                if($customer_original_number!=$customer_changed_number){
                    
                    $booked_number=$customer_changed_number;
                }
                
               $posted_data=array(
                   
                   'userId'=>USER_ID,
                   'transporterId'=>$this->input->get('transporterId'),
                   'journeyType'=>$this->input->get('journeyType'),
                   'bookingPicupDate'=>$this->input->get('bookingPicupDate'),
                   'bookingDropDate'=>$this->input->get('bookingDropDate'),
                   
                   'bookedFrom'=>$this->input->get('bookedFrom'),
                   'customerMobileNumber'=>$booked_number,
                   'address1'=>$this->input->get('address'),
                   
                   'userJounarytype'=>$this->input->get('userJounarytype'),
                   'vehicleModel'=>$this->input->get('vehicleModel'),
                   
                   'userlicencenumber'=>$this->input->get('lisence'),
                   'vehicleType'=>$this->input->get('vehicleType'),
                   'vehicleSegments'=>$this->input->get('segementsName'),
                   
                   'totalCharge'=>$this->input->get('totalCharge'),
                   'customerName'=>$booked_customer,
                   'email'=>$this->input->get('email'),
                   
                   'extrakmCharses'=>$this->input->get('extraKmsCharges'),
                   
                   'segementsName'=>$this->input->get('segementsName'),
                    'modelName'=>$this->input->get('modelName'),
                    'brandName'=>$this->input->get('brandName'),
                    'numbersOfseat'=>$this->input->get('numbersOfseat'),
                   
               );
                //echo $booked_number;die;
                
            
            
           //prd($posted_data);
            //Comment Part
            $user_api = user_api;
           
            $url = "$user_api/selfuserbooking";
          
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            
            $data2 = json_decode($data, true);

            //prd($data2);
            $mydata['id']=$data2['payload'];
            $mydata['data'] = $data2;
            
             //Comment Part ends
               $mydata['posted_data']=$posted_data;
                 $company_details=array(
                
                'companyname'=>$this->input->get('companyname'),
                'avgrate'=>$this->input->get('avgrate'),
                 
            );
             //prd($company_details);
             $mydata['company_details']=$company_details;
           
                set_session_payment($mydata);
            
           
            
            if ($data2['type'] == 'OK') {
               redirect ('userbooking/payment_getaway');
              
            }

            if ($data2['type'] == 'ERROR') {
                
               redirect ('userbooking/payment_getaway');
            }
        } else {

           redirect('homepage');
        }
        
    }
    
    
}

