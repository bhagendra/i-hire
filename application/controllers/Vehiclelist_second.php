<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiclelist_second extends CI_Controller {

    function __construct() {
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
    }

    function index() {

//echo'<pre>';print_r($_GET);
      // prd($_POST);

        if (!empty($this->input->get()) || !empty($this->input->post())) {

            if (isset($_POST['ajax_price_request']) && $_POST['ajax_price_request'] == 1||isset($_POST['ajax_rating_request']) && $_POST['ajax_rating_request'] == 1 ||isset($_POST['ajax_ratingfilter_request']) && $_POST['ajax_ratingfilter_request'] == 1) {

                if(!empty($this->input->post('ratingId')))
                {
                    
                  $rating=$this->input->post('ratingId');
                }
                else{
                    
                    $rating='';
                }
                 if(!empty($this->input->post('ratingsearchvalue')))
                {
                    
                  $ratings=$this->input->post('ratingsearchvalue');
                }
                else{
                    
                    $ratings='';
                }
                
              //  prd($ratings);
                if ($_POST['journeyType'] == 'cab') {
                    if ($this->input->post('userJounarytype') == 'local') {
                        $posted_data = array(
                            'userId' => guest_user,
                            'journeyType' => $this->input->post('journeyType'),
                            'userJounarytype' => $this->input->post('userJounarytype'),
                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
                            'hours' => $this->input->post('hours'),
                            'kms' => $this->input->post('kms'),
                            'bookingDropDate' => $this->input->post('bookingDropDate'),
                            'sendTime' => $this->input->post('sendTime'),
                            'segments' => '0',
                            'bookedFrom' => $this->input->post('bookedFrom'),
                            'bookedTo' => '',
                            'numbersOfseat' => '0',
                            'latitude' => '20.00',
                            'longitude' => '12.00',
                            'routeType' => '0',
                            'pageNo' => $this->input->post('pageNo'),
                            'totalkms' => '0',
                            'city_latitude' => $this->input->post('city_latitude'),
                            'city_longitude' => $this->input->post('city_longitude'),
                            'modelId' => $this->input->post('modelId'),
                            'pricefilter' => $this->input->post('pricevalue'),
                            'ratingfilter'=>$rating,
                            'rating'=>$ratings,
                        );
                    } else {

                        $posted_data = array(
                            'userId' => guest_user,
                            'journeyType' => $this->input->post('journeyType'),
                            'userJounarytype' => $this->input->post('userJounarytype'),
                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
                            'hours' => '0',
                            'kms' => '0',
                            'bookingDropDate' => $this->input->post('bookingDropDate'),
                            'sendTime' => $this->input->post('sendTime'),
                            'segments' => '0',
                            'bookedFrom' => $this->input->post('bookedFrom'),
                            'bookedTo' => $this->input->post('bookedTo'),
                            'numbersOfseat' => '0',
                            'latitude' => '20.00',
                            'longitude' => '12.00',
                            'routeType' => $this->input->post('routeType'),
                            'pageNo' => $this->input->post('pageNo'),
                            'totalkms' => $this->input->post('totalkms'),
                            'city_latitude' => $this->input->post('city_latitude'),
                            'city_longitude' => $this->input->post('city_longitude'),
                            'modelId' => $this->input->post('modelId'),
                            'pricefilter' => $this->input->post('pricevalue'),
                             'ratingfilter'=>$rating,
                            'rating'=>$ratings,
                        );
                    }
                } elseif ($this->input->post('journeyType') == 'coach') {
                    if ($this->input->post('userJounarytype') == 'local') {
                        $posted_data = array(
                            'userId' => guest_user,
                            'journeyType' => $this->input->post('journeyType'),
                            'userJounarytype' => $this->input->post('userJounarytype'),
                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
                            'hours' => $this->input->post('hours'),
                            'kms' => $this->input->post('kms'),
                            'bookingDropDate' => $this->input->post('bookingDropDate'),
                            'sendTime' => $this->input->post('sendTime'),
                            'segments' => $this->input->post('segmentId'),
                            'bookedFrom' => $this->input->post('bookedFrom'),
                            'bookedTo' => '',
                            'numbersOfseat' => $this->input->post('numbersOfseat'),
                            'latitude' => '20.00',
                            'longitude' => '12.00',
                            'routeType' => '0',
                            'pageNo' => $this->input->post('pageNo'),
                            'totalkms' => '0',
                            'city_latitude' => $this->input->post('city_latitude'),
                            'city_longitude' => $this->input->post('city_longitude'),
                            'modelId' => '0',
                            'pricefilter' => $this->input->post('pricevalue'),
                             'ratingfilter'=>$rating,
                            'rating'=>$ratings,
                        );
                    } else {
                        $posted_data = array(
                            'userId' => guest_user,
                            'journeyType' => $this->input->post('journeyType'),
                            'userJounarytype' => $this->input->post('userJounarytype'),
                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
                            'hours' => '0',
                            'kms' => '0',
                            'bookingDropDate' => $this->input->post('bookingDropDate'),
                            'sendTime' => $this->input->post('sendTime'),
                            'segments' => $this->input->post('segmentId'),
                            'bookedFrom' => $this->input->post('bookedFrom'),
                            'bookedTo' => $this->input->post('bookedTo'),
                            'numbersOfseat' => $this->input->post('numbersOfseat'),
                            'latitude' => '20.00',
                            'longitude' => '12.00',
                            'routeType' => $this->input->post('routeType'),
                            'pageNo' => $this->input->post('pageNo'),
                            'totalkms' => $this->input->post('totalkms'),
                            'city_latitude' => $this->input->post('city_latitude'),
                            'city_longitude' => $this->input->post('city_longitude'),
                            'modelId' => '0',
                            'pricefilter' => $this->input->post('pricevalue'),
                             'ratingfilter'=>$rating,
                            'rating'=>$ratings,
                        );
                    }
                }
            } 
            
//             elseif (isset($_POST['ajax_pagination_request']) && $_POST['ajax_pagination_request'] == 1) {
//
//                if ($_POST['journeyType'] == 'cab') {
//                    if ($this->input->post('userJounarytype') == 'local') {
//                        $posted_data = array(
//                            'userId' => guest_user,
//                            'journeyType' => $this->input->post('journeyType'),
//                            'userJounarytype' => $this->input->post('userJounarytype'),
//                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
//                            'hours' => $this->input->post('hours'),
//                            'kms' => $this->input->post('kms'),
//                            'bookingDropDate' => $this->input->post('bookingDropDate'),
//                            'sendTime' => $this->input->post('sendTime'),
//                            'segments' => '0',
//                            'bookedFrom' => $this->input->post('bookedFrom'),
//                            'bookedTo' => '',
//                            'numbersOfseat' => '0',
//                            'latitude' => '20.00',
//                            'longitude' => '12.00',
//                            'routeType' => '0',
//                            'pageNo' => '1',
//                            'totalkms' => '0',
//                            'city_latitude' => $this->input->post('city_latitude'),
//                            'city_longitude' => $this->input->post('city_longitude'),
//                            'modelId' => $this->input->post('modelId'),
//                            //'pricefilter' => $this->input->post('pricevalue'),
//                        );
//                    } else {
//
//                        $posted_data = array(
//                            'userId' => guest_user,
//                            'journeyType' => $this->input->post('journeyType'),
//                            'userJounarytype' => $this->input->post('userJounarytype'),
//                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
//                            'hours' => '0',
//                            'kms' => '0',
//                            'bookingDropDate' => $this->input->post('bookingDropDate'),
//                            'sendTime' => $this->input->post('sendTime'),
//                            'segments' => '0',
//                            'bookedFrom' => $this->input->post('bookedFrom'),
//                            'bookedTo' => $this->input->post('bookedTo'),
//                            'numbersOfseat' => '0',
//                            'latitude' => '20.00',
//                            'longitude' => '12.00',
//                            'routeType' => $this->input->post('routeType'),
//                            'pageNo' => $this->input->post('pageNo'),
//                            'totalkms' => $this->input->post('totalkms'),
//                            'city_latitude' => $this->input->post('city_latitude'),
//                            'city_longitude' => $this->input->post('city_longitude'),
//                            'modelId' => $this->input->post('modelId'),
//                            //'pricefilter' => $this->input->post('pricevalue'),
//                        );
//                    }
//                } elseif ($this->input->post('journeyType') == 'coach') {
//                    if ($this->input->post('userJounarytype') == 'local') {
//                        $posted_data = array(
//                            'userId' => guest_user,
//                            'journeyType' => $this->input->post('journeyType'),
//                            'userJounarytype' => $this->input->post('userJounarytype'),
//                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
//                            'hours' => $this->input->post('hours'),
//                            'kms' => $this->input->post('kms'),
//                            'bookingDropDate' => $this->input->post('bookingDropDate'),
//                            'sendTime' => $this->input->post('sendTime'),
//                            'segments' => $this->input->post('segmentId'),
//                            'bookedFrom' => $this->input->post('bookedFrom'),
//                            'bookedTo' => '',
//                            'numbersOfseat' => $this->input->post('numbersOfseat'),
//                            'latitude' => '20.00',
//                            'longitude' => '12.00',
//                            'routeType' => '0',
//                             'pageNo' => $this->input->post('pageNo'),
//                            'totalkms' => '0',
//                            'city_latitude' => $this->input->post('city_latitude'),
//                            'city_longitude' => $this->input->post('city_longitude'),
//                            'modelId' => '0',
//                            //'pricefilter' => $this->input->post('pricevalue'),
//                        );
//                    } else {
//                        $posted_data = array(
//                            'userId' => guest_user,
//                            'journeyType' => $this->input->post('journeyType'),
//                            'userJounarytype' => $this->input->post('userJounarytype'),
//                            'bookingPicupDate' => $this->input->post('bookingPicupDate'),
//                            'hours' => '0',
//                            'kms' => '0',
//                            'bookingDropDate' => $this->input->post('bookingDropDate'),
//                            'sendTime' => $this->input->post('sendTime'),
//                            'segments' => $this->input->post('segmentId'),
//                            'bookedFrom' => $this->input->post('bookedFrom'),
//                            'bookedTo' => $this->input->post('bookedTo'),
//                            'numbersOfseat' => $this->input->post('numbersOfseat'),
//                            'latitude' => '20.00',
//                            'longitude' => '12.00',
//                            'routeType' => $this->input->post('routeType'),
//                            'pageNo' => $this->input->post('pageNo'),
//                            'totalkms' => $this->input->post('totalkms'),
//                            'city_latitude' => $this->input->post('city_latitude'),
//                            'city_longitude' => $this->input->post('city_longitude'),
//                            'modelId' => '0',
//                            //'pricefilter' => $this->input->post('pricevalue'),
//                        );
//                    }
//                }
//            }
            
            
            
            
            elseif ($this->input->get('journeyType') == 'cab') {
                
                if($this->input->get('pageNo')=='1'||''||'0' || $this->input->get('ajax_segment_request')=='1' ){
                    
                   $pageno='1';  
                }
                else{
                 $pageno= $this->input->get('pageNo');
                    
                }
                if ($this->input->get('userJounarytype') == 'local') {
                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $this->input->get('journeyType'),
                        'userJounarytype' => $this->input->get('userJounarytype'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'sendTime' => $this->input->get('sendTime'),
                        'segments' => '0',
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => '',
                        'numbersOfseat' => '0',
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'routeType' => '0',
                        'pageNo' => $pageno,
                        'totalkms' => '0',
                        'city_latitude' => $this->input->get('city_latitude'),
                        'city_longitude' => $this->input->get('city_longitude'),
                        'modelId' => $this->input->get('modelId'),
                        'pricefilter' => '1',
                    );
                } else {

                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $this->input->get('journeyType'),
                        'userJounarytype' => $this->input->get('userJounarytype'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'hours' => '0',
                        'kms' => '0',
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'sendTime' => $this->input->get('sendTime'),
                        'segments' => '0',
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'numbersOfseat' => '0',
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'routeType' => $this->input->get('routeType'),
                        'pageNo' => $pageno,
                        'totalkms' => $this->input->get('totalkms'),
                        'city_latitude' => $this->input->get('city_latitude'),
                        'city_longitude' => $this->input->get('city_longitude'),
                        'modelId' => $this->input->get('modelId'),
                        'pricefilter' => '1',
                    );
                }
            } elseif ($this->input->get('journeyType') == 'coach') {
                  if($this->input->get('pageNo')=='1'||''||'0' || $this->input->get('ajax_segment_request')=='1' ){
                    
                  $pageno='1';  
                }
                else{
                   $pageno= $this->input->get('pageNo');
                    
                }
                if ($this->input->get('userJounarytype') == 'local') {
                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $this->input->get('journeyType'),
                        'userJounarytype' => $this->input->get('userJounarytype'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'hours' => $this->input->get('hours'),
                        'kms' => $this->input->get('kms'),
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'sendTime' => $this->input->get('sendTime'),
                        'segments' => $this->input->get('modelId'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => '',
                        'numbersOfseat' => $this->input->get('numbersOfseat'),
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'routeType' => '0',
                        'pageNo' => $pageno,
                        'totalkms' => '0',
                        'city_latitude' => $this->input->get('city_latitude'),
                        'city_longitude' => $this->input->get('city_longitude'),
                        'modelId' => '0',
                        'pricefilter' => '1',
                    );
                } else {
                    $posted_data = array(
                        'userId' => guest_user,
                        'journeyType' => $this->input->get('journeyType'),
                        'userJounarytype' => $this->input->get('userJounarytype'),
                        'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                        'hours' => '0',
                        'kms' => '0',
                        'bookingDropDate' => $this->input->get('bookingDropDate'),
                        'sendTime' => $this->input->get('sendTime'),
                        'segments' => $this->input->get('modelId'),
                        'bookedFrom' => $this->input->get('bookedFrom'),
                        'bookedTo' => $this->input->get('bookedTo'),
                        'numbersOfseat' => $this->input->get('numbersOfseat'),
                        'latitude' => '20.00',
                        'longitude' => '12.00',
                        'routeType' => $this->input->get('routeType'),
                        'pageNo' => $pageno,
                        'totalkms' => $this->input->get('totalkms'),
                        'city_latitude' => $this->input->get('city_latitude'),
                        'city_longitude' => $this->input->get('city_longitude'),
                        'modelId' => '0',
                        'pricefilter' => '1',
                    );
                }
            }
           //prd($posted_data);
//                       echo "<pre>";
//                     print_r($posted_data);die;
            $user_api = user_api;

            $url = "$user_api/sortingvehiclelist";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);

            $data2 = json_decode($data, true);
//           echo "<pre>";
//                     print_r($data2);
          
            $mydata['posted_data'] = $posted_data;
            @$mydata['list'] = $data2['payload']['bookinglist']['allvehicle'];
            $mydata['number'] = $data2['payload'];
            $mydata['mdata'] = $data2;


           //prd($data2);

            if ($data2['type'] == 'OK') {
                     if (isset($_POST['ajax_price_request']) && $_POST['ajax_price_request'] == 1){
                 $this->load->view('transporter_list_ajax', $mydata);
                    
                }
                      elseif (isset($_POST['ajax_rating_request']) && $_POST['ajax_rating_request'] == 1){
                 $this->load->view('transporter_list_ajax', $mydata);
                    
                }
                
                     elseif (isset($_POST['ajax_ratingfilter_request']) && $_POST['ajax_ratingfilter_request'] == 1){
                 $this->load->view('transporter_list_ajax', $mydata);
                    
                }
                else{
                    
                    $data1['body']=$this->load->view('vehicle_list_second',$mydata,true); 
                    $this->load->view('admin_template',$data1);
                }
               
            }

            if ($data2['type'] == 'ERROR') {


                //$this->load->view('vehicle_list_second', $mydata);
                $data1['body'] = $this->load->view('vehicle_list_second', $mydata, true);
                $this->load->view('admin_template', $data1);
            }
            
        } else {

            $data1['body'] = $this->load->view('vehicle_list_second', '', true);
            $this->load->view('admin_template', $data1);
        }
    }

}
