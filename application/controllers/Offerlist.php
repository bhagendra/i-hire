<?php

defined('BASEPATH') OR exit('No Direct Script Access Allowed');

class Offerlist extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');
    }

    function index() {

        if (empty($this->input->post(''))) {

            $data = array(
                'userId' => USER_ID,
                'vehicleType' => '',
                'destinationId' => '',
                'originId' => '',
            );
            $user_api = user_api;
            $url = "$user_api/offerList";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
           // prd($data2);
            
                
            $mydata['list']=$data2['payload'];
            $mydata['data']=$data2;
            //$data = array();
            if ($data2['type'] == 'OK') {
              
                $this->load->view('offerlist', $mydata);
            }

            if ($data2['type'] == 'ERROR') {

               
                $this->load->view('offerlist', $mydata);
            }
            
        } else {

            $this->load->view('offerlist');
        }
    }

}
