<?php

defined("BASEPATH") OR exit('No direct script access allowed');

class Bookinghistory extends MY_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation','session');
        $this->load->helper('url');
        
    }
    
    function index(){
        
             if (empty($this->input->post())) {
            $data = array(
                'userId' => USER_ID,
         
            );
//print_r($data);die;
            //$url = "http://apistaging.ihire.in/api_user_third/changePassword";
            $user_api = user_api;
            //echo $user_api;die;
            $url = "$user_api/bookingHistory";
            //echo $url;die;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
             //print_r($data);die;
            $data2 = json_decode($data, true);
//            echo "<pre>";
//            print_r($data2);
//            die;
            
            $mydata['data'] = $data2;
            $mydata['list']=$data2['payload'];
            if ($data2['type'] == 'OK') {
                
                $this->load->view('booking_history', $mydata);
            }

            if ($data2['type'] == 'ERROR') {

                $data['data'] = $data2;
                $this->load->view('booking_history', $mydata);
            }
        } else {

            $this->load->view('booking_history');
        }
        
    }
    
    
}
