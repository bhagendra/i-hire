<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_detail extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
        
    }
    
    function index(){
//prd($_GET);
        if (!empty($this->input->get())) {
            
            if($this->input->get('journeyType')=='cab'){
              if($this->input->get('userJounarytype')=='local'){  
               $posted_data = array(
                'userId' => guest_user,
                'vehicleId' => $this->input->get('vehicleId'),
                'transporterId' => $this->input->get('transporterId'),
                'journeyType' => $this->input->get('journeyType'),
                'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                'bookingDropDate' => $this->input->get('bookingDropDate'),
                'numberOfPassengers' => $this->input->get('numbersOfseat'),
                'bookedFrom' => $this->input->get('bookedFrom'),
                'bookedTo' => $this->input->get('bookedTo'),
                'userJounarytype' => $this->input->get('userJounarytype'),
                'vehicleModel' => '',
                'vehicleType' => $this->input->get('userJounarytype'),
                //
                'vehicleSegments' => $this->input->get('segmentsName'),
                'hours' => $this->input->get('hours'),
                'kms' => $this->input->get('kms'),
                'segments' => $this->input->get('segmentsIds'),
                'totalkms' => $this->input->get('totalkms'),
                'totalCharge' => $this->input->get('totalcharge'),
                
                'totalchargebydays' => $this->input->get('totalchargebydays'),
                'totalchargebykm' => $this->input->get('totalchargebykm'),
                'discountprice' => $this->input->get('discountprice'),
                'baseprice' => $this->input->get('baseprice'),
                'addnightcharges' => $this->input->get('addnightcharges'),
                'adddrivercharge' => $this->input->get('adddrivercharge'),
                'city_latitude' => $this->input->get('city_latitude'),
                'city_longitude' => $this->input->get('city_longitude'),
                
                
                
            );
            }
            else{
                 $posted_data = array(
                'userId' => guest_user,
                'vehicleId' => $this->input->get('vehicleId'),
                'transporterId' => $this->input->get('transporterId'),
                'journeyType' => $this->input->get('journeyType'),
                'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                'bookingDropDate' => $this->input->get('bookingDropDate'),
                'numberOfPassengers' => $this->input->get('numbersOfseat'),
                'bookedFrom' => $this->input->get('bookedFrom'),
                'bookedTo' => $this->input->get('bookedTo'),
                'userJounarytype' => $this->input->get('userJounarytype'),
                'vehicleModel' => '',
                'vehicleType' => $this->input->get('userJounarytype'),
                //
                'vehicleSegments' => $this->input->get('segmentsName'),
                'hours' =>'0',
                'kms' => '0',
                'segments' => $this->input->get('segmentsIds'),
                'totalkms' => $this->input->get('totalkms'),
                'totalCharge' => $this->input->get('totalcharge'),
                
                'totalchargebydays' => $this->input->get('totalchargebydays'),
                'totalchargebykm' => $this->input->get('totalchargebykm'),
                'discountprice' => $this->input->get('discountprice'),
                'baseprice' => $this->input->get('baseprice'),
                'addnightcharges' => $this->input->get('addnightcharges'),
                'adddrivercharge' => $this->input->get('adddrivercharge'),
                'city_latitude' => $this->input->get('city_latitude'),
                'city_longitude' => $this->input->get('city_longitude'),
                );
            }
            
              }
             elseif($this->input->get('journeyType')=='coach'){
              if($this->input->get('userJounarytype')=='local'){  
                $posted_data = array(
                'userId' => guest_user,
                'vehicleId' => $this->input->get('vehicleId'),
                'transporterId' => $this->input->get('transporterId'),
                'journeyType' => $this->input->get('journeyType'),
                'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                'bookingDropDate' => $this->input->get('bookingDropDate'),
                'numberOfPassengers' => $this->input->get('numbersOfseat'),
                'bookedFrom' => $this->input->get('bookedFrom'),
                'bookedTo' => $this->input->get('bookedTo'),
                'userJounarytype' => $this->input->get('userJounarytype'),
                'vehicleModel' => '',
                'vehicleType' => $this->input->get('userJounarytype'),
                //
                'vehicleSegments' => $this->input->get('segmentsName'),
                'hours' => $this->input->get('hours'),
                'kms' => $this->input->get('kms'),
                'segments' => $this->input->get('segmentsIds'),
                'totalkms' => $this->input->get('totalkms'),
                'totalCharge' => $this->input->get('totalcharge'),
                
                'totalchargebydays' => $this->input->get('totalchargebydays'),
                'totalchargebykm' => $this->input->get('totalchargebykm'),
                'discountprice' => $this->input->get('discountprice'),
                'baseprice' => $this->input->get('baseprice'),
                'addnightcharges' => $this->input->get('addnightcharges'),
                'adddrivercharge' => $this->input->get('adddrivercharge'),
                'city_latitude' => $this->input->get('city_latitude'),
                'city_longitude' => $this->input->get('city_longitude'),
                
                
            );
            }
            else{
                 $posted_data = array(
                'userId' => guest_user,
                'vehicleId' => $this->input->get('vehicleId'),
                'transporterId' => $this->input->get('transporterId'),
                'journeyType' => $this->input->get('journeyType'),
                'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                'bookingDropDate' => $this->input->get('bookingDropDate'),
                'numberOfPassengers' => $this->input->get('numbersOfseat'),
                'bookedFrom' => $this->input->get('bookedFrom'),
                'bookedTo' => $this->input->get('bookedTo'),
                'userJounarytype' => $this->input->get('userJounarytype'),
                'vehicleModel' => '',
                'vehicleType' => $this->input->get('userJounarytype'),
                //
                'vehicleSegments' => $this->input->get('segmentsName'),
                'hours' =>'0',
                'kms' => '0',
                'segments' => $this->input->get('segmentsIds'),
                'totalkms' => $this->input->get('totalkms'),
                'totalCharge' => $this->input->get('totalcharge'),
                
                'totalchargebydays' => $this->input->get('totalchargebydays'),
                'totalchargebykm' => $this->input->get('totalchargebykm'),
                'discountprice' => $this->input->get('discountprice'),
                'baseprice' => $this->input->get('baseprice'),
                'addnightcharges' => $this->input->get('addnightcharges'),
                'adddrivercharge' => $this->input->get('adddrivercharge'),
                'city_latitude' => $this->input->get('city_latitude'),
                'city_longitude' => $this->input->get('city_longitude'),
                );
            }
            
              }
              
              //prd($posted_data);
            $user_api = user_api;
            $url = "$user_api/vehicledetail";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
           
            $data2 = json_decode($data, true);
       //prd($data2);
//            echo "<pre>";
          // prd($data2);
//           
//            
//            $mydata['images']=$data2['payload']['images'];
            //Myarray for Company name and rating Start
            
            $company_details=array(
                
                'companyName'=>$this->input->get('companyName'),
                'avgrate'=>$this->input->get('avgrate'),
                'afterdiscountPrice'=>$this->input->get('afterdiscountPrice'),
                'routeType' =>  $this->input->get('routeType'),  
               
            );
         //prd($company_details);
            
            $mydata['company_details']=$company_details;
            //Myarray For Company name and Rating
            
            $mydata['data']=$data2;
            $mydata['rates']=$data2['payload']['rates'];
            $mydata['vehicledetails']=$data2['payload']['vehicledetails'];
            
            $mydata['terms']=$data2['payload']['terms'];
            $mydata['bookinginfo']=$data2['payload']['bookinginfo'];
            $mydata['posted_data']=$posted_data;
              
            
           // prd($mydata['rates']);
            
            if ($data2['type'] == 'OK') {
                 $data1['body']=$this->load->view('vehicle_detail',$mydata,true);
                 $this->load->view('admin_template',$data1);
            }

            if ($data2['type'] == 'ERROR') {
                 $data1['body']=$this->load->view('vehicle_detail',$mydata,true);
                 $this->load->view('admin_template',$data1);
            }
        } else {

            $data1['body']=$this->load->view('vehicle_detail','',true);
            $this->load->view('admin_template',$data1);
        }
    }
    
}