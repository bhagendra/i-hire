<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends MY_Controller{
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('session','form_validation');
        $this->load->helper('url');
    }
    
    function index(){
        if(empty($this->input->post(''))){
        $data=array(
            
            'transporterId'=>'7',
      
        );
        
        //prd($data);
        
        $user_api=user_api;
        
        $url="$user_api/review";
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            //print_r($data);die;
            $data2 = json_decode($data, true);
//            echo "<pre>";
//            print_r($data2);
//             die;
            $mydata['list'] = $data2['payload']['reviewlist'];
            $mydata['data'] = $data2;
            $mydata['number'] = $data2['payload'];
            
           // prd($mydata['list']);
            
            

            if ($data2['type'] == 'OK') {
                $this->load->view('review_list', $mydata);
            }

            if ($data2['type'] == 'ERROR') {
                $this->load->view('review_list', $mydata);
            }
        
        }
        else{
            
           $this->load->view('review_list'); 
            
        }
        
        
    }
}