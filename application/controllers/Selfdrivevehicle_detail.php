<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Selfdrivevehicle_detail extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');
    }

    function index() {

//prd($_SESSION);

        if (!empty($this->input->get())||!empty($this->input->post())) {


            if (isset($_GET['ajax_booknow_request']) && $_GET['ajax_booknow_request'] == 1) {


                $posted_data = array(
                    'userId' => guest_user,
                    'journeyType' => $this->input->get('journeyType'),
                    'userJounarytype' => $this->input->get('userJounarytype'),
                    'city_latitude' => $this->input->get('city_latitude'),
                    'city_longitude' => $this->input->get('city_longitude'),
                    'bookingPicupDate' => $this->input->get('bookingPicupDate'),
                    'bookingDropDate' => $this->input->get('bookingDropDate'),
                    'bookedFrom' => $this->input->get('bookedFrom'),
                    'bookedTo' => $this->input->get('bookedTo'),
                    
                    'pick_drop_charges' => $this->input->get('pick_drop_charges'),
                    'modelId' => $this->input->get('modelId'),
                    'vehicleId' => $this->input->get('vehicleId'),
                    'transporterId' => $this->input->get('transporterId'),
                    'totalCharge' => $this->input->get('totalCharge'),
                    'segments' => $this->input->get('segments'),
                    'vehicleType' => $this->input->get('vehicleType'),
                    'segementsName' => $this->input->get('segementsName'),
                    'companyName' => $this->input->get('companyName'),
                    'avgrate' => $this->input->get('avgrate'),
                     
                    
                    
                );
            }
            
            
         
         //prd($posted_data);
            $user_api = user_api;
            $url = "$user_api/selfvehicledetail";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $posted_data);
            @curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $data = curl_exec($ch);
            curl_close($ch);
            $data2 = json_decode($data, true);
            //prd($data2);
            $mydata['data']=$data2;
            $mydata['rates']=$data2['payload']['rates'];
            $mydata['vehicledetails']=$data2['payload']['vehicledetails'];
            
            $mydata['terms']=$data2['payload']['terms'];
            $mydata['bookinginfo']=$data2['payload']['bookinginfo'];
            $mydata['posted_data']=$posted_data;
            
             $company_details=array(
                
                'companyName'=>$this->input->get('companyName'),
                'avgrate'=>$this->input->get('avgrate'),
                
            );
             $mydata['company_details']=$company_details;
            
            if ($data2['type'] == 'OK') {
               
                $data1['body'] = $this->load->view('selfdrive/self_vehicledetail', $mydata, true);
                $this->load->view('admin_template', $data1);
               
            }

            if ($data2['type'] == 'ERROR') {
           
                $data1['body'] = $this->load->view('selfdrive/self_vehicledetail', $mydata, true);
                $this->load->view('admin_template', $data1);
               }
           
        } else {

            $data1['body'] = $this->load->view('selfdrive/self_vehicledetail', '', true);
            $this->load->view('admin_template', $data1);
        }
    }

}
