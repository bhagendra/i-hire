<?php

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        date_default_timezone_set('Asia/Kolkata');
        getSession();
        if ($this->uri->rsegments[2] != 'login') {

            $this->redirect_not_logged();
        }
    }

    function redirect_not_logged() {
        if (!USER_ID) {
            redirect(base_url('login'));
            die;
        }
    }

}
